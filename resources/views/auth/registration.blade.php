@extends('layouts.auth_app')
<title>Register</title>
@section('content')
   <div class="container">
      <div class="row justify-content-center">
        <div class="col-md-6 ">
          <div class="card-group shadow">
            <div class="card p-4">
                <h3 class="card-header text-center">Registration Form</h3>
                <div class="card-body">
                    <form action="{{ route('auth.save') }}" method="post">
                        @if(Session::get('Success'))
                            <div class="aler alert-success">
                                {{ Session::get('Success')}}
                            </div>
                        @endif
                        @if(Session::get('Fail'))
                            <div class="alertt alert-success">
                                {{ Session::get('Fail')}}
                            </div>
                        @endif
                            @csrf
                            <div class="form-group mb-4">
                                <input type="text" placeholder="Firstname"  class="form-control" name="fname"
                                    value="{{ old('fname') }}" autofocus>
                                <span class="text-danger">@error('fname'){{ $message }} @enderror</span>
                            </div>

                            <div class="form-group mb-4">
                                <input type="text" placeholder="Lastname" class="form-control" name="lname"
                                    value="{{ old('lname') }}"  autofocus>
                                <span class="text-danger">@error('lname'){{ $message }} @enderror</span>
                                <!-- @if ($errors->has('name'))
                                <span class="text-danger">{{ $errors->first('name') }}</span>
                                @endif -->
                            </div>

                            <div class="form-group mb-4">
                                <input type="text" placeholder="Email" id="email_address" class="form-control"
                                    name="email" value="{{ old('email') }}"  autofocus>
                                    <span class="text-danger">@error('email'){{ $message }} @enderror</span>
                                <!-- @if ($errors->has('email'))
                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                @endif -->
                            </div>

                            <div class="form-group mb-4">
                                <input type="password" placeholder="Password" id="title" class="form-control @error('title') is-invalid @enderror"
                                    name="password" >
                                    <span class="text-danger">@error('password'){{ $message }} @enderror</span>
                               <!-- @error('title')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                @enderror -->
                            </div>

                            <!-- <div class="form-group mb-3">
                                <div class="checkbox">
                                    <label><input type="checkbox" name="remember"> Remember Me</label>
                                </div>
                            </div> -->

                            <div class="d-grid mb-4">
                                <button type="submit" class="btn btn-primary btn-block">Sign up</button>
                            </div>
                            <div class="text-center  style="padding-top: 20px; margin-top; 30px;">
                                <i class="" style="font-size: 16px;"> Already have an account?</i><a href="{{ route('auth.login')}}"> Log In</a>
                            </div>
                        </form>
                </div>
            </div>
           </div>
        </div>
      </div>
    </div>

@endsection

@section('javascript')

@endsection