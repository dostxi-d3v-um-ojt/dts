<!DOCTYPE html>
<html lang="en">
<head class="">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <title>Login</title>
    <meta name="description" content="CoreUI Template - InfyOm Laravel Generator">
    <meta name="keyword" content="CoreUI,Bootstrap,Admin,Template,InfyOm,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">

    <!-- CoreUI CSS -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" crossorigin="anonymous">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/>
</head>

<body>
<header class="c-header c-header-light px-3">
    <ul class="c-header-nav ">
        <li class="c-header-nav-item ">
            <a class="c-header-brand text-decoration-none" href="{{ route('auth.login') }}">
                <h3 class="pt-2">TRACE</h3>
            </a>
        </li>
    </ul>    

    <ul class="c-header-nav ml-auto mr-3">
      <li class="c-header-nav-item ">
        <a class="c-header-nav-link mr-4" href="{{ route('contact') }}">Contact Info <span class="sr-only">(current)</span></a>
      </li>
      <li class="c-header-nav-item mr-4">
        <a class="c-header-nav-link" href="{{ route('about') }}">About Us</a>
      </li>
      <li class="c-header-nav-item active">
        <a class="c-header-nav-link font-weight-bold" href="/login" >Login</a>
      </li>
    </ul>
</header>

<div class="container fade-in" style="width: 40%; margin-top: 10%;">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card-group shadow" style="width: 100%;">
                <div class="card p-4">
                    <div class="card-body">
                        <form method="post" action="{{ route('auth.check') }}">
                            @csrf
                            <h1>Login</h1>
                            <p class="text-muted">Sign In to your account</p>
                             @if(Session::get('fail'))
                                <div class="alert alert-danger">
                                    {{ Session::get('fail')}}
                                </div>
                             @endif
                             
                            <div class="input-group mb-3 ">
                                <div class="input-group-prepend ">
                                    <span class="input-group-text">
                                      <i class="cil-user"></i>
                                    </span>
                                </div>
                                <input type="email" class="form-control @error('email') is-invalid @enderror"
                                       name="email" value="{{ old('email') }}"
                                       placeholder="Email">
                                @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                      <i class="cil-lock-locked"></i>
                                    </span>
                                </div>
                                <input type="password"
                                       class="form-control @error('password') is-invalid @enderror"
                                       placeholder="Password" name="password">
                                @error('password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <button class="btn btn-primary px-4" type="submit">Login</button>
                                </div>
                                <div class="col-8 text-right">
                                    <a class="btn btn-link px-0" href="">
                                        Forgot password?
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- CoreUI -->
<script src="{{ mix('js/app.js') }}" defer></script>

</body>
</html>
