<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>About</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/>
</head>
<body class="bg-white text-dark">
    <header class="c-header c-header-light px-3">
        <ul class="c-header-nav ">
            <li class="c-header-nav-item ">
                <a class="c-header-brand text-decoration-none" href="{{ route('auth.login') }}">
                    <h3 class="pt-2">TRACE</h3>
                </a>
            </li>
        </ul>    

        <ul class="c-header-nav ml-auto mr-3">
        <li class="c-header-nav-item mr-4">
            <a class="c-header-nav-link " href="{{ route('contact') }}">Contact Info <span class="sr-only">(current)</span></a>
        </li>
        <li class="c-header-nav-item mr-4 active">
            <a class="c-header-nav-link font-weight-bold" >About Us</a>
        </li>
        <li class="c-header-nav-item">
            <a class="c-header-nav-link " href="{{ route('auth.login') }}">Login</a>
        </li>
        </ul>
    </header>

    <div class="container-fluid align-items-center fade-in">
        <h1 class="text-center  mt-5 mb-5 " style=" color: #212121;">ABOUT US</h1>
        <div class="" style="width: 45%; height: 300px; margin: 80px 0px 0px 60px;">
            <div class=" ">

            </div>
            <div class="p-5" style="color: #212121;">
                <h3 class="text-center">Document Tracking Sytem (DTS)</h3>
                <p class="mt-4 text-justify">Document Tracking System (DTS) is an information system capable of tracking the paper trail of documents created within office 
                    of company/organization. The DTS includes information on the originating and receiving office and personnel, as well as the time elapsed between offices/units/departments. 
                    In this information system, document attachments, revisions, updates, and remarks are also supported.
                    
                </p>
            </div>
        </div>
        <div class="" style="width: 45%; height: 300px; float: right; margin: -300px 60px 0px 0px;">
            <div class="">

            </div>
            <div class="p-5" style="color: #212121;">
                 <h3 class="text-center">Department of Science and Technology (DOST XI) </h3>
                <p class="mt-4 text-justify">
                    DOST XI provides technology training services to customers particularly those intending to establish new technology-based enterprises (livelihood) or upgrade existing ones. 
                    This program involves the transfer of knowledge and/or skills by tapping S&T experts from various RDIs and councils thru any or a combination of the following processes:
                </p>
            </div>
        </div>
    </div>
</body>
    <div class="container" style="margin-top: 80px;">
        <div class="p-2">
             <div class="c-footer bg-white" style="padding-left: 39%;">
                 Copyright &copy; {{ date('Y') }} &nbsp;
                <a href="https://region11.dost.gov.ph" class="text-decoration-none"> DOSTrack</a>, All rights reserved
            </div>
           
        </div>  
        
    </div>
    <div class="container text-center " style="color:#616161;">
            Developer: Steven C. Badang - 2021
    </div> 
    <div class="container text-center " style="color:#616161;">
            Developer: Remi Vallejo - 2022
    </div> 
</html>