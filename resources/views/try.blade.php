
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>DOSTRACK</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="https://kit.fontawesome.com/d7199dc3ef.js" crossorigin="anonymous"></script>
    <script
    src="https://code.jquery.com/jquery-3.6.0.js"
    integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
    crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

</head>

<body class="c-app" data-new-gr-c-s-check-loaded="14.1012.0" data-gr-ext-installed="">
@include('includes.sidebar')

<div class="c-wrapper c-fixed-components">
    @include('includes.header')
    <div class="c-body">
        <main class="c-main">
            <link rel="stylesheet" href="{{asset('css/style.css')}}">
            <div class="container-fluid mb-3">
                @role('Regional Director')
                    <div class="fade-in">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb" style="font-size: 16px;">
                                <li class="breadcrumb-item"><a href="{{ route('admin.documents')}}" class="breadcrumb-item"  aria-current="page">View Documents</a></li>
                                <li class="breadcrumb-item active">Route</li>
                            </ol>
                        </nav>  
                    </div>
                @endrole
                @role('Document Controller')
                    <div class="fade-in">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb" style="font-size: 16px;">
                                <li class="breadcrumb-item"><a href="{{ route('user.incoming_doc')}}" class="breadcrumb-item"  aria-current="page">Incoming Document</a></li>
                                <li class="breadcrumb-item"><a href="{{ route('user.document')}}" class="breadcrumb-item"  aria-current="page">View Documents</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </nav>  
                    </div>
                @endrole
                @role('Ordinary User Role|Section Head|Division Head')
                    <div class="fade-in">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb" style="font-size: 16px;">
                                <li class="breadcrumb-item"><a href="{{ route('user.document')}}" class="breadcrumb-item"  aria-current="page">View Documents</a></li>
                                <li class="breadcrumb-item active">Edit</li>
                            </ol>
                        </nav>  
                    </div>
                @endrole
                
                @foreach ($document as $document)
                <!-- The form -->
                
                
                
                <form action="{{ route('admin.saveupdate', $document->doc_id)}}" method="post" enctype="multipart/form-data" class="myform">
                    {{ csrf_field()}}
                    <div class="shadow bg-white " style="padding: 20px 150px 20px 100px; margin: 0px 0px 0px 10px;" id="div-form" >
                            <h3 style="text-align: center; margin: 10px 0px 30px 0px;">Edit Document's Data</h3>
                            <h5 class="heading" style="margin: 0px 0px 0px 70px;">Document Information </h5>
                            <div class="form-group row "  style="margin-top: 50px;">
                                <input type="text" value="{{ $document->doc_id }}" name="doc_id" hidden>
                                <label for="subject" class="col-sm-2 col-form-label"  style="text-align: right" >Subject:</label>
                                <div class="col-sm-10">
                                <input type="text" class="form-control" id="subject" name="subject" autocomplete="on" value="{{ $document->subject }}"required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="docType" class="col-sm-2 col-form-label" style="text-align: right" >Document Type:</label>
                                <div class="col-sm-10">
                                    <select class="form-control" name="doc_type">
                                        <option value="{{  $document->document_type }}" hidden>{{ $document->document_type}}</option>
                                        @foreach ($doc_types as $doc_type)
                                            <option value="{{ $doc_type->doc_type }}">{{$doc_type->doc_type}}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                            </div>
                            

                            <div class="form-group row">
                            <label for="docDate" class="col-sm-2 col-form-label" style="text-align: right">Document Date:</label>
                                <div class="col-sm-10" style=" width: 245px;" >
                                <input type="date" class="form-control" id="docDate" style="width: 250px;" name="doc_date" autocomplete="off"  value="{{ $document->document_date }}"required >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date-received" class="col-sm-2 col-form-label" style="text-align: right">Date Received:</label>
                                <div class="col-sm-10" style=" width: 245px;" >
                                    <input type="date" class="form-control" id="date-received"  style="width: 250px;" name="date_received" autocomplete="off" value="{{ $document->date_received }}"required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="sender" class="col-sm-2 col-form-label" style="text-align: right">Sender:</label>
                                <div class="col-sm-10" style=" width: 245px;" >
                                    <input type="text" class="form-control" id="sender" name="sender" autocomplete="on" value="{{ $document->sender }}" required >
                                </div>
                            </div>

                            <div class="form-group row">
                            <label for="company_desc" class="col-sm-2 col-form-label" style="text-align: right">Company:</label>
                                <div class="col-sm-10" style=" width: 245px;" >
                                    <input type="text" class="form-control" id="company_desc" name="company" autocomplete="on" value="{{ $document->company }}"required >
                                </div>
                            </div>

                            <h5 class="heading" style="margin: 80px 0px 0px 70px;">File Attachments</h5>
                            <div class="form-group row" style="margin: 20px 0px 30px 90px;" >
                                <div class="col-sm-10">
                                    <?php  
                                        $fileArr = DB::table('attachments_lists')
                                                ->select('file_name') 
                                                ->where('doc_id', $document->doc_id)
                                                ->get();
                                        
                                    ?>
                                    @foreach($fileArr as $file)
                                            <input type="text" name="oldFile[]"  value="{{ $file->file_name }}" hidden >
                                            <a href="{{ route('admin.download',$file->file_name)}}" class="active" data-toggle="tooltip" data-placement="bottom" title="Click to download" >{{ $file->file_name }}</a>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group row">
                            <label for="files" class="col-sm-2 col-form-label" style="text-align: right">Replace Attachment:</label>
                                <div class="col-sm-10" style=" width: 245px;" >
                                    <input type="file" class="border  @error('file') is-invalid @enderror" id="file" name="file[]" multiple >
                                    @if(Session::get('fail'))
                                            <div class="alert text-danger">
                                                {{ Session::get('fail')}}
                                            </div>
                                    @endif
                                </div>
                                
                            </div>
                            
                          
                                <h5 class="heading" style="margin: 80px 0px 0px 70px;">Routing Information</h5>
                            <div class="form-group row"  style="margin-top: 50px;" >
                            <input type="text" value="{{ $document->route_id }}" name="route_id" hidden>
                            <label for="routingSlip" class="col-sm-2 col-form-label"  style="text-align: right">Routing Slip:</label>
                            <div class="col-sm-10">
                                    <input type="text" class="form-control" id="routingSlip"  value="{{ $document->route_slip }}" name="slip"readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                            <label for="from" class="col-sm-2 col-form-label" style="text-align: right">From:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="from" name="rfrom" required value="{{ $document->route_from }}">
                                            </div>
                            </div>

                            @if (auth()->user()->usergroup == "Regional Director")
                            <div class="form-group row"  style="margin-top: 20px;" >
                                <label for="route-thru" class="col-sm-2 col-form-label required"  style="text-align: right" required >Routed Thru:</label>
                                <div class="col-sm-10">
                                    <select class="form-control"  name="routeThru[]" id="choices-multiple-remove-button" placeholder="{{$document->route_thru}}" value="{{$document->route_thru}}" multiple>
                                        @foreach ($heads as $user)
                                            <option value="{{$user->fname . " " .  $user->lname}}">{{$user->fname . " ". $user->lname . " (". $user->usergroup .") "}} </option>
                                        @endforeach
                                    </select> 
                                </div>
                            </div>

                            <div class="form-group row"  style="margin-top: 20px;" >
                            <label for="route-to" class="col-sm-2  required"  style="text-align: right" required >Routed To:</label>
                                <div class="col-sm-10">
                                    <select multiple id="e1" style="width:300px">
                                            <option value="AL">Alabama</option>
                                            <option value="Am">Amalapuram</option>
                                            <option value="An">Anakapalli</option>
                                            <option value="Ak">Akkayapalem</option>
                                            <option value="WY">Wyoming</option>
                                        </select>
                                    <input type="checkbox" id="checkbox" >Select All

                                    <input type="button" id="button" value="check Selected">
                                </div>
                            </div>
                  
                            <div class="form-group row"  style="margin-bottom: 60px;">
                            <label for="action" class="col-sm-2 col-form-label required"  style="text-align: right" required >Action Needed:</label>
                            
                            <div class="col-sm-10">
                                <select class="form-control" name="action[]" id="choices-multiple-remove-button" placeholder="{{ $document->action }}" value="{{ $document->action }}"  multiple>
                                    @foreach ($actions as $action)
                                        <option value="{{$action->action}}" >{{$action->action}}</option>    
                                    @endforeach
                                </select>
                            </div>
                            </div>
                            @endif
                            

                        <div class="form-group row "  >
                            <div class="col-lg ">
                                @role('Regional Director')
                                    <button type="submit" class="btn btn-primary" style="width: 150px; height: 45px; margin-left:16px; float: right;">Update</button>
                                    <a href="{{ route('admin.documents') }}" ><button type="button" class="btn btn-secondary" style="width: 150px; height: 45px; float: right;">Backs</button></a>
                                @endrole
                                @role('Document Controller|Division Head|Section Head|Ordinary User Role')
                                    <button type="submit" class="btn btn-primary" style="width: 150px; height: 45px; margin-left:16px; float: right;">Update</button>
                                    <a href="{{ route('user.document') }}" ><button type="button" class="btn btn-secondary" style="width: 150px; height: 45px; float: right;">Backs</button></a>
                                @endrole
                            </div>
                        </div>
                      
                        
                    </div>
                    @endforeach
                            
                </form>
                                
            </div>
        </main>
        @include('includes.footer')
    </div>
</div>
<script>
$("#e1").select2();
$("#checkbox").click(function(){
    if($("#checkbox").is(':checked') ){
        $("#e1 > option").prop("selected","selected");
        $("#e1").trigger("change");
    }else{
        $("#e1 > option").removeAttr("selected");
         $("#e1").trigger("change");
     }
});

$("#button").click(function(){
       alert($("#e1").val());
});

</script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>

