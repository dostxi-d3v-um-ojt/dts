<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>DOSTRACK</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <script src="https://kit.fontawesome.com/d7199dc3ef.js" crossorigin="anonymous"></script>
   
    @yield('after-styles')
</head>

<body class="c-app" data-new-gr-c-s-check-loaded="14.1012.0" data-gr-ext-installed="">
@include('includes.sidebar')

<div class="c-wrapper c-fixed-components">
    @include('includes.header')
    <div class="c-body">
        <main class="c-main">
            @yield('content')
        </main>
        @include('includes.footer')
    </div>
</div>

<!-- @yield('third_party_scripts') -->

<!-- @stack('page_scripts') -->
</body>
</html>
<script src="{{mix('js/app.js')}}"></script>
<script>
    var APP_URL = "{{ url('/') }}";
</script>
@yield('after-scripts')
