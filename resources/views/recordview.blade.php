@extends('layouts.app')

@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid" >
    <!-- fade-in -->
     @role('Regional Director')
        <div class="fade-in">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="font-size: 16px;">
                    <li class="breadcrumb-item"><a href="{{ route('admin.documents')}}" class="breadcrumb-item"  aria-current="page">View Documents</a></li>
                    <li class="breadcrumb-item" aria-current="page">Record's Information</li>
                </ol>
            </nav>  
        </div>
    @endrole
  


    @foreach($document as $document)
    <div class="form-group fade-in p-3 mb-5 bg-white"  style="margin: 20px 0px 0px 0px; width: auto;" >
      <form action="{{route('user.received', $document->doc_id)}}" method="POST" style="padding: 20px 200px 20px 200px;">
        @csrf
        <h3 style="margin: 20px 0px 80px 250px;">Document's Information</h3>
            <div class="form-group row">
                <div class="col">
                    <label for="subject">Subject</label>
                    <input type="text" class="form-control bg-white border-dark" id="subject" value="{{ $document->subject }}" readonly>
                </div>
                <div class="col">
                    <label for="type">Document Type</label>
                    <input type="text" class="form-control  bg-white border-dark" id="type" value="{{ $document->document_type }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <div class="col">
                    <label for="subject">Document Date</label>
                    <input type="text" class="form-control  bg-white border-dark" id="subject" value="{{ $document->document_date }}" readonly>
                </div>
                <div class="col">
                    <label for="type">Date Received</label>
                    <input type="text" class="form-control  bg-white border-dark" id="type" value="{{ $document->date_received }}" readonly>
                </div>
            </div>
            <div class="form-group row">
                <div class="col">
                    <label for="subject">Sender</label>
                    <input type="text" class="form-control  bg-white border-dark" id="subject"  value="{{ $document->sender }}" readonly>
                </div>
                <div class="col">
                    <label for="type">Company</label>
                    <input type="text" class="form-control  bg-white border-dark" id="type"  value="{{ $document->company }}" readonly>
                </div>
            </div>
            <div class="form-group row" style="margin-top: 80px;">
            <div class="col">
                    <label for="subject">Routing Slip</label>
                    <input type="text" class="form-control  bg-white border-dark" id="subject" value="{{ $document->route_slip }}" readonly>
                </div>
                <div class="col">
                    <label for="type">Routed Thru</label>
                    <input type="text" class="form-control  bg-white border-dark" id="type" value="{{ $document->route_thru }}" readonly>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-lg-6">
                    <label for="subject">Routing To</label>
                    <input type="text" class="form-control  bg-white border-dark" id="subject"value="{{ $document->route_to }}"  readonly>
                </div>
                <div class="col-lg-6">
                    <label for="subject">Action Needed</label>
                    <input type="text" class="form-control  bg-white border-dark" id="subject" style="margin: 0px 0px 50px 0px;" value="{{ $document->action }}"  readonly>
                </div>
                <div class="col">
                        <p>File Attachments</p>
                        <?php  
                            $fileArr = DB::table('attachments_lists')
                                    ->select('file_name') 
                                    ->where('doc_id', $document->doc_id)
                                    ->get();
                             
                        ?>
                        @foreach($fileArr as $file)
                                <input type="text" name="oldFile[]"  value="{{ $file->file_name }}" hidden >
                                <a href="{{ route('admin.download',$file->file_name)}}" class="active" data-toggle="tooltip" data-placement="bottom" title="Click to download" >{{ $file->file_name }}</a>
                        @endforeach
                    </div>
                    {{-- To be fix --}}
                    {{-- @role('Regional Director')
                        @foreach ($comments as $object)    
                            <div class="col">
                                    <label for="exampleFormControlTextarea1" class="form-label">Comments</label>
                                    <textarea class="form-control" name="comments" placeholder="{{ $object->route_to_message }}" value="{{ $object->route_to_message }}" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
                        @endforeach
                    @endrole --}}
                    @if(auth()->user()->usergroup == 'Ordinary User' || auth()->user()->usergroup == "Section Head" || auth()->user()->usergroup == "Division Head" || auth()->user()->usergroup == "Document Controller") 
                        @foreach ($comments as $object)    
                            <div class="col">
                                <label for="exampleFormControlTextarea1" class="form-label">Comments</label>
                                <textarea class="form-control" name="comments" placeholder="{{ $object->route_to_message }}" value="{{ $object->route_to_message }}" id="exampleFormControlTextarea1" rows="3"></textarea>
                            </div>
            </div>
                    {{-- receive--}}
                            @if($object->receive == 'no')
                            <div class="form-group row">
                                    <div class="col">
                                                <button type="submit" class="btn btn-success" style="width: 150px; height: 45px; float: right;">Receive Document</button>
                                    </div>
                            </div> 
                            @endif
                        @endforeach            
                    @endif
        </form>
        
    </div>
    @endforeach
</div>
@endsection
