@extends('layouts.app')
@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="fade-in">
        
        @can('create-incoming-documents')
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="font-size: 16px;">
                    <li class="breadcrumb-item"><a href="{{ route('user.incoming_doc')}}" class="breadcrumb-item">Incoming Document</a></li>
                    <li class="breadcrumb-item active" aria-current="page">View Records</li>
                </ol>
            </nav>  
        @endcan

        @can('view-incoming-documents')
            <nav aria-label="breadcrumb" >
                <ol class="breadcrumb" style="font-size: 16px;">
                    <li class="breadcrumb-item">View Documents</li>
                </ol>
            </nav> 
        @endcan

        <div class="card">
            <div class="card-body">
                <div class="justify-content-between">
                    <div class="container mt-5">
                        <table class="table table-bordered yajra-datatable">
                                <thead>
                                    <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Subject</th>
                                    <th scope="col">Document Type</th>
                                    <th scope="col">Sender</th>
                                    <th scope="col">Company</th>
                                    <th scope="col">Action</th>
                                    </tr>
                                </thead>

                            <tbody>
                            @foreach ($documents as $documents)
                            <tr>
                                <td>{{ $documents->doc_id }}</td>
                                <td>{{ $documents->subject }}</td>
                                <td>{{ $documents->document_type }}</td>
                                <td>{{ $documents->sender }}</td>
                                <td>{{ $documents->company }}</td>
                                <td class="action">
                                    @role('Regional Director')
                                        
                                        {{-- <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{ $documents->doc_id }}"><span class="cil-remove btn-icon mr-1"></span>Delete</button> --}}
                                        @if ($documents->route_thru != NULL)
                                            <a class="" href="/admin/retrieve/{{ $documents->doc_id }}/{{ $documents->route_id }}" >
                                                <button type="button" class="btn btn-success btn-sm">
                                                    <span class="cil-description btn-icon mr-1"></span>View
                                                </button>
                                            </a> 
                                        @else
                                            <a class="" href="/admin/edit/{{ $documents->doc_id }}/{{ $documents->route_id }}" ><button type="button" class="btn btn-info btn-sm"><span class="cil-pencil btn-icon mr-1"></span>Tag</button></a>
                                        @endif
                                    @endrole
                                         
                                </td>
                            </tr>
                
                                @can('delete-incoming-documents')
                                <!-- The Modal -->
                                <div class="modal fade" id="exampleModal{{ $documents->doc_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Are you sure you want to delete this record?</p>
                                        <p>doc_id:{{ $documents->doc_id }} </p>
                                        <p>route_id: {{ $documents->route_id }}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <input type="text" value="{{ $documents->route_id }}" name = "doc_id" hidden >
                                        <a href="/admin/delete/{{$documents->doc_id}}/{{$documents->route_id}}"><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                @endcan
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
               
            </div>
        </div>
        
    </div>
</div>
@endsection
@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function () {
    var table = $('.yajra-datatable').DataTable();

});
    var Chart = Chart;
    var Chart = Chart.BarController;
    console.log(Chart);
    Chart.register(Chart);
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
</script>
@endsection
