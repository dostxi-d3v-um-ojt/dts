@extends('layouts.app')
@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('content')

<div class="container fade-in">
   
<form action="/user/update/{{auth()->user()->id}}" method="post" enctype="multipart/form-data">
    @csrf
    @foreach($user_info as $user)
    <div class="container-fluid mt-4" style="width: 700px;">
        
            <div class="form-group row d-flex align-items-center justify-content-center ">
               <img src="{{$user->picture ? asset('uploads/'.$user->picture) : asset('img/circle-user-regular.svg')}}" alt="picture" class="w-25 h-25 border" style="border-radius: 50%" > 
            </div>
    <div class="accordion" id="accordionExample">
        <div class="card">
              
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn " type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    <h6>Basic Information</h6>
                    </button>
                </h2>
            </div>
            
            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    <div class="container">
                        <div class="form-group row" >
                            <label for="rto" class="col-sm-2 col-form-label" style="text-align: right">Firstname:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="rto" value="{{ $user->fname }}"name="fname" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rto" class="col-sm-2 col-form-label" style="text-align: right">Lastname:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="rto" value="{{ $user->lname }}" name="lname" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="rto" class="col-sm-2 col-form-label" style="text-align: right">Birthday:</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" id="rto" value="{{$user->birthday}}" name="birthday">
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="rto" class="col-sm-2 col-form-label" style="text-align: right">Picture</label>
                            <div class="col-sm-10">
                                 <input type="file" class="form-control p-1" id="rto" name="picture">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pos" class="col-sm-2 col-form-label" style="text-align: right">Position:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="pos" value="{{$user->usergroup}}" name="position"  readonly>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <div class="card">
            <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <h6>Contact Information</h6>
                    </button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">

                <div class="container">
                    <div class="form-group row" >
                        <label for="rto" class="col-sm-2 col-form-label" style="text-align: right">Email:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rto"  value="{{ $user->email }}"  name="email" required>
                        </div>
                    </div>
                   
                    <div class="form-group row">
                        <label for="rto" class="col-sm-2 col-form-label" style="text-align: right">Phone:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="rto" name="phone" value="{{ $user->phone }}" >
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    
    
    @endforeach

    <div class="container-fluid mb-3" style="width: 700px;">
        <button class="btn btn-block btn-primary mb-5">Update</button>
    </div>
</form>
</div>
    



@endsection

