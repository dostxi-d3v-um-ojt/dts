@extends('layouts.app')
@section('after-styles')
 <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="fade-in">
    
        <h2 class="mb-4 text-center">Routed To Me</h2>
        <div class="card rounded" style="width: 80%; margin-left: 12%;">
            <div class="card-body mb-4">
                    <div class="container mt-5" style="width: 80%;">
                       <table class="table border yajra-datatable">
                                <thead class="text-value-md">
                                    <tr>
                                        <th scope="col" class="border-top-0 border-bottom-0 text-nowrap">No</th>
                                        <th scope="col" class="border-top-0 border-bottom-0 text-nowrap">Document Type</th>
                                        <th scope="col" class="border-top-0 border-bottom-0 text-nowrap">Sender</th>
                                        <th scope="col" class="border-top-0 border-bottom-0 text-nowrap">Company</th>
                                        <th scope="col" class="border-top-0 border-bottom-0 text-nowrap">Action</th>
                                    </tr>
                                </thead>

                            <tbody>
                            @foreach ($document as $documents)
                            <tr>
                                <td>{{ $documents->doc_id }}</td>
                                <td>{{ $documents->document_type }}</td>
                                <td>{{ $documents->sender }}</td>
                                <td>{{ $documents->company }}</td>
                                <td class="action">
                                    <a class="" href="/user/retrieve/{{ $documents->doc_id }}/{{ $documents->route_id }}" ><button type="button" class="btn btn-success btn-sm"><span class="cil-description btn-icon mr-1"></span>View</button></a>                                    
                                    <a class="" href="/user/edit/{{ $documents->doc_id }}/{{ $documents->route_id }}" ><button type="button" class="btn btn-info btn-sm"><span class="cil-pencil btn-icon mr-1"></span>Edit</button></a>
                                    <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#exampleModal{{ $documents->doc_id }}"><span class="cil-remove btn-icon mr-1"></span>Delete</button>
                                </td>
                            </tr>
            

                                <!-- The Modal -->
                                <div class="modal fade" id="exampleModal{{ $documents->doc_id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>Are you sure you want to delete this record?</p>
                                        <p>doc_id:{{ $documents->doc_id }} </p>
                                        <p>route_id: {{ $documents->route_id }}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                        <input type="text" value="{{ $documents->route_id }}" name = "doc_id" hidden >
                                        <a href="/admin/delete/{{$documents->doc_id}}/{{$documents->route_id}}"><button type="button" class="btn btn-danger">Delete</button></a>
                                    </div>
                                    </div>
                                </div>
                                </div>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
               
            </div>
        </div>
        
    </div>
</div>
@endsection

@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function () {
    var table = $('.yajra-datatable').DataTable();
});
    // var Chart = Chart;
    
    // var Chart = Chart.BarController;
    // console.log(Chart);
    // Chart.register(Chart);
    var ctx = document.getElementById('myChart');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
    </script>
@endsection