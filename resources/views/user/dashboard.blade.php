@extends('layouts.app')
@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid ">
    <div class="fade-in">
        @role('Document Controller')
         <div class="row justify-content-center">
                <div class="col-lg-4">
                    <div class="card shadow">
                        <div class="card-header bg-youtube content-center text-uppercase large text-white">
                            <i class="c-header-nav "></i> Documents
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                {{-- correct output --}}
                                <a href="{{ route('user.document')}}" class="text-value-xl text-dark"> {{count($document)}}</a>
                                <div class="text-uppercase text-muted small">Incoming documents</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <div class="text-value-xl">{{ count($document) - count($pending)}}</div>
                                <div class="text-uppercase text-muted small">Routed Documents</div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-4">
                     <div class="card shadow">
                        <div class="card-header bg-yahoo content-center text-uppercase large text-white">
                           <i class="c-header-nav "></i> Notifications
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">0</div>
                                <div class="text-uppercase text-muted small">Messages</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                
                                <a href="{{ route('user.pendingincoming')}}" class="text-value-xl text-dark"> {{count($pending)}}</a>
                                <div class="text-uppercase text-muted small">unrouted documents</div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        @endrole
        @role('Ordinary User Role|Division Head|Section Head')
           <div class="row justify-content-center">
                <div class="col-sm-5 col-lg-4 ">
                    <div class="card shadow">
                        <div class="card-header bg-youtube content-center text-uppercase large text-white">
                            <i class="c-header-nav "></i> Documents
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">{{ count($document) }}</div>
                                <div class="text-uppercase text-muted small">Incoming documents</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <a href="{{ route('user.receive')}}" class="text-value-xl text-dark"> {{count($document) - count($pendings)}}</a>
                                <div class="text-uppercase text-muted small">recieved documents</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-lg-4 ">
                    <div class="card shadow">
                        <div class="card-header bg-yahoo content-center text-uppercase large text-white">
                           <i class="c-header-nav "></i> Notifications
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <div class="text-value-xl">0</div>
                                <div class="text-uppercase text-muted small">Messages</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <a href="{{ route('user.pending')}}" class="text-value-xl text-dark"> {{count($pendings)}}</a>
                                <div class="text-uppercase text-muted small">Pending documents</div>
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        @endrole
    </div>
</div>
<div class="container-fluid">
     <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card p-5 shadow">
                    <p class="h5">Welcome {{ auth()->user()->fname }}! </p>
                    <p>You now have access to DOST XI TRACE DTS Management System.</p>
                    <div class="card-body">
                    </div>
                </div>
        </div>
     </div>
</div>
@endsection
@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function () {
    var table = $('.yajra-datatable').DataTable();
});
   
</script>
@endsection
