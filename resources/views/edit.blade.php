@extends('layouts.app')

@section('after-styles')
    
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/gh/bbbootstrap/libraries@main/choices.min.js"></script>
@endsection

@section('content')
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<div class="container-fluid mb-3">
    @role('Regional Director')
        <div class="fade-in">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="font-size: 16px;">
                    <li class="breadcrumb-item"><a href="{{ route('admin.documents')}}" class="breadcrumb-item"  aria-current="page">View Documents</a></li>
                    <li class="breadcrumb-item active">Tag</li>
                </ol>
            </nav>  
        </div>
    @endrole
    @role('Document Controller')
        <div class="fade-in">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="font-size: 16px;">
                    <li class="breadcrumb-item"><a href="{{ route('user.incoming_doc')}}" class="breadcrumb-item"  aria-current="page">Incoming Document</a></li>
                    <li class="breadcrumb-item"><a href="{{ route('user.document')}}" class="breadcrumb-item"  aria-current="page">View Documents</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </nav>  
        </div>
    @endrole
    @role('Ordinary User Role|Section Head|Division Head')
        <div class="fade-in">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb" style="font-size: 16px;">
                    <li class="breadcrumb-item"><a href="{{ route('user.document')}}" class="breadcrumb-item"  aria-current="page">View Documents</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </nav>  
        </div>
    @endrole
    
     @foreach ($document as $document)
    <!-- The form -->
    
    
    
    <form action="{{ route('admin.saveupdate', $document->doc_id)}}" method="post" enctype="multipart/form-data" class="myform">
        {{ csrf_field()}}
        <div class="shadow bg-white " style="padding: 20px 150px 20px 100px; margin: 0px 0px 0px 10px;" id="div-form" >
                {{-- <h3 style="text-align: center; margin: 10px 0px 30px 0px;">Edit Document's Data</h3> --}}
                <h5 class="heading" style="margin: 0px 0px 0px 70px;">Document Information </h5>
                <div class="form-group row "  style="margin-top: 50px;">
                    <input type="text" value="{{ $document->doc_id }}" name="doc_id" hidden>
                    <label for="subject" class="col-sm-2 col-form-label"  style="text-align: right" >Subject:</label>
                    <div class="col-sm-10">
                    <input type="text" class="form-control" id="subject" name="subject" autocomplete="on" value="{{ $document->subject }}"required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="docType" class="col-sm-2 col-form-label" style="text-align: right" >Document Type:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="doc_type">
                            <option value="{{  $document->document_type }}" hidden>{{ $document->document_type}}</option>
                            @foreach ($doc_types as $doc_type)
                                <option value="{{ $doc_type->doc_type }}">{{$doc_type->doc_type}}</option>
                            @endforeach
                            
                        </select>
                    </div>
                </div>
                

                <div class="form-group row">
                <label for="docDate" class="col-sm-2 col-form-label" style="text-align: right">Document Date:</label>
                    <div class="col-sm-10" style=" width: 245px;" >
                    <input type="date" class="form-control" id="docDate" style="width: 250px;" name="doc_date" autocomplete="off"  value="{{ $document->document_date }}"required >
                    </div>
                </div>

                <div class="form-group row">
                    <label for="date-received" class="col-sm-2 col-form-label" style="text-align: right">Date Received:</label>
                    <div class="col-sm-10" style=" width: 245px;" >
                        <input type="date" class="form-control" id="date-received"  style="width: 250px;" name="date_received" autocomplete="off" value="{{ $document->date_received }}"required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="sender" class="col-sm-2 col-form-label" style="text-align: right">Sender:</label>
                    <div class="col-sm-10" style=" width: 245px;" >
                        <input type="text" class="form-control" id="sender" name="sender" autocomplete="on" value="{{ $document->sender }}" required >
                    </div>
                </div>

                <div class="form-group row">
                <label for="company_desc" class="col-sm-2 col-form-label" style="text-align: right">Company:</label>
                    <div class="col-sm-10" style=" width: 245px;" >
                        <input type="text" class="form-control" id="company_desc" name="company" autocomplete="on" value="{{ $document->company }}"required >
                    </div>
                </div>

                <h5 class="heading" style="margin: 80px 0px 0px 70px;">File Attachments</h5>
                <div class="form-group row" style="margin: 20px 0px 30px 90px;" >
                    <div class="col-sm-10">
                        <?php  
                            $fileArr = DB::table('attachments_lists')
                                    ->select('file_name') 
                                    ->where('doc_id', $document->doc_id)
                                    ->get();
                             
                        ?>
                        @foreach($fileArr as $file)
                                <input type="text" name="oldFile[]"  value="{{ $file->file_name }}" hidden >
                                <a href="{{ route('admin.download',$file->file_name)}}" class="active" data-toggle="tooltip" data-placement="bottom" title="Click to download" >{{ $file->file_name }}</a>
                        @endforeach
                    </div>
                </div>

                <div class="form-group row">
                <label for="files" class="col-sm-2 col-form-label" style="text-align: right">Replace Attachment:</label>
                    <div class="col-sm-10" style=" width: 245px;" >
                        <input type="file" class="border  @error('file') is-invalid @enderror" id="file" name="file[]" multiple >
                        @if(Session::get('fail'))
                                <div class="alert text-danger">
                                    {{ Session::get('fail')}}
                                </div>
                        @endif
                    </div>
                     
                </div>
                 
               
                    <h5 class="heading" style="margin: 80px 0px 0px 70px;">Routing Information</h5>
                <div class="form-group row"  style="margin-top: 50px;" >
                <input type="text" value="{{ $document->route_id }}" name="route_id" hidden>
                <label for="routingSlip" class="col-sm-2 col-form-label"  style="text-align: right">Routing Slip:</label>
                <div class="col-sm-10">
                        <input type="text" class="form-control" id="routingSlip"  value="{{ $document->route_slip }}" name="slip"readonly>
                    </div>
                </div>
                <div class="form-group row">
                                <label for="from" class="col-sm-2 col-form-label" style="text-align: right">From:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="from" name="rfrom" required value="{{ $document->route_from }}">
                                </div>
                </div>

                @if (auth()->user()->usergroup == "Regional Director")
                <div class="form-group row"  style="margin-top: 20px;" >
                    <label for="route-thru" class="col-sm-2 col-form-label required"  style="text-align: right" required >Routed Thru:</label>
                    <div class="col-sm-10">
                        <select class="form-control"  name="routeThru[]" id="choices-multiple-remove-button" placeholder="{{$document->route_thru}}" value="{{$document->route_thru}}" multiple>
                             @foreach ($heads as $user)
                                <option value="{{$user->fname . " " .  $user->lname}}">{{$user->fname . " ". $user->lname . " (". $user->usergroup .") "}} </option>
                            @endforeach
                        </select> 
                    </div>
                </div>

                <div class="form-group row"  style="margin-top: 20px;" >
                <label for="route-to" class="col-sm-2 col-form-label required"  style="text-align: right" required >Routed To:</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="routeTo[]" id="choices-multiple-remove-button" placeholder="{{ $document->route_to }}" value="{{ $document->route_to }}" multiple>
                                @foreach ($ordinary as $user)
                                    <option value="{{$user->fname . " " .  $user->lname}}">{{$user->fname . " ". $user->lname}}</option>
                                @endforeach
                        </select> 
                    </div>
                </div>
       
                <div class="form-group row"  style="margin-bottom: 60px;">
                <label for="action" class="col-sm-2 col-form-label required"  style="text-align: right" required >Action Needed:</label>
                
                <div class="col-sm-10">
                    <select class="form-control" name="action[]" id="choices-multiple-remove-button" placeholder="{{ $document->action }}" value="{{ $document->action }}"  multiple>
                        @foreach ($actions as $action)
                            <option value="{{$action->action}}" >{{$action->action}}</option>    
                        @endforeach
                    </select>
                </div>
                </div>
                @endif
                

            <div class="form-group row "  >
                <div class="col-lg ">
                    @role('Regional Director')
                        <button type="submit" class="btn btn-primary" style="width: 150px; height: 45px; margin-left:16px; float: right;">Tag</button>
                        <a href="{{ route('admin.documents') }}" ><button type="button" class="btn btn-secondary" style="width: 150px; height: 45px; float: right;">Back</button></a>
                    @endrole
                    @role('Document Controller|Division Head|Section Head|Ordinary User Role')
                        <button type="submit" class="btn btn-primary" style="width: 150px; height: 45px; margin-left:16px; float: right;">Update</button>
                        <a href="{{ route('user.document') }}" ><button type="button" class="btn btn-secondary" style="width: 150px; height: 45px; float: right;">Back</button></a>
                    @endrole
                </div>
            </div>
           
             
        </div>
        @endforeach
                
    </form>
                    
</div>
<script>
  $(document).ready(function(){
    
     var multipleCancelButton = new Choices('#choices-multiple-remove-button', {
        removeItemButton: true,
        
        searchResultLimit:5,
        
      }); 
     
     
 });
</script>
@endsection


