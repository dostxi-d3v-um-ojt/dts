@extends('layouts.app')
@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="fade-in">
        
            <nav aria-label="breadcrumb" >
                <ol class="breadcrumb" style="font-size: 16px;">
                    <li class="breadcrumb-item">View Documents</li>
                </ol>
            </nav> 
            
        <div class="card">
          
            <div class="card-body">
                <div class="justify-content-between">
                    <div class="container mt-5">
                        <table class="table table-bordered yajra-datatable">
                                <thead>
                                    <tr>
                                    <th scope="col">Subject</th>
                                    <th scope="col">Routed To</th>
                                    <th scope="col">Date Received</th>
                                    <th scope="col">Action</th>
                                    </tr>
                                </thead>

                            <tbody>
                            @foreach ($documents as $documents)
                            <tr>
                                <td>{{ $documents->subject }}</td>
                                <td>{{ $documents->route_to_name }}</td>
                                <td>{{ $documents->updated_at }}</td>
                                <td class="action">
                                        <a class="" href="/admin/retrieve/{{ $documents->doc_id }}/{{ $documents->route_id }}" >
                                            <button type="button" class="btn btn-success btn-sm">
                                                <span class="cil-description btn-icon mr-1"></span>View
                                            </button>
                                        </a>  
                                </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
               
            </div>
        </div>
        
    </div>
</div>
@endsection
@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function () {
    var table = $('.yajra-datatable').DataTable({
        "order": [[3, "desc"]]
    });

});
   
</script>
@endsection
