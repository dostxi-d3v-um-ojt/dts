@extends('layouts.app')

@section('after-styles')

@endsection
@section('content')
<link rel="stylesheet" href="{{asset('css/style.css')}}">
<div class="container-fluid">
    <!-- fade-in -->
    <div class="fade-in">
        
        <nav aria-label="breadcrumb" >
            <ol class="breadcrumb" style="font-size: 16px;">
                <li class="breadcrumb-item active">Incoming Document</li>
                <li class="breadcrumb-item active"><a href="{{ route('user.document') }}" class="breadcrumb-item "  aria-current="page">View Documents</a></li>
            </ol>
        </nav> 
        
        @if(Session::get('fail'))
            <div class="alert alert-danger">{{ Session::get('fail')}}</div>
        @endif
   
        <div class="conatiner fade-in" >
            <form action="{{ route('admin.savedoc') }}"  method="post" enctype="multipart/form-data" class="myform">
            {{ csrf_field() }}
                 
                <div class="card">
                    <div class="card-header pb-0">
                        <h5>Document Details</h5>
                    </div>
                    <div class="card-body">
                         <div class="card-body" style="padding: 20px 200px 20px 20px;">
                            <div class="form-group row">
                                <label for="subject" class="col-sm-2 col-form-label required" style="text-align: right">Subject</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('subject') is-invalid @enderror" id="subject" name="subject" value="{{old('subject')}}">
                                </div>
                                @error('subject')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            
                            {{-- <div class="form-group row">
                                <label for="doc_type" class="col-sm-2 col-form-label required" style="text-align: right">Document Type:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('doc_type') is-invalid @enderror" id="doc_type" name= "doc_type" value="{{old('doc_type')}}" >
                                </div>
                                @error('doc_type')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div> --}}

                            <div class="form-group row">
                                <label for="doc_type" class="col-sm-2 col-form-label required" style="text-align: right">Document Type:</label>
                                <div class="col-sm-10">
                                    <select class="form-control  @error('doc_type') is-invalid @enderror" name="doc_type">
                                        <option disabled selected value> -- select a document type -- </option>
                                        @foreach ($doc_types as $doc_type)
                                            <option value="{{ $doc_type->doc_type }}">{{ $doc_type->doc_type }}</option>
                                        @endforeach
                                        
                                    </select>
                                </div>
                                @error('doc_type')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            
                            
                            <div class="form-group row">
                                <label for="exampleFormControlFile1" class="col-sm-2 col-form-label required" style="text-align: right">File Attachment:</label>
                                <div class="col-sm-10">
                                    <input type="file" class="form-control-file" id="exampleFormControlFile1" style="width: 210px;" name="files[]" multiple value="{{old('files')}} ">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="doc_date" class="col-sm-2 col-form-label required" style="text-align: right">Document Date:</label>
                                <div class="col-sm-10">
                                <input type="date" class="form-control @error('doc_date') is-invalid @enderror" id="doc_date" style="width: 250px;" name="doc_date"  value="{{old('doc_date')}}">
                                </div>
                                 @error('doc_date')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                           

                            <div class="form-group row">
                                <label for="date_receive" class="col-sm-2 col-form-label required" style="text-align: right">Date Receive:</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control @error('date_receive') is-invalid @enderror" id="date_receive" style="width: 250px;" name="date_receive" value="{{old('date_receive')}}">
                                </div>
                                @error('date_receive')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            
                            
                            <div class="form-group row">
                                <label for="sender" class="col-sm-2 col-form-label required" style="text-align: right">Sender:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('sender') is-invalid @enderror" id="sender" name="sender" value="{{old('sender')}}">
                                </div>
                                @error('sender')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            

                            <div class="form-group row">
                                <label for="company" class="col-sm-2 col-form-label required" style="text-align: right">Company:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('company') is-invalid @enderror" id="company" name="company" value="{{old('company')}}">
                                </div>
                                @error('company')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            
                        </div>
                    </div>
                </div>

                <div class="card">
                    <div class="card-header pb-0">
                        <h5>Routing Details</h5>
                    </div>
                    <div class="card-body">
                        <div class="card-body" style="padding: 20px 200px 20px 20px;">
                            <div class="form-group row">
                                <label for="slip" class="col-sm-2 col-form-label" style="text-align: right">Route Slip:</label>
                                <div class="col-sm-10">
                                <?php
                                    $max_id = DB::table('routing_details')->max('route_id');
                                    $id = ($max_id+1);
                                ?>
                                    <input type="text" class="form-control" id="slip" name="rslip" value="2021-IN06-ORD-<?php echo $id?>" readonly>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="from" class="col-sm-2 col-form-label required" style="text-align: right">From:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control @error('company') is-invalid @enderror" id="from" name="rfrom" value="{{old('rfrom')}}">
                                </div>
                                @error('rfrom')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            @if (auth()->user()->usergroup == "Regional Director")
                            <div class="form-group row">
                                <label for="rthru" class="col-sm-2 col-form-label required" style="text-align: right">Route Thru:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="rthru" name="routeThru" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="rto" class="col-sm-2 col-form-label required" style="text-align: right">Route To:</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="rto" name="routeTo">
                                </div>
                            </div>

                            <div class="form-group row"  style="margin-top: 20px;" >
                                <label  class="col-sm-2 col-form-label required"  style="text-align: right">Action Needed:</label>
                                <div class="input-group-prepend">
                                    <div class="col-sm-10" style="margin-top: 10px;">
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input" name="action" value="Please rush" id="Check1" onclick="validate(this)">
                                            <label class="form-check-label" for="Check1" >Please rush</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please attend" id="Check2" onclick="validate(this)">
                                            <label class="form-check-label" for="Check2">Please attend</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please  drop reply/memo/letter" id="Check3" onclick="validate(this)">
                                            <label class="form-check-label" for="Check3">Pleas drop reply/memo/letter</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please acknowledge receipt" id="Check4" onclick="validate(this)">
                                            <label class="form-check-label" for="Check4">Please acknowledge receipt</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please discuss with me" id="Check5" onclick="validate(this)">
                                            <label class="form-check-label" for="Check5">Please discuss with me</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please follow up" id="Check6" onclick="validate(this)">
                                            <label class="form-check-label" for="Check6">Please follow up</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please act on this" id="Check7" onclick="validate(this)">
                                            <label class="form-check-label" for="Check7">Please act on this</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please post" id="Check8" onclick="validate(this)">
                                            <label class="form-check-label" for="Check8">Please post</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please give me feedback" id="Check9" onclick="validate(this)">
                                            <label class="form-check-label" for="Check9">Please give me feedback</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="Please file" id="Check10" onclick="validate(this)">
                                            <label class="form-check-label" for="Check10">Please file</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="For your info/study/reference" id="Check11" onclick="validate(this)">
                                            <label class="form-check-label" for="Check11">For your info/study/reference</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="For your comments" id="Check12" onclick="validate(this)">
                                            <label class="form-check-label" for="Check12">For your comments</label>
                                            <br>
                                            <input type="checkbox" class="form-check-input" name="action" value="For your initial/signature" id="Check13" onclick="validate(this)">
                                            <label class="form-check-label" for="Check13">For your initial/signature</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            
                        </div>
                    </div>
                </div>
                
               

               
                <input type="submit" class="btn btn-primary btn-lg btn-block" value="Submit" style="margin: 0px 0px 30px 0px;" name="submit">
            </form>
        </div>
        <!-- end div fade-in  -->

    </div>
</div>
@endsection

@section('after-scripts')

@endsection