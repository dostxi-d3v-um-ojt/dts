<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar" style="font-size: 16px;">
    <div class="c-sidebar-brand d-lg-down-none">
       
    </div>
    <ul class="c-sidebar-nav ps">
        <li class="c-sidebar-nav-item">
            <!-- checking the roles of the currently logged in user  -->
            @role('Regional Director')
                <a class="c-sidebar-nav-link" href="{{url('admin/dashboard')}}">
            @endrole
            @role('Document Controller|Ordinary User Role|Division Head|Section Head')
                <a class="c-sidebar-nav-link" href="{{url('user/dashboard')}}">
            @endrole
                    <i class="c-sidebar-nav-icon fa-solid fa-gauge"></i>Dashboard
                </a>
        </li>
        
        <!-- sidebar for administrators  -->
        @role('Regional Director')
        <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#docs">
                <i class="c-sidebar-nav-icon fa-solid fa-file"></i>Documents
            </a>
            @if (auth()->user()->usergroup == "Regional Director")
            <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item text-value-md">
                    <a class="c-sidebar-nav-link " href="{{ route('admin.documents')}}" style="font-size: 15px;">
                         <i class="c-sidebar-nav-icon cil-address-book"></i> Pending Documents
                    </a>
                </li>
                 <li class="c-sidebar-nav-item text-value-md">
                    <a class="c-sidebar-nav-link " href="{{ route('admin.routed')}}" style="font-size: 15px;">
                        <i class="c-sidebar-nav-icon cil-map"></i> Routed Document 
                    </a>
                </li>
                 <li class="c-sidebar-nav-item text-value-md">
                    <a class="c-sidebar-nav-link " href="{{ route('admin.displaydocumentreceive') }}" style="font-size: 15px;">
                        <i class="c-sidebar-nav-icon fa-solid fa-file-circle-check"></i> Received Document 
                    </a>
                </li>
            </ul>
            @endif
        </li>
       
        <li class="c-sidebar-nav-item" >
            
            <a href="{{ route('admin.showUser') }}" class="c-sidebar-nav-link"><i class="c-sidebar-nav-icon fa-solid fa-users"></i>Users</a>
        </li>
        <li class="c-sidebar-nav-item" >
            <a href="/underdevelpoment" class="c-sidebar-nav-link"><i class="c-sidebar-nav-icon fa-solid fa-arrow-trend-up"></i>Statistics</a>
        </li>
        <li class="c-sidebar-nav-item" >
            <a href="/underdevelpoment" class="c-sidebar-nav-link"><i class="c-sidebar-nav-icon fa-solid fa-user-group"></i>Employees</a>
        </li>
       
        @endrole

        

        <!-- sidebar for staffs eg doc con or ordinary users  -->
        @role('Document Controller')
       
            @if (auth()->user()->usergroup == "Document Controller")
             <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#docs">
                    <i class="c-sidebar-nav-icon fa-solid fa-file"></i>Documents
                </a>
                        <ul class="c-sidebar-nav-dropdown-items">
                            <li class="c-sidebar-nav-item">
                                <a class="c-sidebar-nav-link " href="{{ route('user.incoming_doc') }}" style="font-size: 15px;">
                                    <i class="c-sidebar-nav-icon fa-solid fa-right-from-bracket"></i> Incoming Document
                                    
                                </a>
                            </li>
                        </ul>
                        <ul class="c-sidebar-nav-dropdown-items">
                            <li class="c-sidebar-nav-item">
                                <a class="c-sidebar-nav-link " href="{{ route('user.document') }}" style="font-size: 15px;">
                                    <i class="c-sidebar-nav-icon fa-solid fa-file-circle-check"></i> Recent Document
                                </a>
                            </li>
                        </ul>
                         
             </li>
            @endif
             <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#notif">
                <i class="c-sidebar-nav-icon fa-solid fa-bell"></i>Notifications
            </a>
            <ul class="c-sidebar-nav-dropdown-items text-value-md">
                <li class="c-sidebar-nav-items text-value-md">
                    <a class="c-sidebar-nav-link" href="/underdevelpoment">
                        <i class="c-sidebar-nav-icon cil-envelope-closed"></i> Messages
                    </a>
                </li>
            </ul>
            <ul class="c-sidebar-nav-dropdown-items">
                            <li class="c-sidebar-nav-item">
                                <a class="c-sidebar-nav-link text-value-md" href="{{ route('user.pending') }}">
                                    <i class="c-sidebar-nav-icon cil-address-book"></i> Pending Documents
                                </a>
                            </li>
            </ul>

            {{-- <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item text-value-md">
                    <a class="c-sidebar-nav-link " href="/underdevelpoment">
                        <i class="c-sidebar-nav-icon cil-library"></i> Routed to Me
                    </a>
                </li>
            </ul> --}}

            <li class="c-sidebar-nav-item" >
                <a href="/user/myprofile/{{auth()->user()->id}}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fa-solid fa-user"></i></i>My Profile</a>
            </li>

        </li> 
        @endrole
        @role('Ordinary User Role|Division Head|Section Head')
            
            @if (auth()->user()->usergroup == "Division Head" || auth()->user()->usergroup == "Ordinary User" || auth()->user()->usergroup == "Section Head" )
            <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
                <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#docs">
                    <i class="c-sidebar-nav-icon fa-solid fa-file"></i>Documents
                </a>
                 <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link text-value-md" href="{{ route('user.pending') }}">
                            <i class="c-sidebar-nav-icon cil-address-book"></i> Pending Documents
                        </a>
                    </li>
                </ul>
                <ul class="c-sidebar-nav-dropdown-items">
                    <li class="c-sidebar-nav-item">
                        <a class="c-sidebar-nav-link text-value-md" href="{{route('user.receive')}}">
                            <i class="c-sidebar-nav-icon cil-check-circle"></i> Received Documents
                        </a>
                    </li>
                </ul>
            </li>
        
            @endif
             <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
            <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#notif">
                <i class="c-sidebar-nav-icon fa-solid fa-bell"></i>Notifications
            </a>
            <ul class="c-sidebar-nav-dropdown-items text-value-md">
                <li class="c-sidebar-nav-items text-value-md">
                    <a class="c-sidebar-nav-link" href="/underdevelpoment">
                        <i class="c-sidebar-nav-icon cil-envelope-closed"></i> Messages
                    </a>
                </li>
            </ul>

            {{-- <ul class="c-sidebar-nav-dropdown-items">
                <li class="c-sidebar-nav-item text-value-md">
                    <a class="c-sidebar-nav-link " href="/underdevelpoment">
                        <i class="c-sidebar-nav-icon cil-library"></i> Routed to Me
                    </a>
                </li>
            </ul> --}}

            <li class="c-sidebar-nav-item" >
                <a href="/user/myprofile/{{auth()->user()->id}}" class="c-sidebar-nav-link">
                <i class="c-sidebar-nav-icon fa-solid fa-user"></i></i>My Profile</a>
            </li>

        </li> 
        @endrole
        <div class="ps__rail-x" style="left: 0px; bottom: 0px;">
            <div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div>
        </div>
        <div class="ps__rail-y" style="top: 0px; right: 0px;">
            <div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div>
        </div>
    </ul>
    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div>