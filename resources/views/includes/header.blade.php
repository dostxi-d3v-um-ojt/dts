<header class="c-header c-header-dark c-header-fixed c-header-with-subheader">
    <button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
        <i class="c-icon c-icon-lg cil-menu"></i>
    </button>

    <button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
        <i class="c-icon c-icon-lg cil-menu"></i>
      
    </button>

    <ul class="c-header-nav d-md-down-none" >
        <h3><li class="c-header-nav-item px-3" > <a class="c-header-nav-link " href="{{ route('admin.dashboard') }}">TRACE</a> </li></h3>

        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">&nbsp;</a></li>
        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">&nbsp;</a></li>
        <li class="c-header-nav-item px-3"><a class="c-header-nav-link" href="#">&nbsp;</a></li>
    </ul>

    <ul class="c-header-nav ml-auto mr-4">
       
        <li class="c-header-nav-item dropdown">
            <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <div class="c-avatar"><img class="c-avatar-img" src="{{(auth()->user()->picture != null) ? asset('uploads/'.auth()->user()->picture) : asset ('profile/no-user-photo.png')}}" alt="user@email.com" style="width:40px; height: 35px"></div>
            </a>
            <div class="dropdown-menu dropdown-menu-right pt-0">
                @role('Regional Director')
                <a class="dropdown-item" href="/admin/myprofile/{{auth()->user()->id}}">
                   <i class="c-sidebar-nav-icon fa-solid fa-user"></i> My Profile
                </a>
                <a class="dropdown-item" href="{{ route('admin.actions')}}">
                   <i class="c-sidebar-nav-icon c-sidebar-nav-icon fa-solid fa-gear"></i> Action Settings
                </a>
                 <a class="dropdown-item" href="{{ route('admin.types') }}">
                   <i class="c-sidebar-nav-icon fa-regular fa-file"></i> Document Types
                </a>
                @endrole
                <a class="dropdown-item" href="{{ route('auth.logout') }}">
                   <i class="c-sidebar-nav-icon cil-account-logout"></i> Logout
                </a>
            </div>
        </li>
    </ul>
   
</header>