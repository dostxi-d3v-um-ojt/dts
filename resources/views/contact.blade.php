<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css"
          integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog=="
          crossorigin="anonymous"/>
</head>
<body class="bg-white text-dark">
    <header class="c-header c-header-light px-3">
        <ul class="c-header-nav ">
            <li class="c-header-nav-item ">
                <a class="c-header-brand text-decoration-none" href="{{ route('auth.login') }}">
                    <h3 class="pt-2 ">TRACE</h3>
                </a>
            </li>
        </ul>    

        <ul class="c-header-nav ml-auto mr-3">
        <li class="c-header-nav-item active">
            <a class="c-header-nav-link mr-4 font-weight-bold" >Contact Info <span class="sr-only">(current)</span></a>
        </li>
        <li class="c-header-nav-item mr-4">
            <a class="c-header-nav-link" href="{{ route('about') }}">About Us</a>
        </li>
        <li class="c-header-nav-item ">
            <a class="c-header-nav-link"  href="{{ route('auth.login') }}">Login</a>
        </li>
        </ul>
    </header>

    <div class="container-fluid align-items-center fade-in">
        <h1 class="text-center  mt-5 mb-5" style=" color: #212121;">CONTACT INFORMATION</h1>
        <!-- <div class="card" style="width: 55%; height: 400px; margin: 80px 0px 0px 60px;">
            <div class="card-header ">

            </div>
            <div class="card-body" style="color: #212121;">
                 <h3>The System</h3>
                <p>Document Tracking System (DTS) is designed to handle document transactions inside a company/organization. 
                   It is centered in providing users an efficient and effective way of routing their documents instantly. </p> -->
            </div>
        </div> -->
        
    </div>
</body>
    <div class="container" style="margin-top: 100px;">
        <div class=" p-2">
             <div class="c-footer  bg-white" style="padding-left: 37%;">
                 Copyright &copy; {{ date('Y') }}
                <a href="https://region11.dost.gov.ph"> &nbsp; DOSTrack</a>, All rights reserved
            </div>
        </div>   
    </div>
</html>