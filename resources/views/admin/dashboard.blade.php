@extends('layouts.app')
@section('after-styles')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

@endsection
@section('content')

            <div class="container-fluid fade-in ">
                <div class="row d-flex justify-content-center">
                    <div class="col-sm-5 col-lg-4">
                        <div class="card shadow">
                            <div class="card-header bg-youtube content-center text-uppercase large text-white">
                                <i class="c-header-nav "></i> Documents
                            </div>
                            <div class="card-body row text-center">
                                <div class="col">
                                    <div class="text-value-xl">{{count($routedocuments) + count($documents)}}</div>
                                    <div class="text-uppercase text-muted small">Incoming documents</div>
                                </div>
                                <div class="c-vr"></div>
                                <div class="col">
                                    <a href="{{ route('admin.routed')}}" class="text-value-xl text-dark">{{ count($routedocuments)}}</a>
                                    <div class="text-uppercase text-muted small">Routed Documents</div>
                                </div>
                            </div>
                        </div>
                    </div>
                 <div class="col-lg-4">
                    <div class="card shadow">
                        <div class="card-header bg-yahoo content-center text-uppercase large text-white">
                           <i class="c-header-nav "></i> Notifications
                        </div>
                        <div class="card-body row text-center">
                            <div class="col">
                                <a href="{{ route('admin.displaycomments')}}" class="text-value-xl text-dark">{{ count($comments)}}</a>
                                <div class="text-uppercase text-muted small">Comments</div>
                            </div>
                            <div class="c-vr"></div>
                            <div class="col">
                                <a href="{{ route('admin.documents')}}" class="text-value-xl text-dark">{{ count($documents)}}</a>
                                <div class="text-uppercase text-muted small">pending documents</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-6">
                    <div class="card mb-4 shadow">
                        <div class="card-header text-center">
                            <i class="fas fa-chart-area me-1"></i>
                                        <Strong>Documents Added Per Day</Strong>
                            </div>
                        <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
                    </div>
                </div>
            <div class="col-xl-6">
                <div class="card mb-4 shadow">
                    <div class="card-header text-center">
                            <i class="fas fa-chart-bar me-1"></i>
                                        <Strong>Documents Added Per Month</Strong>
                    </div>
                    <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card mb-4 shadow">
                    <div class="card-header text-center">
                            <i class="fas fa-chart-bar me-1"></i>
                                        <Strong>Received Documents vs Unreceived Documents</Strong>
                    </div>
                    <div class="card-body"><canvas id="myPieChart" width="100%" height="40"></canvas></div>
                </div>
            </div>
        </div>
        </div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
<script type="text/javascript">
    var _ydata = JSON.parse('{!! json_encode($months) !!}');
    var _xdata = JSON.parse('{!! json_encode($monthCount) !!}');

    var _yddata = JSON.parse('{!! json_encode($dmonths) !!}');
    var _xddata = JSON.parse('{!! json_encode($dmonthCount) !!}');

    var _xpie  = JSON.parse('{!! json_encode($_xpie) !!}');
</script>
<script src="{{asset('assets/demo/chart-area-demo.js')}}"></script>
<script src="{{asset('assets/demo/chart-bar-demo.js')}}"></script>
<script src="{{asset('assets/demo/chart-pie-demo.js')}}"></script>
</div>
@endsection


@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script> $(function () { var table = $('.yajra-datatable').DataTable(); }); </script>

@endsection