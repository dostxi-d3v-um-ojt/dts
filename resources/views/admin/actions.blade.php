@extends('layouts.app')
@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
   
    <div class="container-fluid" style="font-size: 16px;">
        <div class="fade-in">
            @role('Regional Director')
                <button type="button" class="btn btn-success" style="float:right;" data-bs-toggle="modal" data-bs-target="#exampleModal">
                  Create Action
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Action</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form action="/admin/saveaction" method="post">
                        @csrf
                        <div class="modal-body">
                          <label for="action" class="col-sm-2 col-form-label required" style="text-align: right">Action</label>
                          <input type="text" class="form-control @error('action') is-invalid @enderror" id="action" name="action" value="{{old('action')}}">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save action</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            @endrole
            <h2 class="card-title" >Actions settings</h2> 
    
             <div class="card">
                <div class="card-body">
                    <div class="flex">
                        <div class="container-fluid mt-5">
                            <table class="table table-bordered yajra-datatable" >
                            <!-- yajra-datatable -->
                                <thead>
                                    <tr>
                                        <th>Action ID</th>
                                        <th>Actions</th>
                                        <th>Options</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($actions as $action)
                                    <tr>
                                        <td>{{ $action->id }}</td>
                                        <td>{{ $action->action }}</td>
                                            <td>
                                                <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal{{$action->id}}">
                                                  Edit action
                                                </button>
                                                <!-- EditModal -->
                                                <div class="modal fade" id="exampleModal{{$action->id}}" tabindex="-1" aria-labelledby="exampleModalLabel{{$action->id}}" aria-hidden="true">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel{{$action->id}}">Edit Action</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                      </div>
                                                          <form action="/admin/updateaction/{{$action->id}}" method="post">
                                                            @csrf
                                                            <div class="modal-body">
                                                              <label for="action" class="col-sm-2 col-form-label required" style="text-align: right">Action</label>
                                                              <input type="text" class="form-control @error('action') is-invalid @enderror" id="action" name="action" value="{{$action->action}}">
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                              <button type="submit" class="btn btn-primary">Update action</button>
                                                            </div>
                                                          </form>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                            </table>
                        </div>

                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    
@endsection
@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script>
$(function () {
    var table = $('.yajra-datatable').DataTable();
    table.page( 'next' ).draw( true );
});
   
</script>
@endsection