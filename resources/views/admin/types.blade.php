@extends('layouts.app')
@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
   
    <div class="container-fluid" style="font-size: 16px;">
        <div class="fade-in">
            @role('Regional Director')
                <button type="button" class="btn btn-success" style="float:right;" data-bs-toggle="modal" data-bs-target="#exampleModal">
                  Create Document Type
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create Document Type</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                      </div>
                      <form action="/admin/savedoctype" method="post">
                        @csrf
                        <div class="modal-body">
                          <label for="doc_type" class="col-sm-2 col-form-label required" style="text-align: right">Document_Type</label>
                          <input type="text" class="form-control @error('doc_type') is-invalid @enderror" id="doc_type" name="doc_type" value="{{old('doc_type')}}">
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                          <button type="submit" class="btn btn-primary">Save Document Type</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
            @endrole
            <h2 class="card-title" >Document Type settings</h2> 
    
             <div class="card">

                @if(Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                      {{ Session::get('success')}}
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                @endif

                <div class="card-body">
                    <div class="flex">
                        <div class="container-fluid mt-5">
                            <table class="table table-bordered yajra-datatable" >
                            <!-- yajra-datatable -->
                                <thead>
                                    <tr>
                                        <th>Document Type</th>
                                        <th>Document Count</th>
                                        <th>Option</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($doc_types as $doc_type)
                                    <tr>
                                        <td>{{ $doc_type->doc_type }}</td>
                                        <td>{{ $doc_type->count }}</td>
                                            <td>
                                                <button type="button" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal{{$doc_type->id}}">
                                                  Edit
                                                </button>
                                                <!-- EditModal -->
                                                <div class="modal fade" id="exampleModal{{$doc_type->id}}" tabindex="-1" aria-labelledby="exampleModalLabel{{$doc_type->id}}" aria-hidden="true">
                                                  <div class="modal-dialog">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel{{$doc_type->id}}">Edit Document Type</h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                      </div>
                                                          <form action="/admin/updatedocumenttype/{{$doc_type->id}}" method="post">
                                                            @csrf
                                                            <div class="modal-body">
                                                              <label for="doc_type" class="col-lg-6 col-form-label required" style="text-align: left">Document Type</label>
                                                              <input type="text" class="form-control @error('doc_type') is-invalid @enderror" id="doc_type" name="doc_type" value="{{$doc_type->doc_type}}">
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                              <button type="submit" class="btn btn-primary">Update action</button>
                                                            </div>
                                                          </form>
                                                        </div>
                                                    </div>
                                                  </div>
                                            </td>
                                    </tr>
                                    @endforeach
                            </tbody>
                            </table>
                        </div>

                    </div> 
                </div>
            </div>
        </div>
    </div>
    
    
@endsection
@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<script>
$(function () {
    var table = $('.yajra-datatable').DataTable();
    table.page( 'next' ).draw( true );
});
   
</script>
@endsection