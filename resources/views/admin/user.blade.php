@extends('layouts.app')
@section('after-styles')
    <link href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
   
    <div class="container-fluid" style="font-size: 16px;">
        <div class="fade-in">
            @role('Regional Director')
                <div class="card-title" style="margin-top: 40px;"><a class="btn btn-success " href="{{ route('admin.addUser') }}" style="float:right;">Create</a></div>
            @endrole
            <h2 class="card-title" >User Accounts</h2> 
    
             <div class="card">
                <div class="card-body">
                    <div class="flex">
                        <div class="container-fluid mt-5">
                            <table class="table table-bordered yajra-datatable" >
                            <!-- yajra-datatable -->
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Firstname</th>
                                        <th>Lastname</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user as $users)
                                    <tr>
                                        <td>{{ $users->id }}</td>
                                        <td>{{ $users->fname }}</td>
                                        <td>{{ $users->lname }}</td>
                                        <td>{{ $users->email }}</td>
                                        <td>{{ $users->role }}</td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="/admin/edit/{{ $users->id }}" ><span class="cil-pencil btn-icon mr-1"></span>Edit</a>
                                                <a class="btn btn-danger btn-sm" href="" data-toggle="modal" data-target="#exampleModal{{ $users->id }}"><span class="cil-remove btn-icon mr-1"></span>Delete</a>
                                            </td>
                                    </tr>
                                    
                                    <!-- The Modal -->
                                    <div class="modal fade" id="exampleModal{{ $users->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <form action="{{ route('admin.accountdelete',$users->id)}}" method="post">
                                    @csrf
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <strong><p>User: {{ $users->fname }}</p></strong>
                                                <strong><p>Role: {{ $users->role }}</p></strong>
                                                <p>Are you sure you want to delete this record?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                    </div>
                                    @endforeach
                            </tbody>
                            </table>
                        </div>

                    </div> 
                </div>
            </div>
        </div>
    </div>
    
@endsection
@section('after-scripts')
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function () {
    var table = $('.yajra-datatable').DataTable();
    table.page( 'next' ).draw( true );
});
   
</script>
@endsection