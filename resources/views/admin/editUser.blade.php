@extends('layouts.app')
@section('after-styles')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="fade-in">
       

    <div class="fade-in">
        <div class="conatiner" >
            <div class="card-body bg-white border mb-5 " style="width: 700px; margin: 50px 0px 0px 25%;">
                <div class="card-body mb-4" >
                    <h2 class="mb-5 text-center">Edit User Account</h2>
                     @foreach($user as $users)
                    <form action="/admin/saveEdit/{{ $users->id }}" method="post">
                        @csrf
                        <div class="form-group row "  >
                            <label for="subject" class="col-sm-2 col-form-label"  style="text-align: right" >Firstname:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fname" name="fname" autocomplete="on" value="{{ $users->fname }}"required>
                            </div>
                        </div>
                        
                        <div class="form-group row "  >
                            <label for="subject" class="col-sm-2 col-form-label"  style="text-align: right" >Lastname:</label>
                            <div class="col-sm-10 ">
                                <input type="text" class="form-control  " id="lname" name="lname" autocomplete="on" value="{{ $users->lname }}"required>
                            </div>
                        </div>
                        <div class="form-group row "  >
                            <label for="subject" class="col-sm-2 col-form-label"  style="text-align: right" >Email:</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="email" name="email" autocomplete="on" 
                                value="{{ $users->email }}"required>
                                
                            </div>
                            
                        </div>
                        <div class="form-group row"  >
                            <label for="subject" class="col-sm-2 col-form-label"  style="text-align: right" >Group:</label>
                            <div class="col-sm-10">
                                <select class="form-control" name="usergroup" id="usergroup"> 
                                    <option value="{{$users->usergroup}}" selected disabled>{{$users->usergroup}}</option>
                                    <option value="Regional Director">Regional Director</option>
                                    <option value="Division Head">Division Head</option>
                                    <option value="Section Head">Section Head</option>
                                    <option value="Document Controller">Document Controller</option>
                                    <option value="Ordinary User">Ordinary User</option>
                                </select>
                            </div>
                        </div>
                        {{-- <div class="form-group row"  >
                            <label for="subject" class="col-sm-2 col-form-label"  style="text-align: right" >Role:</label>
                            <div class="col-sm-10">
                                @foreach($userole as $role)
                                <select class="form-control" id="role" name="role" style="width: 150px;">
                                    <option value="admin" <?php if($role->role_id == '2' ) {echo("selected");}?> >Administrator</option>
                                    <option value="staff" <?php if($role->role_id == '1') {echo("selected");}?> >Staff</option>
                                </select>
                                @endforeach
                            </div>
                        </div> --}}
                        @endforeach
                        <button type="submit" class="btn btn-primary" style="float: right">Update</button>
                        <a href="{{ route('admin.showUser') }}" style="float: right; margin-right: 10px;"><button type="button" class="btn btn-primary">Cancel</button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>
   

</div>
    


@endsection