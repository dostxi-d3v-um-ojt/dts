@extends('layouts.app')
@section('after-styles')
<link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('content')
<div class="container-fluid">
    <div class="fade-in">
      

        <div class="conatiner" >
            <div class="card-body bg-white border mb-5" style="width: 700px; margin: 50px 0px 0px 25%;">
            <div  class="card-body" >
                <h2 class="mb-5 text-center">Create User Account</h2>
                <form method="post" action="{{ route('admin.saveaccount') }}" style="width: 500px; display: inline-block; margin-left: 10%;" >
                @csrf
                    @if(Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ Session::get('success')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                    @endif
                    @if(Session::get('fail'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ Session::get('fail')}}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                    @endif
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="cil-user"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control @error('firstname') is-invalid @enderror"
                            name="firstname" value="{{ old('firstname') }}"placeholder="First Name">
                        @error('firstname')
                            <div class="invalid-feedback">{{ $message }}</div>
                         @enderror
                    </div>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="cil-user"></i>
                            </span>
                        </div>
                        <input type="text" class="form-control @error('lastname') is-invalid @enderror"
                            name="lastname" value="{{ old('firstname') }}"placeholder="Last Name">
                        @error('lastname')
                            <div class="invalid-feedback">{{ $message }}</div>
                         @enderror
                    </div>
                     <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="cil-envelope-open"></i>
                            </span>
                        </div>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"
                            name="email" value="{{ old('email') }}"placeholder="Email">
                        @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                         @enderror
                    </div>
                     <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="cil-lock-locked"></i>
                            </span>
                        </div>
                        <input type="password" class="form-control @error('password') is-invalid @enderror"
                            name="password" placeholder="Password">
                        @error('password')
                            <div class="invalid-feedback">{{ $message }}</div>
                         @enderror
                    </div>
                    <div class="input-group mb-3"  style="margin-bottom: 60px;">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="cil-people"></i>
                            </span>
                        </div>

                        <select class="form-control" id="action" name="usergroup">
                            <option value="Regional Director">Regional Director</option>
                            <option value="Division Head">Division Head</option>
                            <option value="Section Head">Section Head</option>
                            <option value="Document Controller">Document Controller</option>
                            <option value="Ordinary User">Ordinary User</option>
                        </select>
                        @error('usergroup')
                            <div class="invalid-feedback">{{ $message }}</div>
                         @enderror
                    </div>
                    <input type="submit" class="btn btn-primary" style="float: right">
                    <a href="{{ route('admin.showUser') }}"><button type="button" class="btn btn-secondary" style="float: right; margin-right: 20px;">Cancel</button></a>
                </form>
            </div>
            </div>
        </div>
   

</div>
    


@endsection