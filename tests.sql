-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2022 at 08:41 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tests`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `id` int(11) NOT NULL,
  `action` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id`, `action`) VALUES
(1, 'Please rush'),
(2, 'Please Attend'),
(3, 'Pleas drop reply/memo/letter'),
(4, 'Please acknowledge receipt'),
(5, 'Please discuss with me'),
(6, 'For your comments'),
(7, 'Please follow up'),
(8, 'Please see me immediately'),
(9, 'For your information/study'),
(10, 'For your initial/signature'),
(11, 'Please file'),
(12, 'Please give me feedback');

-- --------------------------------------------------------

--
-- Table structure for table `attachments_lists`
--

CREATE TABLE `attachments_lists` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `file_name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_id` int(10) UNSIGNED DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `document_details`
--

CREATE TABLE `document_details` (
  `doc_id` int(10) UNSIGNED NOT NULL,
  `users_id` bigint(20) UNSIGNED NOT NULL,
  `subject` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_type` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `document_date` date DEFAULT NULL,
  `date_received` date DEFAULT NULL,
  `route_id` int(10) UNSIGNED DEFAULT 0,
  `sender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `document_types`
--

CREATE TABLE `document_types` (
  `id` int(11) NOT NULL,
  `doc_type` varchar(225) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `document_types`
--

INSERT INTO `document_types` (`id`, `doc_type`) VALUES
(1, 'Letter'),
(2, 'Announcement'),
(3, 'Notice of Meeting'),
(4, 'Memorandom'),
(5, 'MOA'),
(6, 'Publication'),
(7, 'Resolution'),
(8, 'Administrative Order'),
(9, 'Special Order'),
(10, 'Minutes of Meeting'),
(11, 'Schedule'),
(12, 'Tax message'),
(13, 'Email Message'),
(14, 'Receipt'),
(15, 'Executive Order'),
(16, 'Primer'),
(17, 'Form');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_06_17_135428_create_routing_details_table', 1),
(5, '2021_06_18_022820_create_document_details_table', 1),
(6, '2021_06_18_033455_create_attachments_lists_table', 1),
(7, '2021_07_05_050614_create_permission_tables', 2),
(8, '2019_12_14_000001_create_personal_access_tokens_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_permissions`
--

INSERT INTO `model_has_permissions` (`permission_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 4),
(2, 'App\\Models\\User', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 21),
(1, 'App\\Models\\User', 22),
(1, 'App\\Models\\User', 48),
(1, 'App\\Models\\User', 49),
(2, 'App\\Models\\User', 47),
(2, 'App\\Models\\User', 50),
(3, 'App\\Models\\User', 4),
(3, 'App\\Models\\User', 52),
(4, 'App\\Models\\User', 53),
(4, 'App\\Models\\User', 56),
(6, 'App\\Models\\User', 57),
(7, 'App\\Models\\User', 55),
(7, 'App\\Models\\User', 58);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'add user', 'web', '2021-07-05 22:09:39', '2021-07-05 22:09:39'),
(2, 'edit user', 'web', '2021-07-05 22:18:13', '2021-07-05 22:18:13'),
(3, 'delete user', 'web', '2021-07-18 20:21:17', '2021-07-18 20:21:17'),
(4, 'create-users', 'web', '2022-07-22 01:49:15', '2022-07-22 01:49:15'),
(5, 'edit-users', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(6, 'delete-users', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(7, 'create-incoming-documents', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(8, 'view-incoming-documents', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(9, 'edit-incoming-documents', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(10, 'delete-incoming-documents', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(11, 'receive-incoming-documents', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(30, 'App\\Models\\User', 45, 'myapptoken', '89d882056ed709330e5ee2901593b3516f6ba0c98d15082d5a797f239d2fa91e', '[\"*\"]', NULL, '2022-07-14 19:09:39', '2022-07-14 19:09:39'),
(31, 'App\\Models\\User', 46, 'myapptoken', '2fb6ee0eb3deb824da8a3d0dcbe852c686e06d8fa7422ed1d64d44ef1396ee2a', '[\"*\"]', NULL, '2022-07-14 19:11:16', '2022-07-14 19:11:16'),
(32, 'App\\Models\\User', 1, 'myapptoken', '5e6a4d5ec9e4b4b0563af6968adf5de9fc56a6c35035e58926a8c837c8f03ea6', '[\"*\"]', NULL, '2022-07-14 19:19:42', '2022-07-14 19:19:42'),
(33, 'App\\Models\\User', 2, 'myapptoken', '8f65c55b87f657af2a5d10860298b97d6efa567c36f7a41333881a519f7d963b', '[\"*\"]', NULL, '2022-07-14 19:24:44', '2022-07-14 19:24:44'),
(34, 'App\\Models\\User', 2, 'myapptoken', '9237e80509aa0692471378d7067bf154ace191e9e1f4e3f5e424123acac96893', '[\"*\"]', NULL, '2022-07-14 19:25:10', '2022-07-14 19:25:10'),
(35, 'App\\Models\\User', 1, 'myapptoken', '0fb4ffc4e3f53fef53989f1aa7d27d95b78ccfd7ece18c562210106bacf0e307', '[\"*\"]', NULL, '2022-07-14 20:00:00', '2022-07-14 20:00:00'),
(36, 'App\\Models\\User', 1, 'myapptoken', '3fd9e74552ce05fa21559a7c25da0348f5634e6789c75791b1ef582473abd4e6', '[\"*\"]', NULL, '2022-07-14 20:03:45', '2022-07-14 20:03:45'),
(37, 'App\\Models\\User', 2, 'myapptoken', '69aeaa89d3d1ab9402e957c1a7b18aec945598e457071a322632a5fcdd7e4ca3', '[\"*\"]', NULL, '2022-07-14 20:04:08', '2022-07-14 20:04:08'),
(38, 'App\\Models\\User', 2, 'myapptoken', 'aba5f2571a817f3957857c47725359d16814c8233a0dda027c277e3e898fe5ea', '[\"*\"]', NULL, '2022-07-14 20:04:49', '2022-07-14 20:04:49'),
(39, 'App\\Models\\User', 1, 'myapptoken', 'fd4122994a0a1ae7d447120828f9b021e82a8ba2ced736d1faa308822d41ed75', '[\"*\"]', NULL, '2022-07-14 20:16:34', '2022-07-14 20:16:34'),
(40, 'App\\Models\\User', 1, 'myapptoken', '12adec99a37fef1a09665790208df99099809d5615487e782431b3a981ae820c', '[\"*\"]', NULL, '2022-07-14 20:17:20', '2022-07-14 20:17:20'),
(41, 'App\\Models\\User', 1, 'myapptoken', 'bd58ddbf4056dd68448e700221f10b1284ab8e447482aef27327b643c0f9133b', '[\"*\"]', NULL, '2022-07-14 20:19:09', '2022-07-14 20:19:09'),
(42, 'App\\Models\\User', 1, 'myapptoken', 'd3d87bd3328ca9b4e5cacb940758186a331abad474e46a5e934567ad8ad0409b', '[\"*\"]', NULL, '2022-07-14 22:53:34', '2022-07-14 22:53:34'),
(43, 'App\\Models\\User', 1, 'myapptoken', 'd372d4307080b170780e4e4a440e58ce50e03edde78e91c8b3b34f0c37759653', '[\"*\"]', NULL, '2022-07-14 23:06:47', '2022-07-14 23:06:47'),
(44, 'App\\Models\\User', 1, 'myapptoken', 'e9093f9012ca253d9058808dcb4ebddd35d594295fb8881c901b17bfc747f0ac', '[\"*\"]', NULL, '2022-07-14 23:21:51', '2022-07-14 23:21:51'),
(45, 'App\\Models\\User', 1, 'myapptoken', '2e9236eb8eb183571b3f1d6defbd760c0b906c744969fa99cea75089ff28a6e3', '[\"*\"]', NULL, '2022-07-14 23:23:24', '2022-07-14 23:23:24'),
(46, 'App\\Models\\User', 1, 'myapptoken', '8717d8cf908fa9e8b875da8a19377511d9fff4a0d321f0a41824abe7da3bf1d5', '[\"*\"]', NULL, '2022-07-14 23:36:33', '2022-07-14 23:36:33'),
(47, 'App\\Models\\User', 1, 'myapptoken', '6e3a27ae1af984c4c3069815e12549af7d3e4479d59db0b5276b1eaf1753c474', '[\"*\"]', NULL, '2022-07-14 23:37:36', '2022-07-14 23:37:36'),
(48, 'App\\Models\\User', 1, 'myapptoken', '1e624110afa113dfc1fdc76b35ff5b12b66234448637929576e147bc6f05cb01', '[\"*\"]', NULL, '2022-07-15 00:35:54', '2022-07-15 00:35:54'),
(49, 'App\\Models\\User', 1, 'myapptoken', 'ae25473c001d46b74b8d6c2e1b66c3eca44cb7d74d1b14cfd1634a7ff9ce6bb7', '[\"*\"]', NULL, '2022-07-15 00:45:22', '2022-07-15 00:45:22'),
(50, 'App\\Models\\User', 1, 'myapptoken', 'c923d68e08b4442cfa05b5fd62a18f91dd6ec750de241c393f4b0608248fdb94', '[\"*\"]', NULL, '2022-07-15 01:15:50', '2022-07-15 01:15:50'),
(51, 'App\\Models\\User', 1, 'myapptoken', 'ee9116224c453e7486e27f4a2f8a857875cf93fef881fdc506e88d4f968b7ab7', '[\"*\"]', NULL, '2022-07-15 01:27:00', '2022-07-15 01:27:00'),
(52, 'App\\Models\\User', 2, 'myapptoken', 'a524cd54b386cc3dc17fe4661593a774413bc515299ca049be64ff6973ccd400', '[\"*\"]', NULL, '2022-07-15 16:26:43', '2022-07-15 16:26:43'),
(53, 'App\\Models\\User', 1, 'myapptoken', 'ce8228a2225cdbb7ec7ac0817f80bf8a4c83b1341a988a9a527a6d3ed237a2ea', '[\"*\"]', NULL, '2022-07-15 16:27:27', '2022-07-15 16:27:27'),
(54, 'App\\Models\\User', 1, 'myapptoken', '066c335370cadcec2c21554d2c2e083871a2f9e4824253555c571fae5b41be58', '[\"*\"]', NULL, '2022-07-15 19:13:04', '2022-07-15 19:13:04'),
(55, 'App\\Models\\User', 1, 'myapptoken', '1b844b9db6c2421d4334250c23bf7828a18abde2756e7f2966da219a7611efbd', '[\"*\"]', NULL, '2022-07-15 19:15:40', '2022-07-15 19:15:40'),
(56, 'App\\Models\\User', 2, 'myapptoken', '39f4ae76d739ed8416c78664b85a1f9e4661a51ca76d6ce7720c0d6d47bc7117', '[\"*\"]', NULL, '2022-07-15 19:20:34', '2022-07-15 19:20:34'),
(57, 'App\\Models\\User', 2, 'myapptoken', '45cd05f6a2748a4ecc4576a157cbb51e7c9bade441454e9be48cd8492ca43679', '[\"*\"]', NULL, '2022-07-15 19:36:06', '2022-07-15 19:36:06'),
(58, 'App\\Models\\User', 2, 'myapptoken', '011e5600f980111b1473bf00e9fd40ba54b2a1b49259221cf940deee03fd7c02', '[\"*\"]', NULL, '2022-07-15 19:36:39', '2022-07-15 19:36:39'),
(59, 'App\\Models\\User', 2, 'myapptoken', '31e7fef41370f5e406e5e10ab9c8bc12261f46455dbfe8b9a9d8efdf31bfda61', '[\"*\"]', NULL, '2022-07-15 19:37:23', '2022-07-15 19:37:23'),
(60, 'App\\Models\\User', 2, 'myapptoken', '1c506352304df2a73b5f526fbce1504cd92011a10094c229a47b9324d5c042a9', '[\"*\"]', NULL, '2022-07-15 19:40:55', '2022-07-15 19:40:55'),
(61, 'App\\Models\\User', 2, 'myapptoken', 'cf40bcd7f667e503eedca51aaee980e40948c2a0bf1aaf0fef1133d4cd20e7fa', '[\"*\"]', NULL, '2022-07-15 19:42:34', '2022-07-15 19:42:34'),
(62, 'App\\Models\\User', 2, 'myapptoken', '87502ffaa051adce94ac8e5b74818bb022d5fbabcd8efa56901056b69719bdfe', '[\"*\"]', NULL, '2022-07-15 19:45:21', '2022-07-15 19:45:21'),
(63, 'App\\Models\\User', 1, 'myapptoken', 'd39053ec1ddc4e1855cac82db44de2de9f01917e4081b545fc2c7a2551a066e9', '[\"*\"]', NULL, '2022-07-15 19:46:59', '2022-07-15 19:46:59'),
(64, 'App\\Models\\User', 2, 'myapptoken', '38368cc4eed5cc2e44c1307573048b5297e40e020e7b91ab2189bd6ccb3e0139', '[\"*\"]', NULL, '2022-07-15 19:49:53', '2022-07-15 19:49:53'),
(65, 'App\\Models\\User', 2, 'myapptoken', 'd1796a5534decd01b8ec5439bbff02a0d71691de18662a4216757b1becd1ec34', '[\"*\"]', NULL, '2022-07-15 19:50:49', '2022-07-15 19:50:49'),
(66, 'App\\Models\\User', 2, 'myapptoken', '53fce02cb916172a7f753b7067dd2a50bad76835a580821cf143e11742435eee', '[\"*\"]', NULL, '2022-07-15 19:55:00', '2022-07-15 19:55:00'),
(67, 'App\\Models\\User', 2, 'myapptoken', '7747cb4a82e69775524d69b4529265625c722bce78000ca843f4116dfbe875d1', '[\"*\"]', NULL, '2022-07-15 20:21:28', '2022-07-15 20:21:28'),
(68, 'App\\Models\\User', 2, 'myapptoken', 'f412eeee0a224a6563a6a66786bb09a0926757fcd8804eefea22f62654ed1df6', '[\"*\"]', NULL, '2022-07-15 20:24:46', '2022-07-15 20:24:46'),
(69, 'App\\Models\\User', 1, 'myapptoken', 'e0560cf50d80c9f4418aa255af27020902ea007b091399904e0311b2a2d0290a', '[\"*\"]', NULL, '2022-07-15 20:26:20', '2022-07-15 20:26:20'),
(70, 'App\\Models\\User', 2, 'myapptoken', '432260a2aa3bb8b5aebfac4aa86f034a2ba92556b245133451e299b62ef357be', '[\"*\"]', NULL, '2022-07-15 20:26:39', '2022-07-15 20:26:39'),
(71, 'App\\Models\\User', 2, 'myapptoken', '749ab1633ec058b0c919637400c19c07b9b616608ccbde7fccaf5af3bbb80e59', '[\"*\"]', NULL, '2022-07-15 20:28:16', '2022-07-15 20:28:16'),
(72, 'App\\Models\\User', 1, 'myapptoken', '75fe24181fd20b559f7e8dcb7c41e127e52f6a5be7410a39c88d34ea8761ca1b', '[\"*\"]', NULL, '2022-07-15 20:28:36', '2022-07-15 20:28:36'),
(73, 'App\\Models\\User', 2, 'myapptoken', '18a834886e455356bfc0183e6349d534bce2dcf0c884951c0973b888d348f4d5', '[\"*\"]', NULL, '2022-07-15 21:16:56', '2022-07-15 21:16:56'),
(74, 'App\\Models\\User', 1, 'myapptoken', '760842d96b4d2e0226a391c2546cd21373bc2d1967c5afede388767a197b06b8', '[\"*\"]', NULL, '2022-07-15 21:17:38', '2022-07-15 21:17:38'),
(75, 'App\\Models\\User', 1, 'myapptoken', '163a732763968fa64043b2561a7c34b6116054be5f58bd6dfeede24aa6e65c32', '[\"*\"]', NULL, '2022-07-16 01:05:46', '2022-07-16 01:05:46'),
(76, 'App\\Models\\User', 1, 'myapptoken', '35b5c816cfdb53147ec46217295465bffd5dd7340ea8ec88e854e8220cb5a2c9', '[\"*\"]', NULL, '2022-07-16 01:15:29', '2022-07-16 01:15:29'),
(77, 'App\\Models\\User', 1, 'myapptoken', '41e5da64611c23db1a52c898437fd7b399776f8caf3eb3bb0a1b06a86f5e31b2', '[\"*\"]', NULL, '2022-07-16 01:21:41', '2022-07-16 01:21:41'),
(78, 'App\\Models\\User', 1, 'myapptoken', 'ae7880ad5a21a2f86e0a79cd05688e073402121cec64f6131514b87a2718b32a', '[\"*\"]', NULL, '2022-07-16 01:23:24', '2022-07-16 01:23:24'),
(79, 'App\\Models\\User', 1, 'myapptoken', '29524193e7afcf4a0766e0866f09b0ea99e5f2f62e5016ae0e3db1b07723b218', '[\"*\"]', NULL, '2022-07-16 01:29:32', '2022-07-16 01:29:32'),
(80, 'App\\Models\\User', 1, 'myapptoken', 'b05338a55ff1f747de93182e63e897c439cb63075a53ad838b08796bdfebf855', '[\"*\"]', NULL, '2022-07-16 01:29:33', '2022-07-16 01:29:33'),
(81, 'App\\Models\\User', 2, 'myapptoken', '4ade3238856668b6e9f72b9d39029cdefc33a863463c5a42e0c52c4c42623609', '[\"*\"]', NULL, '2022-07-16 01:37:08', '2022-07-16 01:37:08'),
(82, 'App\\Models\\User', 1, 'myapptoken', 'cb2de281e1d76b811300e5d1b7f4ee585efb0840a279bd05df4625dfc0f6dab2', '[\"*\"]', NULL, '2022-07-16 01:38:52', '2022-07-16 01:38:52'),
(83, 'App\\Models\\User', 2, 'myapptoken', 'badedb8efbc60c555cbb3e17ee9c7b427011bc73b305546ee58fd7947bd49e14', '[\"*\"]', NULL, '2022-07-16 01:40:09', '2022-07-16 01:40:09'),
(84, 'App\\Models\\User', 1, 'myapptoken', '0d1573eb50cfe8c640e40655eb09573db5a8b101c67636cd597667c99a64cad2', '[\"*\"]', NULL, '2022-07-16 01:42:07', '2022-07-16 01:42:07'),
(85, 'App\\Models\\User', 1, 'myapptoken', 'f6b6216d351a188f71342047698250eddc826ffd9520c911c06139f2e5dbac76', '[\"*\"]', NULL, '2022-07-16 01:42:40', '2022-07-16 01:42:40'),
(86, 'App\\Models\\User', 1, 'myapptoken', '5d5eefd1605fc3ea570c7170c209a8eae72d661120bb7138406e1a501f15d84e', '[\"*\"]', NULL, '2022-07-16 01:43:44', '2022-07-16 01:43:44'),
(87, 'App\\Models\\User', 2, 'myapptoken', 'e183dbef29122b7771bfd59904109451e975264438665e9e5cb4479c135cf0eb', '[\"*\"]', NULL, '2022-07-16 01:44:28', '2022-07-16 01:44:28'),
(88, 'App\\Models\\User', 2, 'myapptoken', '300cbcee8ba914da7ad0929975878c3278da73cecba4b6cc976a5204c06c33b1', '[\"*\"]', NULL, '2022-07-16 01:45:38', '2022-07-16 01:45:38'),
(89, 'App\\Models\\User', 1, 'myapptoken', '7fcc3b6fa1aa384d053d75dfebc9ca86a22280d12cb80377759142a0473b7af3', '[\"*\"]', NULL, '2022-07-16 01:55:12', '2022-07-16 01:55:12'),
(90, 'App\\Models\\User', 2, 'myapptoken', 'c38ef0a9a7e7aec8a876a09e36d3311340cac8e9b8d1aecefcf81598cdbe9861', '[\"*\"]', NULL, '2022-07-16 01:56:42', '2022-07-16 01:56:42'),
(91, 'App\\Models\\User', 2, 'myapptoken', 'f4e3719ce03afb981e0a75d54d47701e8ff644c599bad2cc2efac313b4dfc4aa', '[\"*\"]', NULL, '2022-07-16 01:58:29', '2022-07-16 01:58:29'),
(92, 'App\\Models\\User', 1, 'myapptoken', '7fb923522b67049391bbeb964743ecfd9f7a7fdb3c1b38fcc4b1a112b750f8b0', '[\"*\"]', NULL, '2022-07-16 02:01:11', '2022-07-16 02:01:11'),
(93, 'App\\Models\\User', 2, 'myapptoken', 'e154d516884eed9876696d45964bfbf99ce16b55d98da8e26da58b62fd63ca1d', '[\"*\"]', NULL, '2022-07-16 02:01:31', '2022-07-16 02:01:31'),
(94, 'App\\Models\\User', 2, 'myapptoken', '3de0d7ecfabd13aae57efeed887eeaed7f336427293994ca4e45d23e286d8d91', '[\"*\"]', NULL, '2022-07-16 03:28:19', '2022-07-16 03:28:19'),
(95, 'App\\Models\\User', 1, 'myapptoken', '31fa60c592c3dbed35d1fcf523664b3930963f1550b24171ce29c573af1cab63', '[\"*\"]', NULL, '2022-07-16 03:30:28', '2022-07-16 03:30:28'),
(96, 'App\\Models\\User', 2, 'myapptoken', '77991c8a0bba0611887a79c99ac3a0220c66a8ba433e2e5e3a204533afa3cb05', '[\"*\"]', NULL, '2022-07-16 03:48:56', '2022-07-16 03:48:56'),
(97, 'App\\Models\\User', 1, 'myapptoken', 'be6bd99554b00a5748b07d60e30aec072c9754b04e6eccf17a8edfaf18a76bb3', '[\"*\"]', NULL, '2022-07-16 03:51:55', '2022-07-16 03:51:55'),
(98, 'App\\Models\\User', 1, 'myapptoken', 'd6394d62cc966dab1912a45645eba7d6e26b3bbbbf2065320a57c7e294921a81', '[\"*\"]', NULL, '2022-07-16 05:50:49', '2022-07-16 05:50:49'),
(99, 'App\\Models\\User', 1, 'myapptoken', '0f89f398ac5de663d105664332a0a188971091db1d9015ef7656ef645af0c1ea', '[\"*\"]', NULL, '2022-07-16 06:02:49', '2022-07-16 06:02:49'),
(100, 'App\\Models\\User', 1, 'myapptoken', 'c97d2aaed13f05d26252ca1a2dfc56951e215cddc30b507ad6a6ac316dfae349', '[\"*\"]', NULL, '2022-07-16 06:07:56', '2022-07-16 06:07:56'),
(101, 'App\\Models\\User', 2, 'myapptoken', '315b77ec052df89c1b752537b7fdc56bafd9c67a3dd75b7800fdbfbec81cc450', '[\"*\"]', NULL, '2022-07-16 06:16:07', '2022-07-16 06:16:07'),
(102, 'App\\Models\\User', 1, 'myapptoken', '780a802826ffbdf34455a4080f1baa6ba6fcc452b6158195ce180bb794fd18cd', '[\"*\"]', NULL, '2022-07-16 21:31:18', '2022-07-16 21:31:18'),
(103, 'App\\Models\\User', 2, 'myapptoken', 'aa8a19ac1fb34a4cdb4f4c1a40eaee61e9d58b435c5e626e4bd673a9114bba46', '[\"*\"]', NULL, '2022-07-16 21:32:32', '2022-07-16 21:32:32'),
(104, 'App\\Models\\User', 1, 'myapptoken', '643de2d6c1208da9d7025144abb55c50a217bd4261afb1678d1bc3853a427e9c', '[\"*\"]', NULL, '2022-07-16 21:45:24', '2022-07-16 21:45:24'),
(105, 'App\\Models\\User', 1, 'myapptoken', '25b5978f620ee84810759167deeb2d0eff11bac6c0dbcef9ac84e81477147df5', '[\"*\"]', NULL, '2022-07-17 00:47:37', '2022-07-17 00:47:37'),
(106, 'App\\Models\\User', 1, 'myapptoken', '6451023a1a08a6de60127e5e9f8c22ac590995c5a7bd67e0546c75bab113498b', '[\"*\"]', NULL, '2022-07-17 00:58:28', '2022-07-17 00:58:28'),
(107, 'App\\Models\\User', 1, 'myapptoken', '94484bc24e4d2929821ad001c8456829cb538bf59feeba1e3ad184ba7e82bd9e', '[\"*\"]', NULL, '2022-07-17 00:59:55', '2022-07-17 00:59:55'),
(108, 'App\\Models\\User', 1, 'myapptoken', '14ead77ef26e1eefabc5be5581150486613bccf61a3761e444cd8235aa9ab8e0', '[\"*\"]', NULL, '2022-07-17 01:03:08', '2022-07-17 01:03:08'),
(109, 'App\\Models\\User', 1, 'myapptoken', '1905639ccac2d804b6fe0aef441b9d17dfeb0459b17a556d0d45b0e9e19babe3', '[\"*\"]', NULL, '2022-07-17 01:10:06', '2022-07-17 01:10:06'),
(110, 'App\\Models\\User', 1, 'myapptoken', '4e08f395878a3324ee30b17d44c5cca199026d9b67406682b67f1b30653c392f', '[\"*\"]', NULL, '2022-07-17 01:10:20', '2022-07-17 01:10:20'),
(111, 'App\\Models\\User', 1, 'myapptoken', '81697c73b2095f8ac3998d4c520ae914de9c1ba7bd7c0f9729b40d188e794174', '[\"*\"]', NULL, '2022-07-17 01:29:22', '2022-07-17 01:29:22'),
(112, 'App\\Models\\User', 1, 'myapptoken', '8c7a8279c3a2ca299087cf63b586812bb277bda9cb645a2f718e70e840a05325', '[\"*\"]', NULL, '2022-07-17 01:38:45', '2022-07-17 01:38:45'),
(113, 'App\\Models\\User', 1, 'myapptoken', '22a3ec7fc8fc7fd1c8a2af70f92e656fdba6519457e75de8f19141dea8129bd3', '[\"*\"]', NULL, '2022-07-17 01:39:23', '2022-07-17 01:39:23'),
(114, 'App\\Models\\User', 1, 'myapptoken', 'd79a7411cfb56ca99401095d3e3189118407ec57333955e7570f57d9af062b9d', '[\"*\"]', NULL, '2022-07-17 01:43:51', '2022-07-17 01:43:51'),
(115, 'App\\Models\\User', 1, 'myapptoken', 'ce498e210dbc31b62c3e200e2e6264560fb05e551a290b683874557964b8955e', '[\"*\"]', NULL, '2022-07-17 01:46:20', '2022-07-17 01:46:20'),
(116, 'App\\Models\\User', 1, 'myapptoken', '2c58b4feeac7264d6261ce5f6c80ded2328bacee0c03651931e78d625bfbbc55', '[\"*\"]', NULL, '2022-07-17 01:46:38', '2022-07-17 01:46:38'),
(117, 'App\\Models\\User', 2, 'myapptoken', '7428127596f6f8c1b0ee767d97386ee9f15ca497daa3be9f1cab70f3c96c05ad', '[\"*\"]', NULL, '2022-07-17 02:12:23', '2022-07-17 02:12:23'),
(118, 'App\\Models\\User', 1, 'myapptoken', '690841beeb8a87477708d49a4a4d933250ea171e86b687e40c11b360162ca38c', '[\"*\"]', NULL, '2022-07-17 02:15:50', '2022-07-17 02:15:50'),
(119, 'App\\Models\\User', 1, 'myapptoken', 'd95fdde2c8130b9d1e302f256cd5b146ea2c606268e6360bb95878fe5b76d637', '[\"*\"]', NULL, '2022-07-17 02:25:44', '2022-07-17 02:25:44'),
(120, 'App\\Models\\User', 2, 'myapptoken', 'c236c6adfb9ac2996f9ebab1febf80de547ee0691171bf16d85603ecdf9b3062', '[\"*\"]', NULL, '2022-07-17 02:30:04', '2022-07-17 02:30:04'),
(121, 'App\\Models\\User', 1, 'myapptoken', 'fa91482f65e6e528c196d018fd9042dfe599d6bbdeec620c7d94818b88221611', '[\"*\"]', NULL, '2022-07-17 02:30:17', '2022-07-17 02:30:17'),
(122, 'App\\Models\\User', 2, 'myapptoken', '40494dc3b377387cd59470d8af194a9d5ce158cb0d1879101b7566aad1971036', '[\"*\"]', NULL, '2022-07-17 03:17:25', '2022-07-17 03:17:25'),
(123, 'App\\Models\\User', 1, 'myapptoken', 'e8af83a59bf0689d6715173ed85432dd2334fa6590df8023e844fabd990d1021', '[\"*\"]', NULL, '2022-07-17 03:17:45', '2022-07-17 03:17:45'),
(124, 'App\\Models\\User', 2, 'myapptoken', 'f90716068b593dffc709164e766d8ac0e904f0dc236eaea7c51886b17573f61a', '[\"*\"]', NULL, '2022-07-17 03:18:09', '2022-07-17 03:18:09'),
(125, 'App\\Models\\User', 1, 'myapptoken', '1c048536bfb66c140fc53150f7356b31f3c7d059dfc20b9893a5c54db5dce8dc', '[\"*\"]', NULL, '2022-07-17 03:20:02', '2022-07-17 03:20:02'),
(126, 'App\\Models\\User', 1, 'myapptoken', 'f64654458cadaa2e3f9733cc6edcd33627863d50446d44d87ddd2fcc846bb319', '[\"*\"]', NULL, '2022-07-17 03:20:15', '2022-07-17 03:20:15'),
(127, 'App\\Models\\User', 1, 'myapptoken', '0fc662185d8ccb71f37cc92017141b15afb47b947707622a899786c648b7d63b', '[\"*\"]', NULL, '2022-07-17 03:29:02', '2022-07-17 03:29:02'),
(128, 'App\\Models\\User', 1, 'myapptoken', '245966dc91605a73c68c34e6bb72263696c7487235ceb89b155c52a6d64e27d3', '[\"*\"]', NULL, '2022-07-17 03:29:15', '2022-07-17 03:29:15'),
(129, 'App\\Models\\User', 2, 'myapptoken', '4ef0bfd21648586912ff243f8888fc632075f477024f3b4ce3c35100f7d1c014', '[\"*\"]', NULL, '2022-07-17 03:29:39', '2022-07-17 03:29:39'),
(130, 'App\\Models\\User', 1, 'myapptoken', '25a0afeec2acbc8d48d2f6cf236a09eabc4a8b3deb91362c48d26f17161b833f', '[\"*\"]', NULL, '2022-07-17 03:44:20', '2022-07-17 03:44:20'),
(131, 'App\\Models\\User', 2, 'myapptoken', 'c5a9ac5b6476a2533bd9cbe3c556bb135a16d51084cf9bad049258fe10cebe50', '[\"*\"]', NULL, '2022-07-17 04:01:24', '2022-07-17 04:01:24'),
(132, 'App\\Models\\User', 1, 'myapptoken', 'dd867e59bfe1dcbff8ae7cd88e7ddb00b30dfb47707af29a70ddb8b77a676b86', '[\"*\"]', NULL, '2022-07-17 04:06:08', '2022-07-17 04:06:08'),
(133, 'App\\Models\\User', 2, 'myapptoken', 'd2d56c5c7690974a3700526720917d7ea8f53a4c9a6c0f5ce0e4676bbce11422', '[\"*\"]', NULL, '2022-07-17 04:06:26', '2022-07-17 04:06:26'),
(134, 'App\\Models\\User', 1, 'myapptoken', 'f6e048cf7deddf75506b6aaf871928278ee99e6364c7f54caff738e03b619318', '[\"*\"]', NULL, '2022-07-17 04:06:43', '2022-07-17 04:06:43'),
(135, 'App\\Models\\User', 1, 'myapptoken', '29a1a97d008896de7a3643b06ce72fe97cec890fe07af667fddd01df0a99afcf', '[\"*\"]', NULL, '2022-07-17 19:29:04', '2022-07-17 19:29:04'),
(136, 'App\\Models\\User', 2, 'myapptoken', '52c1aa095cdb04f68b53afb7515d4773281f88fd90db39a8da0cc91c1b0f33f5', '[\"*\"]', NULL, '2022-07-17 19:45:21', '2022-07-17 19:45:21'),
(137, 'App\\Models\\User', 1, 'myapptoken', '2b92d80a89b805531b3bb07dc106283071661c538fd4f464c1a05accc8830aab', '[\"*\"]', NULL, '2022-07-17 19:45:41', '2022-07-17 19:45:41'),
(138, 'App\\Models\\User', 1, 'myapptoken', 'a9d6916cf67eb72f4be0326a9bb2d9eafc343758b47f1beee4952954b937cbaa', '[\"*\"]', NULL, '2022-07-17 19:50:38', '2022-07-17 19:50:38'),
(139, 'App\\Models\\User', 2, 'myapptoken', '03809332a7fbd4f7c506ba8e235c54a405903e25b6fa6d61f3f968d272b63690', '[\"*\"]', NULL, '2022-07-17 19:51:09', '2022-07-17 19:51:09'),
(140, 'App\\Models\\User', 1, 'myapptoken', '7e62de231ca07c04f68a8ad688663a07492535773e47d87927856bac78f843d7', '[\"*\"]', NULL, '2022-07-17 21:25:33', '2022-07-17 21:25:33'),
(141, 'App\\Models\\User', 1, 'myapptoken', 'b4bc7e1aa62ce162dda8378b973742b7a72b6a51119247aada8293af20b0f11d', '[\"*\"]', NULL, '2022-07-17 21:27:51', '2022-07-17 21:27:51'),
(142, 'App\\Models\\User', 2, 'myapptoken', '43b67c3065734952306c2c92bfcc423bcd492e3ad16311016143dd825ca42bd7', '[\"*\"]', NULL, '2022-07-17 21:29:02', '2022-07-17 21:29:02'),
(143, 'App\\Models\\User', 1, 'myapptoken', '933714f03068810a28d2649da112fe8ff3546df992ed671e760a68598a52f197', '[\"*\"]', NULL, '2022-07-17 21:48:22', '2022-07-17 21:48:22'),
(144, 'App\\Models\\User', 2, 'myapptoken', '46629488cf8a481e47e67b474b196e29bc0048f063fdf85a9fe5013e17526a9e', '[\"*\"]', NULL, '2022-07-17 21:48:42', '2022-07-17 21:48:42'),
(145, 'App\\Models\\User', 1, 'myapptoken', '34d07499e6e808282918ff44efdc051e1ae9e05ce2ae7a4e804c5202be953af1', '[\"*\"]', NULL, '2022-07-17 21:51:50', '2022-07-17 21:51:50'),
(146, 'App\\Models\\User', 2, 'myapptoken', '51856ad48eba17d4def4b74acb83ffb1bee3b5687d8039c3c0d75d8e2cdbde1e', '[\"*\"]', NULL, '2022-07-18 00:44:35', '2022-07-18 00:44:35'),
(147, 'App\\Models\\User', 2, 'myapptoken', 'f929cfba7be64196c7b632c5f888ce53e4a86fb29e5966f21b1cdb31e080d5c0', '[\"*\"]', NULL, '2022-07-18 02:34:54', '2022-07-18 02:34:54'),
(148, 'App\\Models\\User', 2, 'myapptoken', '465c017d981ab2aa52ca87edbe06f7ad9ede26d279b9796d0ab41dbb3ada6510', '[\"*\"]', NULL, '2022-07-18 02:35:08', '2022-07-18 02:35:08'),
(149, 'App\\Models\\User', 1, 'myapptoken', 'eeb21a944c4291882a36857e3e7b43769979913816cf90de394d8748adc21e58', '[\"*\"]', NULL, '2022-07-18 02:36:26', '2022-07-18 02:36:26'),
(150, 'App\\Models\\User', 1, 'myapptoken', 'e605118f7796726fbae16aacc7d8762c7f9a8e17d6918a9adf79b3b9ca965cd2', '[\"*\"]', NULL, '2022-07-18 02:36:50', '2022-07-18 02:36:50'),
(151, 'App\\Models\\User', 2, 'myapptoken', '3096514da6f322f52c2debfeb4e313836c2f1bfcb264e17ec7cba37a5e88b638', '[\"*\"]', NULL, '2022-07-18 02:37:35', '2022-07-18 02:37:35'),
(152, 'App\\Models\\User', 2, 'myapptoken', '3004654c09151e99e0312ca03b113e66c16861033acb843de3ff136fa6dd2eca', '[\"*\"]', NULL, '2022-07-18 02:40:41', '2022-07-18 02:40:41'),
(153, 'App\\Models\\User', 1, 'myapptoken', '6918e3d3b8761442920b973b32eca17edd3f483fadedcd98d6ee27bd266e7607', '[\"*\"]', NULL, '2022-07-18 02:42:42', '2022-07-18 02:42:42'),
(154, 'App\\Models\\User', 2, 'myapptoken', '12a30399387e054ef0bf6ff27a05627384fab3226da74f20eccb8291f6711277', '[\"*\"]', NULL, '2022-07-18 02:45:27', '2022-07-18 02:45:27'),
(155, 'App\\Models\\User', 2, 'myapptoken', 'd09d00f5cc04eb507a536f554fc9dbc7b9a209573ff872be0588d783ecd60b58', '[\"*\"]', NULL, '2022-07-18 02:50:15', '2022-07-18 02:50:15'),
(156, 'App\\Models\\User', 1, 'myapptoken', 'ca2501d1cc2ee4b91df3862c8e3d6afd085d0825954c66dccb66f0b4bb0b4728', '[\"*\"]', NULL, '2022-07-18 02:50:25', '2022-07-18 02:50:25'),
(157, 'App\\Models\\User', 2, 'myapptoken', 'b100739a07e7a4719ee7d8c3f0063b04e784ed9093bc9bda397f21c4f50560d9', '[\"*\"]', NULL, '2022-07-18 02:53:42', '2022-07-18 02:53:42'),
(158, 'App\\Models\\User', 1, 'myapptoken', '7574fb69921bc2342fea9ce5c8eff083588c644b0c0baffe23eb37d5ba8291bb', '[\"*\"]', NULL, '2022-07-18 03:19:24', '2022-07-18 03:19:24'),
(159, 'App\\Models\\User', 1, 'myapptoken', 'b46f091bc18549d70ffd221f82a9f5c13cac7f02272a889f46397f2aebb60b03', '[\"*\"]', NULL, '2022-07-18 03:19:32', '2022-07-18 03:19:32'),
(160, 'App\\Models\\User', 1, 'myapptoken', 'c247bdcaa93c8e88e04da060a300450fd893f4776ff1e4397bee047fdbbd8ec0', '[\"*\"]', NULL, '2022-07-18 03:20:08', '2022-07-18 03:20:08'),
(161, 'App\\Models\\User', 2, 'myapptoken', 'cc0808341f0d3c793edbc61d55c34131ea9108615a653736a8b65aa1098cdbd0', '[\"*\"]', NULL, '2022-07-18 03:21:05', '2022-07-18 03:21:05'),
(162, 'App\\Models\\User', 1, 'myapptoken', '702a7303c5877211f81671a84ffa69b11f33eb78ae0bc0e540e2820d97bbe6c6', '[\"*\"]', NULL, '2022-07-18 03:21:39', '2022-07-18 03:21:39'),
(163, 'App\\Models\\User', 2, 'myapptoken', '72543ec4d22fdd15436381cbf9b1e96d47e617b0dce729c0d5c5c7e77123cc28', '[\"*\"]', NULL, '2022-07-18 03:22:36', '2022-07-18 03:22:36'),
(164, 'App\\Models\\User', 1, 'myapptoken', '8b239cda77877d595393d4aec7d899bdc0a60d289cbbcd7f3e891652846a3563', '[\"*\"]', NULL, '2022-07-18 03:22:55', '2022-07-18 03:22:55'),
(165, 'App\\Models\\User', 2, 'myapptoken', 'f89b4f376e589fe68fb2883025967c3aa503e0e37b70e37b4e60c644c0f60346', '[\"*\"]', NULL, '2022-07-18 03:23:13', '2022-07-18 03:23:13'),
(166, 'App\\Models\\User', 1, 'myapptoken', '89ffb471d80b71272cc118f20e52976a82951bdbcd463460dad66451e4c51a68', '[\"*\"]', NULL, '2022-07-18 03:23:45', '2022-07-18 03:23:45'),
(167, 'App\\Models\\User', 1, 'myapptoken', 'bca71adee137efcd2bbef56a1028f4a8e564c2cefc121058522ec9c540325b09', '[\"*\"]', NULL, '2022-07-18 04:25:12', '2022-07-18 04:25:12'),
(168, 'App\\Models\\User', 2, 'myapptoken', '7567055511d231a08b28e158a242c59c9387234b2d81ccf3e047825603aeaee8', '[\"*\"]', NULL, '2022-07-18 04:25:26', '2022-07-18 04:25:26'),
(169, 'App\\Models\\User', 1, 'myapptoken', '35f9f68741a7b6643e70faf7268ad0229c196a5b346a6f1894de34dbad7c037e', '[\"*\"]', NULL, '2022-07-18 04:25:40', '2022-07-18 04:25:40'),
(170, 'App\\Models\\User', 2, 'myapptoken', 'b8f05d70dc337bfc81a0cfafa362d92b9eaa490a29033e779420b6865f450364', '[\"*\"]', NULL, '2022-07-18 05:14:19', '2022-07-18 05:14:19'),
(171, 'App\\Models\\User', 2, 'myapptoken', '6fb7b5a39ebf1c64bf875e0147e660a5cec289d4fb2f4c95b2023c1874e445c6', '[\"*\"]', NULL, '2022-07-18 05:19:23', '2022-07-18 05:19:23'),
(172, 'App\\Models\\User', 1, 'myapptoken', '2abc1ddfb3df8a3a3fa194f10ee5516f573cc8250f63c46bedef0cd69454208b', '[\"*\"]', NULL, '2022-07-18 05:21:36', '2022-07-18 05:21:36'),
(173, 'App\\Models\\User', 1, 'myapptoken', '8e867b356f17a862c0c82edd4002557551c58a858f64ee6c891c61ce4145b92c', '[\"*\"]', NULL, '2022-07-18 05:23:59', '2022-07-18 05:23:59'),
(174, 'App\\Models\\User', 2, 'myapptoken', 'a8e03bd75a47900ffaa42d2febc4207ae652cf1ac1f318c24469d2d70874eff8', '[\"*\"]', NULL, '2022-07-18 05:32:52', '2022-07-18 05:32:52'),
(175, 'App\\Models\\User', 1, 'myapptoken', '0a885f84190fc4c7e23c78775f846b5c03ab8415a99c5d278c6a26c01d7259d0', '[\"*\"]', NULL, '2022-07-18 05:33:23', '2022-07-18 05:33:23'),
(176, 'App\\Models\\User', 1, 'myapptoken', 'c659291ae0444956a18c83fd80e4be9360b90e36b2458c18eeda4f9a026d5f16', '[\"*\"]', NULL, '2022-07-18 05:37:56', '2022-07-18 05:37:56'),
(177, 'App\\Models\\User', 2, 'myapptoken', '6525d4e52ca6cc37e45970ee3c34a3f2e5cf5b299f10f2c1cc802b93c4a90794', '[\"*\"]', NULL, '2022-07-18 05:38:47', '2022-07-18 05:38:47'),
(178, 'App\\Models\\User', 2, 'myapptoken', 'e270c4c0392762f73a49abdf89f1862b265cec99153b22621657376999c1cd22', '[\"*\"]', NULL, '2022-07-18 05:40:19', '2022-07-18 05:40:19'),
(179, 'App\\Models\\User', 1, 'myapptoken', 'f5e5ec83ec448b3db7f818cf964978e391a34397c076620c0a30d8eb705d29ff', '[\"*\"]', NULL, '2022-07-18 05:41:17', '2022-07-18 05:41:17'),
(180, 'App\\Models\\User', 1, 'myapptoken', '7721e8afb4979fe25dcd9d01fbd90b3a40435fbeea2634d298090d4ca23e9046', '[\"*\"]', NULL, '2022-07-18 05:42:16', '2022-07-18 05:42:16'),
(181, 'App\\Models\\User', 2, 'myapptoken', '0cde6a7964dc5b0708b0158c0bdd3e497264dcd6455515e864021b360997bf7b', '[\"*\"]', NULL, '2022-07-18 05:47:28', '2022-07-18 05:47:28'),
(182, 'App\\Models\\User', 1, 'myapptoken', '9cf639c3f85e952cae054d6dbbf7c46227323de1bcfd36a261bad8fa253744be', '[\"*\"]', NULL, '2022-07-18 05:48:24', '2022-07-18 05:48:24'),
(183, 'App\\Models\\User', 1, 'myapptoken', '122e0489f078fe1164e9569a292d428239a11bf8c88f0504a42a2eb3ac81e209', '[\"*\"]', NULL, '2022-07-18 05:52:02', '2022-07-18 05:52:02'),
(184, 'App\\Models\\User', 1, 'myapptoken', '369fdd4612bf9f6d8884b7738befeb2bc78521586e330d7b26886987ccc30884', '[\"*\"]', NULL, '2022-07-18 06:00:57', '2022-07-18 06:00:57'),
(185, 'App\\Models\\User', 1, 'myapptoken', '8e77b4f0808d03077ef432122174536b285dc2e02801ed858299cdadea0886b4', '[\"*\"]', NULL, '2022-07-18 06:01:44', '2022-07-18 06:01:44'),
(186, 'App\\Models\\User', 1, 'myapptoken', 'c333c0d241243b4e9dd09cd3212a38f94a930939ba84a02e17da9ff8b0d2f29e', '[\"*\"]', NULL, '2022-07-18 06:18:39', '2022-07-18 06:18:39'),
(187, 'App\\Models\\User', 1, 'myapptoken', 'a9e69ec9f64216159c49c6834927bec501665a04d72f4f2fdd92c5ca142ce140', '[\"*\"]', NULL, '2022-07-18 06:18:46', '2022-07-18 06:18:46'),
(188, 'App\\Models\\User', 1, 'myapptoken', 'e3c83bc222639dfc65a204184da5efaed5ffb330b9a418330ccd43246c0569df', '[\"*\"]', NULL, '2022-07-18 06:19:00', '2022-07-18 06:19:00'),
(189, 'App\\Models\\User', 2, 'myapptoken', '1f1c0a7f3d90e8edb5204b97743438758e9b7e6cd972618df85479a6b38a3990', '[\"*\"]', NULL, '2022-07-18 06:19:54', '2022-07-18 06:19:54'),
(190, 'App\\Models\\User', 1, 'myapptoken', '154783b8a4e18815762052fbac79f1ae53a2dcfde021660eb381ba7d4186fb18', '[\"*\"]', NULL, '2022-07-18 06:20:00', '2022-07-18 06:20:00'),
(191, 'App\\Models\\User', 2, 'myapptoken', '2b89d8fe672458550e5ad53ad9833b3a7b2f6a69c9e3bdd3ff9365fc090fbd24', '[\"*\"]', NULL, '2022-07-18 06:20:16', '2022-07-18 06:20:16'),
(192, 'App\\Models\\User', 1, 'myapptoken', '0de9b76b7f8a3faa9919c577e61bd268cb54646dfdae7d62642f9e2edd869cfb', '[\"*\"]', NULL, '2022-07-18 06:20:23', '2022-07-18 06:20:23'),
(193, 'App\\Models\\User', 1, 'myapptoken', '5e18a750c948be29f42318dbf147136c852e501d233f0ee9313133fafb4becdf', '[\"*\"]', NULL, '2022-07-18 06:20:31', '2022-07-18 06:20:31'),
(194, 'App\\Models\\User', 1, 'myapptoken', '9c83ad7f90db8558a99412b58ab7ac151cb999f0af77a4b8d1e1116af07ccf90', '[\"*\"]', NULL, '2022-07-18 06:20:57', '2022-07-18 06:20:57'),
(195, 'App\\Models\\User', 1, 'myapptoken', '4601866a574c1b29def9472d581cbdb0e06fbc030148dd790485d33932783fe3', '[\"*\"]', NULL, '2022-07-18 06:21:55', '2022-07-18 06:21:55'),
(196, 'App\\Models\\User', 1, 'myapptoken', 'd164060adf6f95a3c2192822cfe9e2a3fd537ec76dd0fff337ea062d133d88af', '[\"*\"]', NULL, '2022-07-18 06:24:57', '2022-07-18 06:24:57'),
(197, 'App\\Models\\User', 2, 'myapptoken', 'c6ac59e39b26971a12273576e50becf769068c8dcc870ed5b4f070e04e040298', '[\"*\"]', NULL, '2022-07-18 06:25:03', '2022-07-18 06:25:03'),
(198, 'App\\Models\\User', 1, 'myapptoken', 'd5c97e00ea0d999f8cfb91c83f20457451293789d45e37b33f0044bb107731f7', '[\"*\"]', NULL, '2022-07-18 06:26:11', '2022-07-18 06:26:11'),
(199, 'App\\Models\\User', 1, 'myapptoken', '46369805e983e84afd7e41ed9ef6aac9cae197a5921e2b083cc0452beef35296', '[\"*\"]', NULL, '2022-07-18 06:31:55', '2022-07-18 06:31:55'),
(200, 'App\\Models\\User', 2, 'myapptoken', '4e20106e7e74a8afad8ef07484f7d9f5b1d7229b4d98326bd5239fdbca1c4f51', '[\"*\"]', NULL, '2022-07-18 06:40:26', '2022-07-18 06:40:26'),
(201, 'App\\Models\\User', 1, 'myapptoken', 'de2e58cb9def870906cb1c1c4f00719d44d9ffc4431cf44ec0fa1fc9cf969763', '[\"*\"]', NULL, '2022-07-18 06:40:35', '2022-07-18 06:40:35'),
(202, 'App\\Models\\User', 2, 'myapptoken', '8fd1cb8c86aa0d4cb7cf005f7888d19df785f21a1d73b84cdc4058ac03224b0b', '[\"*\"]', NULL, '2022-07-18 06:57:32', '2022-07-18 06:57:32'),
(203, 'App\\Models\\User', 1, 'myapptoken', '01d7965a542be06c4b9f6d74d276bce05214469e4b5c61fe53ac3673bd06b664', '[\"*\"]', NULL, '2022-07-18 06:57:44', '2022-07-18 06:57:44'),
(204, 'App\\Models\\User', 2, 'myapptoken', 'd04b99705458fb39b42df305017c4c6b2076c7fa685d64ae7732799b3f28e5fc', '[\"*\"]', NULL, '2022-07-18 06:58:16', '2022-07-18 06:58:16'),
(205, 'App\\Models\\User', 1, 'myapptoken', '3d981aae69589d4ba475ca6f8c6a3013b205b4e4cae6320f27b55917d311485e', '[\"*\"]', NULL, '2022-07-18 07:00:05', '2022-07-18 07:00:05'),
(206, 'App\\Models\\User', 1, 'myapptoken', 'b5c4225766aafe000c6721a92eb6b6544b2cc72b2bb871d1c9ac06547bd7d2bc', '[\"*\"]', NULL, '2022-07-18 08:14:11', '2022-07-18 08:14:11'),
(207, 'App\\Models\\User', 1, 'myapptoken', '5ad2df5ca94e0a1666ba370c6b3f9d9b36cd53c511a80bb15612f906ed094c05', '[\"*\"]', NULL, '2022-07-18 08:18:52', '2022-07-18 08:18:52'),
(208, 'App\\Models\\User', 1, 'myapptoken', '451feb44553d9693d2e11f80afe6b48a0e7edd947cac3bb765c953c511bddd1e', '[\"*\"]', NULL, '2022-07-18 08:18:59', '2022-07-18 08:18:59'),
(209, 'App\\Models\\User', 1, 'myapptoken', '0ab46a23bddf350439aa7084fe4097d978f34741f9408a37bf89963366ce8ad3', '[\"*\"]', NULL, '2022-07-18 08:28:02', '2022-07-18 08:28:02'),
(210, 'App\\Models\\User', 1, 'myapptoken', '5d972e83d223a783fb5c20dd5dc66cd668d87a8bd7c94062e0fd0a688e3b20ed', '[\"*\"]', NULL, '2022-07-18 08:32:29', '2022-07-18 08:32:29'),
(211, 'App\\Models\\User', 2, 'myapptoken', 'd308a0a3d2cbdf9500bef186bf13958eeeeb5d7290e7e3fec745b61e1794d5f1', '[\"*\"]', NULL, '2022-07-18 08:32:46', '2022-07-18 08:32:46'),
(212, 'App\\Models\\User', 2, 'myapptoken', 'a48347885587d9df68e3f5c2a0c8e1e32b98686921790f4486e3ae83a213e374', '[\"*\"]', NULL, '2022-07-18 08:35:30', '2022-07-18 08:35:30'),
(213, 'App\\Models\\User', 2, 'myapptoken', 'b9fe38e335d631b009fde3361d5e30b6003aa4ae9fc3c1e32c5877737ef322d2', '[\"*\"]', NULL, '2022-07-18 08:35:51', '2022-07-18 08:35:51'),
(214, 'App\\Models\\User', 1, 'myapptoken', 'b9709fc5bcf154a9dc25f9678c30117c82b1d40113f69ff8d8734009727b821f', '[\"*\"]', NULL, '2022-07-18 08:36:11', '2022-07-18 08:36:11'),
(215, 'App\\Models\\User', 2, 'myapptoken', '06d5de5d0a555a9ff3ad162dd97bb6d73cacc66a7ae7651a483d2aabde4a5fb3', '[\"*\"]', NULL, '2022-07-18 08:38:14', '2022-07-18 08:38:14'),
(216, 'App\\Models\\User', 1, 'myapptoken', 'cca5b4284c73860e1978b1b3de05bda638f62db9f94136b8702ac27f4794be3e', '[\"*\"]', NULL, '2022-07-18 08:39:06', '2022-07-18 08:39:06'),
(217, 'App\\Models\\User', 1, 'myapptoken', '04f85d0f8e3ffaf36535a7e77d8606e920423d49320ae67f1c6261c4787c7918', '[\"*\"]', NULL, '2022-07-18 08:39:19', '2022-07-18 08:39:19'),
(218, 'App\\Models\\User', 2, 'myapptoken', '4a6a1e09cb75c83342bb4e842a8d8f06ae3ce8ebdb155d372b6a7deab2a05558', '[\"*\"]', NULL, '2022-07-18 08:39:51', '2022-07-18 08:39:51'),
(219, 'App\\Models\\User', 1, 'myapptoken', '08e2982528785480f378d7ce16d6ebcf9e7b3d81c749ace69bee68def3b0ea52', '[\"*\"]', NULL, '2022-07-18 08:40:54', '2022-07-18 08:40:54'),
(220, 'App\\Models\\User', 1, 'myapptoken', 'ecfc4bb40250cf4a692bc421c8b6c6cba3891179f37f4ae648d77353fbcf0648', '[\"*\"]', NULL, '2022-07-18 08:41:04', '2022-07-18 08:41:04'),
(221, 'App\\Models\\User', 1, 'myapptoken', '3e914a1b18128c960babbfc9d60cf372494df7f114f2a19cf61db5865d1a8c03', '[\"*\"]', NULL, '2022-07-18 09:19:23', '2022-07-18 09:19:23'),
(222, 'App\\Models\\User', 1, 'myapptoken', '37df94134ca1df910419577494f542cf40d96337bc59f97a43f242e46fa6c70d', '[\"*\"]', NULL, '2022-07-18 09:22:38', '2022-07-18 09:22:38'),
(223, 'App\\Models\\User', 2, 'myapptoken', '0475582ae82ee09a2c109a8e60901b75042f037ee092fd2ab33d339d969755da', '[\"*\"]', NULL, '2022-07-18 09:25:28', '2022-07-18 09:25:28'),
(224, 'App\\Models\\User', 1, 'myapptoken', 'f2e2039b00b9384a6f46bfef946215f3b0381a23858a2cdd3ea1ee8674f71b49', '[\"*\"]', NULL, '2022-07-18 09:25:59', '2022-07-18 09:25:59'),
(225, 'App\\Models\\User', 1, 'myapptoken', '731dd13fa0023e67d9e8a42ff2be83634baa5e0e6be31216979152ec8e23acb1', '[\"*\"]', NULL, '2022-07-18 09:34:20', '2022-07-18 09:34:20'),
(226, 'App\\Models\\User', 1, 'myapptoken', 'dc5051ca5e8161fd7c3133f84be3f3b9fd7b87724855b95ad3b35d882a75c54c', '[\"*\"]', NULL, '2022-07-18 09:34:30', '2022-07-18 09:34:30'),
(227, 'App\\Models\\User', 1, 'myapptoken', '6d1895b9375c2a248ef2cdfc4977b26dac336dfb4e6feb65c3bf9508fdfa98d6', '[\"*\"]', NULL, '2022-07-18 09:34:42', '2022-07-18 09:34:42'),
(228, 'App\\Models\\User', 1, 'myapptoken', '7ff8f8ead59c05084e64f09a77a17d33182d0fa1361556c14721a408d0eefcaf', '[\"*\"]', NULL, '2022-07-18 09:35:03', '2022-07-18 09:35:03'),
(229, 'App\\Models\\User', 1, 'myapptoken', '46e94e71dc2fbf1ccbe23773c8bd41fa85f1a6e95df4098abc741a9fdf4f1115', '[\"*\"]', NULL, '2022-07-18 09:35:52', '2022-07-18 09:35:52'),
(230, 'App\\Models\\User', 1, 'myapptoken', 'a32094dae57ebc1c12408e68a5f864bbc4601425b3ffbbef8d83c73a9b72efec', '[\"*\"]', NULL, '2022-07-18 09:39:57', '2022-07-18 09:39:57'),
(231, 'App\\Models\\User', 1, 'myapptoken', 'f44dced838c2f02f9b95d5767e6da6532fcdc039020f8a046cc78dd8ec37e7d2', '[\"*\"]', NULL, '2022-07-18 09:41:07', '2022-07-18 09:41:07'),
(232, 'App\\Models\\User', 2, 'myapptoken', '68b23dcf0c8798e1ea47bf184adf78c902d0e0ee6bece572fed2a50f0ad29482', '[\"*\"]', NULL, '2022-07-18 09:55:05', '2022-07-18 09:55:05'),
(233, 'App\\Models\\User', 1, 'myapptoken', 'e0f0c26d4c46843377743135f2ec9f65977b9353f38c5af12aa36617db832a3e', '[\"*\"]', NULL, '2022-07-18 09:55:22', '2022-07-18 09:55:22'),
(234, 'App\\Models\\User', 1, 'myapptoken', '507accfa8ad6d08f68ead6846ead8def27f686f013fa7a66a3157a733c4fe0ea', '[\"*\"]', NULL, '2022-07-18 09:56:07', '2022-07-18 09:56:07'),
(235, 'App\\Models\\User', 1, 'myapptoken', 'a5cdd5d1531d2e1b9f93cb061885b7d88a680bea0b723b8b66d5333a83db4e05', '[\"*\"]', NULL, '2022-07-18 09:57:40', '2022-07-18 09:57:40'),
(236, 'App\\Models\\User', 1, 'myapptoken', '432d3ca7acaa99b3cf30b0a588feae873ca21fc3d21de0ea3a5fabe6e09b240a', '[\"*\"]', NULL, '2022-07-18 10:01:49', '2022-07-18 10:01:49'),
(237, 'App\\Models\\User', 2, 'myapptoken', '85978d7fa889af77a0fe967842054275bef9779068c271fe61ff7272f4390591', '[\"*\"]', NULL, '2022-07-18 10:02:46', '2022-07-18 10:02:46'),
(238, 'App\\Models\\User', 1, 'myapptoken', 'babbdef5cd7eb0b7ae0e75b9a6836514b84d3f384607d7510768ebc246d27849', '[\"*\"]', NULL, '2022-07-18 10:03:07', '2022-07-18 10:03:07'),
(239, 'App\\Models\\User', 1, 'myapptoken', 'c9a9cdfe5adcba1fb03e8c3fa002425ada3b71016a30692a3ec7b41124b1d6d5', '[\"*\"]', NULL, '2022-07-18 10:06:43', '2022-07-18 10:06:43'),
(240, 'App\\Models\\User', 2, 'myapptoken', '0925c23d625c091c3456e0c3de9880e38aae1ea3f4c4465063792af8ed34bfc3', '[\"*\"]', NULL, '2022-07-18 10:20:22', '2022-07-18 10:20:22'),
(241, 'App\\Models\\User', 1, 'myapptoken', '616e4962b09c99ad348cad39dfa4a2401b6a19363c74e85466573bd95552eee6', '[\"*\"]', NULL, '2022-07-18 10:25:52', '2022-07-18 10:25:52'),
(242, 'App\\Models\\User', 2, 'myapptoken', '8bfc79fa85cb12de107fa11b591b2f19572e47a62cf9e0fb78a6b2b7d9c40fd7', '[\"*\"]', NULL, '2022-07-18 10:26:30', '2022-07-18 10:26:30'),
(243, 'App\\Models\\User', 2, 'myapptoken', '261091f780d8fd746f6656a15e86f04c19d328db2ab2545ebedeaf84ba90abab', '[\"*\"]', NULL, '2022-07-18 11:05:59', '2022-07-18 11:05:59'),
(244, 'App\\Models\\User', 2, 'myapptoken', 'ce0bf6f02736bd169b2d1351e86d3fe77ff3e015b33f4137e7c2ead260855a59', '[\"*\"]', NULL, '2022-07-18 22:05:08', '2022-07-18 22:05:08'),
(245, 'App\\Models\\User', 1, 'myapptoken', '96e0107bfc11e24cf39b024ae1f6e1c840db0e5b19227c2a2ffc34cd46292924', '[\"*\"]', NULL, '2022-07-18 22:06:27', '2022-07-18 22:06:27'),
(246, 'App\\Models\\User', 2, 'myapptoken', '0afe81da6cb222ee15911c98b00ffad85215514460322090f782390fdcdadd42', '[\"*\"]', NULL, '2022-07-18 22:07:04', '2022-07-18 22:07:04'),
(247, 'App\\Models\\User', 1, 'myapptoken', '77d8e3ab6d5c20dfc89cd27837f2800488d7e9bee3a0dda827660065088c908f', '[\"*\"]', NULL, '2022-07-18 22:07:17', '2022-07-18 22:07:17'),
(248, 'App\\Models\\User', 2, 'myapptoken', 'e0f2d1993884f79f884e492fa1fb326a785b69aea60d745355752aa505b2b6f6', '[\"*\"]', NULL, '2022-07-18 22:08:02', '2022-07-18 22:08:02'),
(249, 'App\\Models\\User', 1, 'myapptoken', '42f156853fad2d96ede5a0ac0a81e35dce5851420ef45a9e58299e33c00edac2', '[\"*\"]', NULL, '2022-07-18 22:11:22', '2022-07-18 22:11:22'),
(250, 'App\\Models\\User', 1, 'myapptoken', 'f588909c944562b04c450cb3f1c90a4093b78c4dd1c87de95ec18e4b04a5de67', '[\"*\"]', NULL, '2022-07-18 22:12:44', '2022-07-18 22:12:44'),
(251, 'App\\Models\\User', 1, 'myapptoken', 'ee595ff92851ba1a559f09b3db897ae4dcf2f64e81a5388370b8943eb0abbcf8', '[\"*\"]', NULL, '2022-07-18 22:18:25', '2022-07-18 22:18:25'),
(252, 'App\\Models\\User', 1, 'myapptoken', 'eb28f7ae14686ec20fe068f7cbdb36e3aaa14d838cad73852844418376cad1e8', '[\"*\"]', NULL, '2022-07-18 22:18:34', '2022-07-18 22:18:34'),
(253, 'App\\Models\\User', 1, 'myapptoken', '86f6b0258a1ffb6edbe4ea4982a53119f86fac756b63770ab35c4c4fd43409c8', '[\"*\"]', NULL, '2022-07-18 22:21:39', '2022-07-18 22:21:39'),
(254, 'App\\Models\\User', 1, 'myapptoken', 'f9dca9f738103693f3b2a893d02eb315fc1ea3e5b4099eb23822d54c35c28198', '[\"*\"]', NULL, '2022-07-18 22:22:23', '2022-07-18 22:22:23'),
(255, 'App\\Models\\User', 1, 'myapptoken', '49e082b9f45263fb0191e105705db8054bf0be7f850f699b2954d47c34178fa4', '[\"*\"]', NULL, '2022-07-18 22:22:35', '2022-07-18 22:22:35'),
(256, 'App\\Models\\User', 1, 'myapptoken', '4f9f39d12816c1090c79f0ccdb250e51ff432d2574db6477e2d673329837bfa1', '[\"*\"]', NULL, '2022-07-18 22:24:01', '2022-07-18 22:24:01'),
(257, 'App\\Models\\User', 1, 'myapptoken', '6a64f7e04dfd295cce8088b0a4aa1075aa580555f37ade0ba3556a39356eca98', '[\"*\"]', NULL, '2022-07-18 22:24:21', '2022-07-18 22:24:21'),
(258, 'App\\Models\\User', 1, 'myapptoken', 'ea9bca880a6fd51a2bd73e28a3c8dc91f0fdd4fd504d59c101aa89ca0d613100', '[\"*\"]', NULL, '2022-07-18 22:24:50', '2022-07-18 22:24:50'),
(259, 'App\\Models\\User', 1, 'myapptoken', '9d453fa9cd8635fe0b0e104184b996841e93013fe5fafb2955d522cf60aa8309', '[\"*\"]', NULL, '2022-07-18 22:27:04', '2022-07-18 22:27:04'),
(260, 'App\\Models\\User', 1, 'myapptoken', '7b43a1582549223504d62b2d0c86e278febbae4a0caf16a86ff668ef5c9464da', '[\"*\"]', NULL, '2022-07-18 22:30:31', '2022-07-18 22:30:31'),
(261, 'App\\Models\\User', 2, 'myapptoken', '3a72484d32b09941d622f9265f5df4b63fdd6d566024e7af97376acb2caa9d94', '[\"*\"]', NULL, '2022-07-18 22:30:45', '2022-07-18 22:30:45'),
(262, 'App\\Models\\User', 1, 'myapptoken', 'b8a54cc1677a1dae8a3fc630317209bda4fd1b363fa82227e170d25961c2611b', '[\"*\"]', NULL, '2022-07-18 22:31:44', '2022-07-18 22:31:44'),
(263, 'App\\Models\\User', 1, 'myapptoken', '80e790cd0cbc57bda75cef2ddbac01648533d3ecbf4fa361a97eb50a9162c37c', '[\"*\"]', NULL, '2022-07-18 22:32:08', '2022-07-18 22:32:08'),
(264, 'App\\Models\\User', 2, 'myapptoken', '0023ce58929e0662f713c4d943ca9fc8fc508dce8c811dddfc2219e89a7832d2', '[\"*\"]', NULL, '2022-07-18 22:32:54', '2022-07-18 22:32:54'),
(265, 'App\\Models\\User', 1, 'myapptoken', '973545a2ecea9724a501904987cf0040a8dc8f48f15ab0811448b5b0cc885456', '[\"*\"]', NULL, '2022-07-18 23:39:36', '2022-07-18 23:39:36'),
(266, 'App\\Models\\User', 2, 'myapptoken', '23279480e8ba7e752350a35e773ead9b2fe22992d4f6c984c9a99f07a392aaae', '[\"*\"]', NULL, '2022-07-18 23:40:23', '2022-07-18 23:40:23'),
(267, 'App\\Models\\User', 1, 'myapptoken', 'ca893e82a8d7f83a6b82152504492d57a64b4429df5c89c4ccf55d7e6e2d897e', '[\"*\"]', NULL, '2022-07-18 23:40:33', '2022-07-18 23:40:33'),
(268, 'App\\Models\\User', 1, 'myapptoken', '268ebfcc1aa06e8340a7855f81de0bddb52897af45e14c38994f8fdd35476a7a', '[\"*\"]', NULL, '2022-07-18 23:40:56', '2022-07-18 23:40:56'),
(269, 'App\\Models\\User', 1, 'myapptoken', 'a72a78bd6e37e6d0e913b2811c63b9fe5dc3d11f04f0c3264e2d9229bd6623f9', '[\"*\"]', NULL, '2022-07-18 23:59:47', '2022-07-18 23:59:47'),
(270, 'App\\Models\\User', 1, 'myapptoken', 'a0207a0136dc873842c305c615faafb1844e49b7cec8d35074e6521b9ca9e2cb', '[\"*\"]', NULL, '2022-07-19 00:03:05', '2022-07-19 00:03:05'),
(271, 'App\\Models\\User', 2, 'myapptoken', '2f4d33cf332d0ac788c7d36a69e831120ace47106910af95ae34e7dc9416a3e2', '[\"*\"]', NULL, '2022-07-19 00:03:44', '2022-07-19 00:03:44'),
(272, 'App\\Models\\User', 1, 'myapptoken', 'be42216e8a52bbde61ad918ff4246f16ebe354c2eef51e4d1b9a8e86ceb0a930', '[\"*\"]', NULL, '2022-07-19 00:03:51', '2022-07-19 00:03:51'),
(273, 'App\\Models\\User', 1, 'myapptoken', 'd3a22ead4bef0b4ed9cf6dd8490c88f78b5dfa1db9bd384956c9b1c999c1eb6b', '[\"*\"]', NULL, '2022-07-19 00:04:08', '2022-07-19 00:04:08'),
(274, 'App\\Models\\User', 2, 'myapptoken', '66dd916bfc523c1b1acd5b1ffbfbcc0a470cfa90a2d6709abacdf396b8c431a6', '[\"*\"]', NULL, '2022-07-19 00:05:10', '2022-07-19 00:05:10'),
(275, 'App\\Models\\User', 1, 'myapptoken', '25aee2aabed1bfb03f38eb09b315d6ffa91b912fda78ed48ba56834dd7e65434', '[\"*\"]', NULL, '2022-07-19 00:06:26', '2022-07-19 00:06:26'),
(276, 'App\\Models\\User', 1, 'myapptoken', '94157339a9c5b4ce0ab60334d04b4b7c818e0126c7bb9bfa6a330ceff256828c', '[\"*\"]', NULL, '2022-07-19 00:07:05', '2022-07-19 00:07:05'),
(277, 'App\\Models\\User', 1, 'myapptoken', '823182cd9937eee5841ee7f90b933478c005eeb5c17b1b65d9a7d786277eb90a', '[\"*\"]', NULL, '2022-07-19 00:07:44', '2022-07-19 00:07:44'),
(278, 'App\\Models\\User', 1, 'myapptoken', 'f8ab3192ea517dd4ee936a2606c71e6d6bf1794e1e0e4a369c333f1746887bf3', '[\"*\"]', NULL, '2022-07-19 00:08:12', '2022-07-19 00:08:12'),
(279, 'App\\Models\\User', 2, 'myapptoken', 'a7b0fb6303a990752fb58ad76281a1e1fc94697aa28932ab16b28158383ed050', '[\"*\"]', NULL, '2022-07-19 00:09:28', '2022-07-19 00:09:28'),
(280, 'App\\Models\\User', 1, 'myapptoken', '5ef4c576a323514b4a8b5e7fe950f75bd2ed807c0c07b6e0ee604171249019b1', '[\"*\"]', NULL, '2022-07-19 00:09:54', '2022-07-19 00:09:54'),
(281, 'App\\Models\\User', 1, 'myapptoken', '142500c49c7d22bbd82a8c15394bc919ee7ec024813da67f43763bab503d0612', '[\"*\"]', NULL, '2022-07-19 00:11:32', '2022-07-19 00:11:32'),
(282, 'App\\Models\\User', 1, 'myapptoken', 'fd2f861167ea3d45f089dfe00109f09541905554ae419bab7d86a1ee12b1e4cb', '[\"*\"]', NULL, '2022-07-19 00:11:56', '2022-07-19 00:11:56'),
(283, 'App\\Models\\User', 1, 'myapptoken', 'e6770dd20ce7b1d14a9d0fb5257ba4ab9a466d7268ac7d9e170b5a2fbd3aeec7', '[\"*\"]', NULL, '2022-07-19 00:12:22', '2022-07-19 00:12:22'),
(284, 'App\\Models\\User', 1, 'myapptoken', '811f3983fe4f1ca34d35edf6c1f4f80c4a839ec2bceb9f048bb26a6e9ae990d5', '[\"*\"]', NULL, '2022-07-19 00:16:51', '2022-07-19 00:16:51'),
(285, 'App\\Models\\User', 1, 'myapptoken', 'ac5a02cfedafb6c7b7c09961f3a4d18619ec7f40b33477daf968c7a93dca0393', '[\"*\"]', NULL, '2022-07-19 00:26:00', '2022-07-19 00:26:00'),
(286, 'App\\Models\\User', 1, 'myapptoken', '4bfab1579b679481bf66e0c58b1e238bd75b9bc9444e554a57d1159288968dab', '[\"*\"]', NULL, '2022-07-19 00:58:28', '2022-07-19 00:58:28'),
(287, 'App\\Models\\User', 2, 'myapptoken', 'c53e6027cb9fcdc828098a7fdc5c657855d6e9f15eaf75eeca8cf919f8390ba5', '[\"*\"]', NULL, '2022-07-19 00:59:04', '2022-07-19 00:59:04'),
(288, 'App\\Models\\User', 1, 'myapptoken', 'ad6cc916c23981f66a0191d0d72fb755bcc40be8ad44cd04b348d3dd48de8028', '[\"*\"]', NULL, '2022-07-19 00:59:36', '2022-07-19 00:59:36'),
(289, 'App\\Models\\User', 1, 'myapptoken', '401d81d2927b9bf1d3a7e3d026b52be250610dbbe6d8bee1c2ea9518b7d7f312', '[\"*\"]', NULL, '2022-07-19 01:01:27', '2022-07-19 01:01:27'),
(290, 'App\\Models\\User', 1, 'myapptoken', '056dd8f92c7d2980f476deca8f7190ee02094ae62e1867793878fc92e9e4fabf', '[\"*\"]', NULL, '2022-07-19 01:05:53', '2022-07-19 01:05:53'),
(291, 'App\\Models\\User', 1, 'myapptoken', 'cf1b6aebef2be37cc3993c9d5bb6db62707a8ce33ae53d3cc0f23701b6188e71', '[\"*\"]', NULL, '2022-07-19 01:06:53', '2022-07-19 01:06:53'),
(292, 'App\\Models\\User', 1, 'myapptoken', '569fdeb0c7709285a1303ba4a0016d09ebacb24d6605022d30dac6a8f4698077', '[\"*\"]', NULL, '2022-07-19 05:22:48', '2022-07-19 05:22:48'),
(293, 'App\\Models\\User', 1, 'myapptoken', '3bdc83bf1afd1ec6fedeaaab5616a0fbf8d111b7268767a510a0fbcdfb0404f3', '[\"*\"]', NULL, '2022-07-19 05:23:50', '2022-07-19 05:23:50'),
(294, 'App\\Models\\User', 2, 'myapptoken', '38194cd9cd4c046ce52c0ec81069e515f0ab5bf16d66df31fb4357c5eede82ca', '[\"*\"]', NULL, '2022-07-19 05:25:40', '2022-07-19 05:25:40'),
(295, 'App\\Models\\User', 1, 'myapptoken', '9f2891d33030f81120f7eab0c2e644008126b54ae84c93f95fb5a08abeec8fcc', '[\"*\"]', NULL, '2022-07-19 05:26:11', '2022-07-19 05:26:11'),
(296, 'App\\Models\\User', 2, 'myapptoken', 'b8aeb3b914c0c719097cfa1544a2ecd4051cc8e5390d4d043a47f1196eccf0cd', '[\"*\"]', NULL, '2022-07-19 05:26:24', '2022-07-19 05:26:24'),
(297, 'App\\Models\\User', 1, 'myapptoken', 'a9db6fbbf44712a08648e9da3bea6b4c193b76b101ec8008433dce82cf899641', '[\"*\"]', NULL, '2022-07-19 05:29:45', '2022-07-19 05:29:45'),
(298, 'App\\Models\\User', 1, 'myapptoken', 'e0c6268b3f0556d7b0827147bb573c89d8ffb40364a7765703c09190fe5017b5', '[\"*\"]', NULL, '2022-07-19 06:50:50', '2022-07-19 06:50:50'),
(299, 'App\\Models\\User', 2, 'myapptoken', '6c6e35799ca032babc3de06b17a2218c1a6dfe5bf4c1a5b02ec1d0f52a040420', '[\"*\"]', NULL, '2022-07-19 06:51:03', '2022-07-19 06:51:03'),
(300, 'App\\Models\\User', 1, 'myapptoken', '0ba94dc1d66c56eec417ddf19790351bc4edae539a4b5764faa4cd51ac733b5e', '[\"*\"]', NULL, '2022-07-19 06:51:12', '2022-07-19 06:51:12'),
(301, 'App\\Models\\User', 2, 'myapptoken', '202acf36d0323214b63c9789f9817692aa0f48ce2d761cc7e35d031ffc830f95', '[\"*\"]', NULL, '2022-07-19 06:51:56', '2022-07-19 06:51:56'),
(302, 'App\\Models\\User', 1, 'myapptoken', 'cb5877b9ec0dacfceb333a12a50472fb373747306c4f83e50cff5c46d697e669', '[\"*\"]', NULL, '2022-07-19 06:52:39', '2022-07-19 06:52:39'),
(303, 'App\\Models\\User', 1, 'myapptoken', '9bb4e4a1638bbaf896cba5c36b44ce03d45ffc4ab9a4f6307ce4f2afeddc7f0f', '[\"*\"]', NULL, '2022-07-19 06:52:58', '2022-07-19 06:52:58'),
(304, 'App\\Models\\User', 1, 'myapptoken', '5a93b2ea7a69dbf2bc8dc5d93450906a4aaa8a9d8ad51d91b74e500eb47287b8', '[\"*\"]', NULL, '2022-07-19 06:53:06', '2022-07-19 06:53:06'),
(305, 'App\\Models\\User', 1, 'myapptoken', 'a9ef62390852aecfc83d4fe985c497035603aaee2670bd3b9837fb80b70a134a', '[\"*\"]', NULL, '2022-07-19 06:54:06', '2022-07-19 06:54:06'),
(306, 'App\\Models\\User', 1, 'myapptoken', '00b801867b88c3a91957ae410743163e7d2f86159c73fe3075a713b17c6cad80', '[\"*\"]', NULL, '2022-07-19 06:55:18', '2022-07-19 06:55:18'),
(307, 'App\\Models\\User', 1, 'myapptoken', '10ed740613442a4919282139fece4b70251538f3032514211e6c719a3a6f6e93', '[\"*\"]', NULL, '2022-07-19 06:58:04', '2022-07-19 06:58:04'),
(308, 'App\\Models\\User', 2, 'myapptoken', 'd920f7f1f05dc06d599745e9d8b2b433fde4d8fbf12a612d0ebc89d7db99ad76', '[\"*\"]', NULL, '2022-07-19 06:58:18', '2022-07-19 06:58:18'),
(309, 'App\\Models\\User', 2, 'myapptoken', 'c3a663c81d0e0369e9968c61ffce9e891e7c078a1c67982980d1c849604be694', '[\"*\"]', NULL, '2022-07-19 07:01:22', '2022-07-19 07:01:22'),
(310, 'App\\Models\\User', 2, 'myapptoken', 'f969b1ea962300ae3e97df21b5c5749206a3f7a878b4fa9d7976b5628d1a913d', '[\"*\"]', NULL, '2022-07-19 07:11:55', '2022-07-19 07:11:55'),
(311, 'App\\Models\\User', 1, 'myapptoken', 'f700dc20ecf6fb61b5a8bb4638216aeb75f5c23b14e254955115ddb623fff8b4', '[\"*\"]', NULL, '2022-07-19 08:19:33', '2022-07-19 08:19:33'),
(312, 'App\\Models\\User', 1, 'myapptoken', '9c10dc5583517b9acc7373f557d18ede7ca6e3d152aa0a012ce7b3fdcefdc712', '[\"*\"]', NULL, '2022-07-19 08:20:06', '2022-07-19 08:20:06'),
(313, 'App\\Models\\User', 1, 'myapptoken', 'd7a3f86b1c489e3865d632f9e793da343486beff85e713ae13c351c6e7924084', '[\"*\"]', NULL, '2022-07-19 08:29:54', '2022-07-19 08:29:54'),
(314, 'App\\Models\\User', 1, 'myapptoken', '0b9162172c7b9d76a9a9b6392f1a5c1f5c5039f929ebf014605f701ef30aeb65', '[\"*\"]', NULL, '2022-07-19 08:30:01', '2022-07-19 08:30:01'),
(315, 'App\\Models\\User', 1, 'myapptoken', '76b9faf16d0000f13ecbccb0dce8a3bcfcbfc5933fe31ffdcc91e0a3dcce2e53', '[\"*\"]', NULL, '2022-07-19 08:30:10', '2022-07-19 08:30:10');
INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(316, 'App\\Models\\User', 1, 'myapptoken', '45070fa293e198d3df75f7aff75584ac1f76416549ce008dfb2c8226ca24af6f', '[\"*\"]', NULL, '2022-07-19 08:30:32', '2022-07-19 08:30:32'),
(317, 'App\\Models\\User', 1, 'myapptoken', 'bd62a5a5af6c5210eb26af9acdcfe9586f42ad01e8ccaddf9e1e92b5696b735a', '[\"*\"]', NULL, '2022-07-19 09:07:30', '2022-07-19 09:07:30'),
(318, 'App\\Models\\User', 1, 'myapptoken', '75653d33cdba32f08b349d4836a72da61805672543874fde09016122cc8a344a', '[\"*\"]', NULL, '2022-07-19 09:09:01', '2022-07-19 09:09:01'),
(319, 'App\\Models\\User', 1, 'myapptoken', '0edc13ddcede75d3379b0ba1dde25fb77d861417aa1435b91149a4b1fe9593e8', '[\"*\"]', NULL, '2022-07-19 09:11:38', '2022-07-19 09:11:38'),
(320, 'App\\Models\\User', 1, 'myapptoken', 'b610714c350e444e815029594a01ace82dcf7e35cc4b3588478a5fd5d7de5713', '[\"*\"]', NULL, '2022-07-19 09:37:06', '2022-07-19 09:37:06'),
(321, 'App\\Models\\User', 1, 'myapptoken', 'fb4ef2c3f48643aa77ea5852ede871f9bb2f74b32e10325b1e6a93e2b1f3aa51', '[\"*\"]', NULL, '2022-07-19 09:40:55', '2022-07-19 09:40:55'),
(322, 'App\\Models\\User', 1, 'myapptoken', '14cd360b446e86af1f2fb9a0bb94cd4bf24913074b36076f5871b3f79ddf9fc3', '[\"*\"]', NULL, '2022-07-19 09:56:40', '2022-07-19 09:56:40'),
(323, 'App\\Models\\User', 1, 'myapptoken', '75f16c54a982aee86791d21633f43da32c3992f79894094d56044dbb59707dd2', '[\"*\"]', NULL, '2022-07-19 09:58:07', '2022-07-19 09:58:07'),
(324, 'App\\Models\\User', 1, 'myapptoken', '2a9901accd69a223f456f65f8c1737656b63c5a44e0885520aa4ad509134d2b2', '[\"*\"]', NULL, '2022-07-19 10:00:36', '2022-07-19 10:00:36'),
(325, 'App\\Models\\User', 1, 'myapptoken', '90873472e4d5ea54467dd614e5ae052888e99c235d2e66ebe2f6b118d3519e4b', '[\"*\"]', NULL, '2022-07-19 10:01:34', '2022-07-19 10:01:34'),
(326, 'App\\Models\\User', 1, 'myapptoken', 'c81722fc7c7719f7e4759b7933e38e24299ad6fcbfdb04af5376caa9d3125d6f', '[\"*\"]', NULL, '2022-07-19 10:07:21', '2022-07-19 10:07:21'),
(327, 'App\\Models\\User', 1, 'myapptoken', '4b5bec1443bf55d428bf9f2b2cf4c0dfd831de5f5252b3848d21cd6fb80618c8', '[\"*\"]', NULL, '2022-07-19 10:14:49', '2022-07-19 10:14:49'),
(328, 'App\\Models\\User', 1, 'myapptoken', '976b85edbd94f8af9e3f7dfcdf43b3b2bd66462906537d383aa758ee4f1107e3', '[\"*\"]', NULL, '2022-07-19 10:37:59', '2022-07-19 10:37:59'),
(329, 'App\\Models\\User', 2, 'myapptoken', '3867c5f5fce1b68788799c7bcce4c2dd76e357d39d8d89c6b39d420f975b9c9a', '[\"*\"]', NULL, '2022-07-19 10:38:32', '2022-07-19 10:38:32'),
(330, 'App\\Models\\User', 1, 'myapptoken', '894a69d3aed5f3647aac68cc4e008cdec97c97b3039937bb5112b56afd501611', '[\"*\"]', NULL, '2022-07-19 20:43:14', '2022-07-19 20:43:14'),
(331, 'App\\Models\\User', 1, 'myapptoken', 'db6483420d43554f6a268d9aef80a492d89de3857976d47b76f71a2e647641c7', '[\"*\"]', NULL, '2022-07-19 20:43:40', '2022-07-19 20:43:40'),
(332, 'App\\Models\\User', 2, 'myapptoken', 'f98d36ef79a1f1fca88c37febef0d5d65e9c65d49b1ebe8cc04bd579845733df', '[\"*\"]', NULL, '2022-07-19 20:44:26', '2022-07-19 20:44:26'),
(333, 'App\\Models\\User', 1, 'myapptoken', '939905c593aabf0673b583d9bdbdf106808073356f5a67a143e64df9618ca56b', '[\"*\"]', NULL, '2022-07-19 20:45:13', '2022-07-19 20:45:13'),
(334, 'App\\Models\\User', 2, 'myapptoken', '7a28b6cf6ae833636affb02ce91b57c085793969a07b957ef3af2ac92e3df4fd', '[\"*\"]', NULL, '2022-07-19 21:24:07', '2022-07-19 21:24:07'),
(335, 'App\\Models\\User', 2, 'myapptoken', '7d566eeb07eeaa925c9822a29d0523254e9c0de136a47c6bb120d6ab357f09b1', '[\"*\"]', NULL, '2022-07-19 21:26:04', '2022-07-19 21:26:04'),
(336, 'App\\Models\\User', 2, 'myapptoken', 'b2964a24a62c428dd9ebb3b2619255c2b3ad4ca834ff6902990feb47cbe3f948', '[\"*\"]', NULL, '2022-07-19 21:34:58', '2022-07-19 21:34:58'),
(337, 'App\\Models\\User', 1, 'myapptoken', 'b7b42cb05042c5c1dc3fbfa3ddb0fd55a736151a5ad161c1614e989525000c3c', '[\"*\"]', NULL, '2022-07-19 21:35:13', '2022-07-19 21:35:13'),
(338, 'App\\Models\\User', 2, 'myapptoken', 'd7c68f9e536bad23bab59d7435bbd4f45d18d26bf6ba66ae16e30a0b83b4b210', '[\"*\"]', NULL, '2022-07-19 21:35:39', '2022-07-19 21:35:39'),
(339, 'App\\Models\\User', 1, 'myapptoken', 'b2ea60d03773365c060aca9900f2d1aaa397a9188c34553e3f44bb31a018c128', '[\"*\"]', NULL, '2022-07-19 21:35:55', '2022-07-19 21:35:55'),
(340, 'App\\Models\\User', 2, 'myapptoken', 'f1c507575eeefd737f8b3022a144f32a76942b1e3717698374accf883cfe7097', '[\"*\"]', NULL, '2022-07-19 21:36:47', '2022-07-19 21:36:47'),
(341, 'App\\Models\\User', 1, 'myapptoken', '9c75a5e8f6ab3f3fbe66d1c0999ecba84f0b699e73c82b6291eed38c97ba84ec', '[\"*\"]', NULL, '2022-07-19 21:38:37', '2022-07-19 21:38:37'),
(342, 'App\\Models\\User', 1, 'myapptoken', '273f12a8c494d3535fe304f897a9d54dd2867367de74c98f55f44fbdeafc3c74', '[\"*\"]', NULL, '2022-07-19 21:39:02', '2022-07-19 21:39:02'),
(343, 'App\\Models\\User', 2, 'myapptoken', '17f40cf7dd567dc770ae25b6e606f4bcd05e9dfeabb4efd1b6dd4c196bb39be5', '[\"*\"]', NULL, '2022-07-19 21:39:12', '2022-07-19 21:39:12'),
(344, 'App\\Models\\User', 1, 'myapptoken', '76e2e95eee7e26990337515a2dce66b00c46a47846f402c9b4c8b278a1077a7f', '[\"*\"]', NULL, '2022-07-19 21:39:21', '2022-07-19 21:39:21'),
(345, 'App\\Models\\User', 2, 'myapptoken', '38f5dd3138ab2569aa58d28f8662666c141356f696c8d0566058dd5372b4bb6b', '[\"*\"]', NULL, '2022-07-19 21:39:46', '2022-07-19 21:39:46'),
(346, 'App\\Models\\User', 1, 'myapptoken', '42dc419f1ee8a4444b905dfbd08d705c4d350f7f1993d6bf01cb6479e4332f5c', '[\"*\"]', NULL, '2022-07-19 21:43:32', '2022-07-19 21:43:32'),
(347, 'App\\Models\\User', 2, 'myapptoken', '992a14858773f0e15d4c0dc3dc5b54daf6080dd40e7e0ad7b1fb519888d874ab', '[\"*\"]', NULL, '2022-07-19 21:44:52', '2022-07-19 21:44:52'),
(348, 'App\\Models\\User', 1, 'myapptoken', '8894702c1ba010ff7c3df43c13750f82dfa4f293133d797f8982d704f206f52d', '[\"*\"]', NULL, '2022-07-19 21:48:33', '2022-07-19 21:48:33'),
(349, 'App\\Models\\User', 1, 'myapptoken', '51711a1ce51bdb8c54260c73d72f47ff38d3d177fe64a0fb1965e85994f4b3bc', '[\"*\"]', NULL, '2022-07-19 21:56:48', '2022-07-19 21:56:48'),
(350, 'App\\Models\\User', 2, 'myapptoken', 'c153b9ada4d99f92537a85a0e54c6cb7fd3d943edab5b8138894cf1e66e204a5', '[\"*\"]', NULL, '2022-07-19 21:57:23', '2022-07-19 21:57:23'),
(351, 'App\\Models\\User', 1, 'myapptoken', 'a01b7951d76c942c19c3c63440a85a58d4797fd21f1b063abd7d56eeedafc243', '[\"*\"]', NULL, '2022-07-19 22:17:46', '2022-07-19 22:17:46'),
(352, 'App\\Models\\User', 1, 'myapptoken', 'e3f9d86b6a79c4454a16011a83e0535ddf4ee868b1aba0af1b9f3bd7497aaf86', '[\"*\"]', NULL, '2022-07-19 22:20:57', '2022-07-19 22:20:57'),
(353, 'App\\Models\\User', 2, 'myapptoken', '134799a5db32c82c1829eaaeaac666b7f27ce93092e315b191ed31e30d45bc64', '[\"*\"]', NULL, '2022-07-19 22:21:33', '2022-07-19 22:21:33'),
(354, 'App\\Models\\User', 1, 'myapptoken', 'ec6ef024736ae05ba9a846ab62865a50ba9cc88044f6e7bc114a681cb7ae2ee5', '[\"*\"]', NULL, '2022-07-19 22:22:07', '2022-07-19 22:22:07'),
(355, 'App\\Models\\User', 2, 'myapptoken', '19d473f3cd729d7a4c405a5a84b34e97ea0894d14ffb51b7a9f9b70c3e44e39d', '[\"*\"]', NULL, '2022-07-19 22:22:59', '2022-07-19 22:22:59'),
(356, 'App\\Models\\User', 1, 'myapptoken', 'c547a87f826d4b1b66ab7fe26e133d7e5e4a49c15fc7e96cd4faf15964965b2b', '[\"*\"]', NULL, '2022-07-19 22:23:17', '2022-07-19 22:23:17'),
(357, 'App\\Models\\User', 1, 'myapptoken', 'c8661aadc1d1363ac26b212f94f89c74a07b094081964d5c58d037f88608c4f5', '[\"*\"]', NULL, '2022-07-19 22:24:18', '2022-07-19 22:24:18'),
(358, 'App\\Models\\User', 2, 'myapptoken', '2d23e0e011c525da6f4d869b1736d19525fa9a3240fdb4996dfb6f375791b9fb', '[\"*\"]', NULL, '2022-07-19 22:27:58', '2022-07-19 22:27:58'),
(359, 'App\\Models\\User', 2, 'myapptoken', '7b9b0f0a9234622ea00b1e215d347d8f47cdb20a5b5201d60cc40473d0888858', '[\"*\"]', NULL, '2022-07-19 22:39:00', '2022-07-19 22:39:00'),
(360, 'App\\Models\\User', 1, 'myapptoken', '60ddad00867be1896bef1ceaf0e8e43ff10d2a1d55c3adea9d05f477976895f4', '[\"*\"]', NULL, '2022-07-19 23:29:15', '2022-07-19 23:29:15'),
(361, 'App\\Models\\User', 2, 'myapptoken', 'ec6499f4b6d394e28070d6e71a411264bea26e813cfe845147c564690f6b480e', '[\"*\"]', NULL, '2022-07-19 23:29:24', '2022-07-19 23:29:24'),
(362, 'App\\Models\\User', 1, 'myapptoken', 'f335d0673a3834cc7ba82c4d1f71096043a2631e98138a3d26a52875ad55b81c', '[\"*\"]', NULL, '2022-07-19 23:32:43', '2022-07-19 23:32:43'),
(363, 'App\\Models\\User', 1, 'myapptoken', 'b7c1b6f6fcb9f27b4b012430e95807e9c29fc34fce433e97bbd0df94dea7c36b', '[\"*\"]', NULL, '2022-07-19 23:32:52', '2022-07-19 23:32:52'),
(364, 'App\\Models\\User', 2, 'myapptoken', 'c0d3b574016c99c80676837b9e5b6a0ddc8b363d4e6fa213b6bac5a222b962eb', '[\"*\"]', NULL, '2022-07-19 23:33:10', '2022-07-19 23:33:10'),
(365, 'App\\Models\\User', 2, 'myapptoken', '8ade9aacf9ededb585e742fc0af6b23262f4c475c918a5036540134bfdc72d03', '[\"*\"]', NULL, '2022-07-20 00:02:32', '2022-07-20 00:02:32'),
(366, 'App\\Models\\User', 2, 'myapptoken', 'dca2e3680a0c9d78ad9f52effab11b6fc9688788e2788f105e7693dac6aa6ad4', '[\"*\"]', NULL, '2022-07-20 00:14:05', '2022-07-20 00:14:05'),
(367, 'App\\Models\\User', 2, 'myapptoken', 'daca273ff2c71ff3742aad6d2a162bf318d8d45d05cf15c276179bc9cc095ae3', '[\"*\"]', NULL, '2022-07-20 00:17:59', '2022-07-20 00:17:59'),
(368, 'App\\Models\\User', 2, 'myapptoken', 'd9438b6e44b7f0179294f4c0c79aa8836f08ef704b0367dd7926c16fc6f59eb3', '[\"*\"]', NULL, '2022-07-20 00:35:32', '2022-07-20 00:35:32'),
(369, 'App\\Models\\User', 1, 'myapptoken', 'fd8fb6442ea1e6b81b0d88cfed81124469db0532f84ee8df8ccdc11fd28cff7f', '[\"*\"]', NULL, '2022-07-20 00:58:58', '2022-07-20 00:58:58'),
(370, 'App\\Models\\User', 1, 'myapptoken', '5c4305633d1dc7c2614524a44f512c50baf311e7ea3fdc0e6f07507ec6d75843', '[\"*\"]', NULL, '2022-07-20 00:59:10', '2022-07-20 00:59:10'),
(371, 'App\\Models\\User', 2, 'myapptoken', 'be9bd04252bed093a70eea2cc57e042760fea044781e8a1547d056b2895740ba', '[\"*\"]', NULL, '2022-07-20 00:59:41', '2022-07-20 00:59:41'),
(372, 'App\\Models\\User', 1, 'myapptoken', 'd97a0620500fa52c55b1f55b71b1ca778011e66283d9bf20d2a405279a97adde', '[\"*\"]', NULL, '2022-07-20 01:00:04', '2022-07-20 01:00:04'),
(373, 'App\\Models\\User', 2, 'myapptoken', '9363ea0753fbde76b211a3ee5429db8cf7b15211764071d8f50780236b0ee33c', '[\"*\"]', NULL, '2022-07-20 01:01:28', '2022-07-20 01:01:28'),
(374, 'App\\Models\\User', 1, 'myapptoken', '2075a39fb36f187363b88a1c5fd835974a8df830d7d7ccaab3a3a16aeae09679', '[\"*\"]', NULL, '2022-07-20 01:56:18', '2022-07-20 01:56:18'),
(375, 'App\\Models\\User', 1, 'myapptoken', '5ffdf2af474caabc01ad49fb64da6100531a9865a290265e1dc9b6a19dcd6c82', '[\"*\"]', NULL, '2022-07-20 01:56:55', '2022-07-20 01:56:55'),
(376, 'App\\Models\\User', 2, 'myapptoken', '5be8ed8370f3876194ca33a4e25e24d63c6ae215ab11e54119aec30451d5651d', '[\"*\"]', NULL, '2022-07-20 01:57:36', '2022-07-20 01:57:36'),
(377, 'App\\Models\\User', 1, 'myapptoken', '6c5b10fa4fa2263024b7cf492fd6a84846e7cbbff669e3c86a222c43e1232baa', '[\"*\"]', NULL, '2022-07-20 01:57:59', '2022-07-20 01:57:59'),
(378, 'App\\Models\\User', 2, 'myapptoken', '8223af55c626beb6ef037ca281954d6d70f918cf32e5a9e6d092611b07c4e2db', '[\"*\"]', NULL, '2022-07-20 02:00:07', '2022-07-20 02:00:07'),
(379, 'App\\Models\\User', 1, 'myapptoken', '1cfa30b95cf7050b3bd53f2d24218382679ae631345496ea8354eb7b84a5fb4d', '[\"*\"]', NULL, '2022-07-20 02:04:47', '2022-07-20 02:04:47'),
(380, 'App\\Models\\User', 1, 'myapptoken', '48bb0f348d40b78be969125e32cfadb28247c3abb9f835048eb542bd4f6fc666', '[\"*\"]', NULL, '2022-07-20 02:04:58', '2022-07-20 02:04:58'),
(381, 'App\\Models\\User', 2, 'myapptoken', '0284f6cbfb6433a17a45e4f1f28f2359ea1f88c9701e28fa37faa9b1c40cb1cd', '[\"*\"]', NULL, '2022-07-20 02:05:16', '2022-07-20 02:05:16'),
(382, 'App\\Models\\User', 1, 'myapptoken', 'bbd83d4d2321c59b8569ba8785429aa659de29db0a42eba3994439f2ffdfd877', '[\"*\"]', NULL, '2022-07-20 02:07:49', '2022-07-20 02:07:49'),
(383, 'App\\Models\\User', 1, 'myapptoken', 'a98c128c5cb4241a7a5ca6c4d77fba5099f9e3f6c0f0d635cb718df5f345e301', '[\"*\"]', NULL, '2022-07-20 02:29:32', '2022-07-20 02:29:32'),
(384, 'App\\Models\\User', 2, 'myapptoken', '8a3bdac67a7c55b65a72a4e1a934a0f90b10be696950ec89346afa1c052f0ad4', '[\"*\"]', NULL, '2022-07-20 02:37:10', '2022-07-20 02:37:10'),
(385, 'App\\Models\\User', 1, 'myapptoken', '7279aae1df8569c482a9f15702e0390f4d4a69b2a0664e4e08f50859579e1f39', '[\"*\"]', NULL, '2022-07-20 02:45:41', '2022-07-20 02:45:41'),
(386, 'App\\Models\\User', 1, 'myapptoken', 'd5544da73078f31ee564a1578c9860e226440adb2efd278c9118b7b4c106b1dc', '[\"*\"]', NULL, '2022-07-20 02:46:36', '2022-07-20 02:46:36'),
(387, 'App\\Models\\User', 1, 'myapptoken', 'ec2ea92c70e166730e48abea2aba4221b8bbaad14f1e71fa9cb00f04cb259535', '[\"*\"]', NULL, '2022-07-20 02:52:06', '2022-07-20 02:52:06'),
(388, 'App\\Models\\User', 1, 'myapptoken', '062317faaef812609a82877ae76733ea45f51935f86ee83ecd71d489c6639ac9', '[\"*\"]', NULL, '2022-07-20 02:55:02', '2022-07-20 02:55:02'),
(389, 'App\\Models\\User', 1, 'myapptoken', '1f0a98ef6f1ba3918d564ae4978ab160fa0d372778fbaeb9399d69e06b6a2b33', '[\"*\"]', NULL, '2022-07-20 03:51:04', '2022-07-20 03:51:04'),
(390, 'App\\Models\\User', 2, 'myapptoken', '28ed36c8d59038e4c9453cbc24feebaba6fe4a35b8a61fa1bd59beb45c9ad41e', '[\"*\"]', NULL, '2022-07-20 03:52:07', '2022-07-20 03:52:07'),
(391, 'App\\Models\\User', 2, 'myapptoken', '5af916d837aa5b4bd29de328ce1b97e0d2b0054cdef0d01564710a27ab6d83a1', '[\"*\"]', NULL, '2022-07-20 04:06:28', '2022-07-20 04:06:28'),
(392, 'App\\Models\\User', 1, 'myapptoken', 'fdc4908b6f5d111762d7c65078a75e44477ea2da04683d0e8a85590c51cab404', '[\"*\"]', NULL, '2022-07-20 04:20:21', '2022-07-20 04:20:21'),
(393, 'App\\Models\\User', 2, 'myapptoken', 'cea20e517a1983a1207568706a854d97d56e403b2d9b9fff3f78d31906710ae3', '[\"*\"]', NULL, '2022-07-20 04:22:38', '2022-07-20 04:22:38'),
(394, 'App\\Models\\User', 2, 'myapptoken', 'af92d205e07ed4fea6236622aecabb3501fcee671525c15f97702d532a379860', '[\"*\"]', NULL, '2022-07-20 04:23:22', '2022-07-20 04:23:22'),
(395, 'App\\Models\\User', 1, 'myapptoken', '0b92b119054360350855d4f18ccce8e2634cc82b44ab53ca5cf8eab72b5c0b2c', '[\"*\"]', NULL, '2022-07-20 04:35:03', '2022-07-20 04:35:03'),
(396, 'App\\Models\\User', 2, 'myapptoken', 'ed1ccc10f7745e32a1884b28ac46548683a5a69ab14a49034a313ba159e02c14', '[\"*\"]', NULL, '2022-07-20 04:35:24', '2022-07-20 04:35:24'),
(397, 'App\\Models\\User', 2, 'myapptoken', '2c44c49be756bd00d8eb0a3edd059271cb352d8cfa52b7b463dbc81575fcacf5', '[\"*\"]', NULL, '2022-07-20 04:40:36', '2022-07-20 04:40:36'),
(398, 'App\\Models\\User', 1, 'myapptoken', '5e1e44798659d9000d3a36565c4d16f8de4c6fc5e6c7130340ed3045742fc333', '[\"*\"]', NULL, '2022-07-20 04:47:43', '2022-07-20 04:47:43'),
(399, 'App\\Models\\User', 1, 'myapptoken', '6f09958be8c0480191eab8d5d5590968ab0f840513ede59965e62bb92866c13b', '[\"*\"]', NULL, '2022-07-20 04:49:20', '2022-07-20 04:49:20'),
(400, 'App\\Models\\User', 1, 'myapptoken', '6b4c9456b244380ea957e7af29e136a41a50ebbb13a49d824dc2bd4e8e019517', '[\"*\"]', NULL, '2022-07-20 05:09:06', '2022-07-20 05:09:06'),
(401, 'App\\Models\\User', 1, 'myapptoken', 'd3e63dd1d109d5db30568d634753e99fb72cdc8da43a2d4ae0a509591271bfa6', '[\"*\"]', NULL, '2022-07-20 05:15:20', '2022-07-20 05:15:20'),
(402, 'App\\Models\\User', 2, 'myapptoken', '4a0340adda3f7876f1c45d30c16e04eb1c2b7fcb913af36290d97ddc2d065e66', '[\"*\"]', NULL, '2022-07-20 05:35:49', '2022-07-20 05:35:49'),
(403, 'App\\Models\\User', 2, 'myapptoken', 'cd75769a2e8a14944e093c02b095eba46d663d0e95fc2531f1559c1000aae774', '[\"*\"]', NULL, '2022-07-20 06:12:37', '2022-07-20 06:12:37'),
(404, 'App\\Models\\User', 1, 'myapptoken', '7848a66d2db9dd40e861f11b657264b3dcb87d243fe5850ff4ef48416172816b', '[\"*\"]', NULL, '2022-07-20 06:13:32', '2022-07-20 06:13:32'),
(405, 'App\\Models\\User', 1, 'myapptoken', '07df65939ad3e6a21c8aefea2434e5a12b515c6ed50b29ffc58bb6a162d41bcb', '[\"*\"]', NULL, '2022-07-20 06:19:30', '2022-07-20 06:19:30'),
(406, 'App\\Models\\User', 1, 'myapptoken', 'de6d2e262d56ca03c81bb7508909caa5163072d5bfc94ace0b34ebe901136ab5', '[\"*\"]', NULL, '2022-07-20 06:42:00', '2022-07-20 06:42:00'),
(407, 'App\\Models\\User', 2, 'myapptoken', '584e2e0a29e2388aaeb118c8c7ec4b0fedac208aaa3de4c10ec3516577855ca8', '[\"*\"]', NULL, '2022-07-20 06:42:29', '2022-07-20 06:42:29'),
(408, 'App\\Models\\User', 2, 'myapptoken', '549742eabdc3b8c85ed103ba348554e11cb332d7f7900876b1c53e5154342d6d', '[\"*\"]', NULL, '2022-07-20 07:01:56', '2022-07-20 07:01:56'),
(409, 'App\\Models\\User', 1, 'myapptoken', '54a5ec6b2584c4d1f6c0985f2084fb2992a75287cc848b92490f11379d6637f6', '[\"*\"]', NULL, '2022-07-20 07:10:57', '2022-07-20 07:10:57'),
(410, 'App\\Models\\User', 2, 'myapptoken', '54defc63cad8bf18bc60c106f1f8b59f64d3ca8f66949a2ef9dd0fa3b0244dee', '[\"*\"]', NULL, '2022-07-20 07:13:35', '2022-07-20 07:13:35'),
(411, 'App\\Models\\User', 1, 'myapptoken', 'e2b43457a4bf791b6f3d069cf72a887e2cf44eb4ae0db83ca7c5488b00a58f6b', '[\"*\"]', NULL, '2022-07-20 07:15:31', '2022-07-20 07:15:31'),
(412, 'App\\Models\\User', 1, 'myapptoken', 'af89664b1ced608219b7c1b2af4ec1f1644d3ef84809c62849ba57d5a2e1cf6e', '[\"*\"]', NULL, '2022-07-20 07:15:57', '2022-07-20 07:15:57'),
(413, 'App\\Models\\User', 1, 'myapptoken', 'b7cedc2c3b0a3a11c593f28f3e2c554eaf6d3ed7f45cd851e235270123bd6a7b', '[\"*\"]', NULL, '2022-07-20 09:49:43', '2022-07-20 09:49:43'),
(414, 'App\\Models\\User', 2, 'myapptoken', '442d54b9552d7c6dd19662bba44570cf576f8c1fee05e9a2da7fc23bce5adaa5', '[\"*\"]', NULL, '2022-07-20 09:51:07', '2022-07-20 09:51:07'),
(415, 'App\\Models\\User', 1, 'myapptoken', 'e5ae387587e2217f7ab9f972000547c6aabd87e01a5dae17cd77cf5231cc6e12', '[\"*\"]', NULL, '2022-07-20 09:51:17', '2022-07-20 09:51:17'),
(416, 'App\\Models\\User', 2, 'myapptoken', '25ca1d804aa55e4f82476ee0ce192e8c2ad42da0e6f840decc62c6d2cd12e3a5', '[\"*\"]', NULL, '2022-07-20 09:51:54', '2022-07-20 09:51:54'),
(417, 'App\\Models\\User', 1, 'myapptoken', 'a2ce6a6d86226cd93ed7d42609f398eeac6d9f453f76b121e8d0aa6bd217dfd3', '[\"*\"]', NULL, '2022-07-20 09:52:45', '2022-07-20 09:52:45'),
(418, 'App\\Models\\User', 1, 'myapptoken', 'a34fdda0275aa8bfdfb9aa223ebe57102e11961da5813e82fab0fc8cb7ce1294', '[\"*\"]', NULL, '2022-07-20 09:53:14', '2022-07-20 09:53:14'),
(419, 'App\\Models\\User', 2, 'myapptoken', 'af91b6b4335e4406b9e7a2ab9f97f846575c6716deac53a08f9b9b3bf5b7047e', '[\"*\"]', NULL, '2022-07-20 09:54:00', '2022-07-20 09:54:00'),
(420, 'App\\Models\\User', 1, 'myapptoken', '466553fb9252b85d86f03e1c30c341af222ef1227e87cb73cf976082eab454ba', '[\"*\"]', NULL, '2022-07-20 09:55:35', '2022-07-20 09:55:35'),
(421, 'App\\Models\\User', 1, 'myapptoken', 'b53c5a12c725592cafd8c281c4fe6ff8f3fd3e82834946978f85bbfcf8cfd16f', '[\"*\"]', NULL, '2022-07-20 10:04:32', '2022-07-20 10:04:32'),
(422, 'App\\Models\\User', 2, 'myapptoken', 'cb3d831e356591d9ce32b355ad5a7d5cbd5c9d7389da62ab247532165b081a61', '[\"*\"]', NULL, '2022-07-20 10:05:32', '2022-07-20 10:05:32'),
(423, 'App\\Models\\User', 1, 'myapptoken', 'b73b4ba5c51f0692d7727e723501b5b3723873449d922b8a5bc583a4e9aac7c4', '[\"*\"]', NULL, '2022-07-20 10:07:54', '2022-07-20 10:07:54'),
(424, 'App\\Models\\User', 2, 'myapptoken', '3cf8a5dd7a29e173afcd8397bfc4db2339ccaa0d10b417d4a3332563d40c3547', '[\"*\"]', NULL, '2022-07-20 10:08:21', '2022-07-20 10:08:21'),
(425, 'App\\Models\\User', 1, 'myapptoken', '31144254cfcb6b4567e30ba43506608bbcd8ab4968b1e86a11535d733a9249da', '[\"*\"]', NULL, '2022-07-20 10:10:47', '2022-07-20 10:10:47'),
(426, 'App\\Models\\User', 1, 'myapptoken', 'f1755ed8917468dd2938a770e0e765cdcbbd8da54b524bc8a3ae9a9d142bcb02', '[\"*\"]', NULL, '2022-07-20 10:10:58', '2022-07-20 10:10:58'),
(427, 'App\\Models\\User', 1, 'myapptoken', '67d9a57bd08b692bed3d994d36f424a074a35fc7c3ff308868226973e4525758', '[\"*\"]', NULL, '2022-07-20 10:11:10', '2022-07-20 10:11:10'),
(428, 'App\\Models\\User', 1, 'myapptoken', '794cc3caa8af558b980534f600c88ba259ea5db2dade06f098d3d1e4ae8fa943', '[\"*\"]', NULL, '2022-07-20 10:22:59', '2022-07-20 10:22:59'),
(429, 'App\\Models\\User', 1, 'myapptoken', 'ad55092a45a340fb959e7b0ad870822a2d6d2fd245780fa2a8bd8fc25b9316d1', '[\"*\"]', NULL, '2022-07-20 22:18:16', '2022-07-20 22:18:16'),
(430, 'App\\Models\\User', 2, 'myapptoken', '506e09720e1acb50f887a4f73045a960b690bf202f03f4d3226e3a09289565f8', '[\"*\"]', NULL, '2022-07-20 22:18:53', '2022-07-20 22:18:53'),
(431, 'App\\Models\\User', 1, 'myapptoken', 'c533393429283fa36aa9c2573b567974a71f75997dd98610568124b9a1cc724c', '[\"*\"]', NULL, '2022-07-20 22:19:18', '2022-07-20 22:19:18'),
(432, 'App\\Models\\User', 2, 'myapptoken', '07326c99977bf64a39d42d2ff1a4e3206a5c1ab2ad7d4ca52d8e465e608d03ab', '[\"*\"]', NULL, '2022-07-20 23:12:03', '2022-07-20 23:12:03'),
(433, 'App\\Models\\User', 1, 'myapptoken', 'd9c3526bad6cb5f2c42d4a0339a1269786dd5f1fc72b2a6f6dafbdc78d987438', '[\"*\"]', NULL, '2022-07-20 23:20:30', '2022-07-20 23:20:30'),
(434, 'App\\Models\\User', 2, 'myapptoken', 'b268e8eb19a7263358f5421b9129d17f152800abba82bed5bca07cdd214cdee7', '[\"*\"]', NULL, '2022-07-20 23:20:57', '2022-07-20 23:20:57'),
(435, 'App\\Models\\User', 1, 'myapptoken', '1138b9d5ed832afefcf860aecc13b31a17c115587bed48184286e1628cc67850', '[\"*\"]', NULL, '2022-07-20 23:43:24', '2022-07-20 23:43:24'),
(436, 'App\\Models\\User', 2, 'myapptoken', 'a0c41f835556fd6a23d95d9df9d3af21f0fb8adbdb901b450192b0b97dad5adc', '[\"*\"]', NULL, '2022-07-20 23:46:29', '2022-07-20 23:46:29'),
(437, 'App\\Models\\User', 2, 'myapptoken', 'e33f28b43ee9a70420303cd0c47f38c159ddc31613a9df27a8d64e7cb46d32a2', '[\"*\"]', NULL, '2022-07-20 23:52:34', '2022-07-20 23:52:34'),
(438, 'App\\Models\\User', 1, 'myapptoken', '3b311d7eb57597c10b830422341b8e84349a0b7c306d5fc9cd6c0155de37b45f', '[\"*\"]', NULL, '2022-07-20 23:52:51', '2022-07-20 23:52:51'),
(439, 'App\\Models\\User', 1, 'myapptoken', '06537b32e3c6fc03a61c4cc8bd4dec27723946f35e870f11355642f9298262d6', '[\"*\"]', NULL, '2022-07-21 00:16:08', '2022-07-21 00:16:08'),
(440, 'App\\Models\\User', 1, 'myapptoken', 'f6fef241ca067f54b21557f227f67286d324b1c2f9e054a2c5cf1bd8e3653413', '[\"*\"]', NULL, '2022-07-21 00:34:12', '2022-07-21 00:34:12'),
(441, 'App\\Models\\User', 2, 'myapptoken', '24503c78f7ad52cad391a8e273eb9ed082c51c681419210e1b4b10f324772878', '[\"*\"]', NULL, '2022-07-21 00:46:25', '2022-07-21 00:46:25'),
(442, 'App\\Models\\User', 1, 'myapptoken', '4c069db447f4dc12dc2c5e69d8a6bf384fe32a529fe6efa27722fd19a4c3727a', '[\"*\"]', NULL, '2022-07-21 00:48:45', '2022-07-21 00:48:45'),
(443, 'App\\Models\\User', 2, 'myapptoken', '3de4c1164ead3fac9913e7598c2740fd89cc2aae93b9455da995e885c5115496', '[\"*\"]', NULL, '2022-07-21 00:52:13', '2022-07-21 00:52:13'),
(444, 'App\\Models\\User', 1, 'myapptoken', 'aaf8cf040b839743766b7efa8481c122d45f041e7242d8a109669a40ceaab4c2', '[\"*\"]', NULL, '2022-07-21 00:53:29', '2022-07-21 00:53:29'),
(445, 'App\\Models\\User', 1, 'myapptoken', '159cd35db70052b6c06491766228bfda18c6eda48976c20fe17a3f99936e4790', '[\"*\"]', NULL, '2022-07-21 00:56:12', '2022-07-21 00:56:12'),
(446, 'App\\Models\\User', 2, 'myapptoken', '9281d55e66ed19c5701e244be84a3334c7e4a27b0bcbdac2e1e33bc55915853d', '[\"*\"]', NULL, '2022-07-21 00:56:35', '2022-07-21 00:56:35'),
(447, 'App\\Models\\User', 2, 'myapptoken', 'a14819437554f01d3e0bcd12be9537f88b87170675e83d19e6109120694d982c', '[\"*\"]', NULL, '2022-07-21 00:57:30', '2022-07-21 00:57:30'),
(448, 'App\\Models\\User', 1, 'myapptoken', 'e4015d7f6d2cb620f74cd60c6d4729c80eaee99fd18d356a514a6dee63415ca0', '[\"*\"]', NULL, '2022-07-21 00:57:38', '2022-07-21 00:57:38'),
(449, 'App\\Models\\User', 2, 'myapptoken', '23d0acb8093d64d0ddab8b84481874c5a4a72733cd991aff431742f5ae8bb289', '[\"*\"]', NULL, '2022-07-21 00:58:20', '2022-07-21 00:58:20'),
(450, 'App\\Models\\User', 1, 'myapptoken', 'c525aaafcd2db146e99d97e5f6784c54cb909e4309ee407911ebe55851a846e6', '[\"*\"]', NULL, '2022-07-21 01:05:15', '2022-07-21 01:05:15'),
(451, 'App\\Models\\User', 1, 'myapptoken', '5b0bd54f9cedba3716e53ffe5c8130943268fb805b1b803b0e3c5c789c2fc708', '[\"*\"]', NULL, '2022-07-21 01:38:23', '2022-07-21 01:38:23'),
(452, 'App\\Models\\User', 1, 'myapptoken', 'a9f4bd833b36460cff23d9da475670babf424996d5ced895874fdf1920390593', '[\"*\"]', NULL, '2022-07-21 01:51:22', '2022-07-21 01:51:22'),
(453, 'App\\Models\\User', 2, 'myapptoken', '222fca3871d90da4b4076adeb784a75a972d3cc8806d932a15b6abced6700c85', '[\"*\"]', NULL, '2022-07-21 02:14:34', '2022-07-21 02:14:34'),
(454, 'App\\Models\\User', 1, 'myapptoken', 'acaef1465f8d58117cfbc4820cebd70e94e359b5eed45dc5b7f62441da69b05b', '[\"*\"]', NULL, '2022-07-21 02:16:52', '2022-07-21 02:16:52'),
(455, 'App\\Models\\User', 1, 'myapptoken', '7e56e623353d104fa982b9da92fae738081fa38146efeee64a1eb30fabd8cfd1', '[\"*\"]', NULL, '2022-07-21 02:17:00', '2022-07-21 02:17:00'),
(456, 'App\\Models\\User', 2, 'myapptoken', '349d192afe5906dccdb3a0703c142031d68eb2f852aae1402db2c17452d6f103', '[\"*\"]', NULL, '2022-07-21 02:17:18', '2022-07-21 02:17:18'),
(457, 'App\\Models\\User', 1, 'myapptoken', 'cf30504a086566c3ab9b1ab80f96b5d7327684c975811e9ae4af76e59afd206c', '[\"*\"]', NULL, '2022-07-21 02:22:15', '2022-07-21 02:22:15'),
(458, 'App\\Models\\User', 1, 'myapptoken', 'ea83d57987edab8b1596cb7d63c6fa6c07ba42f29553ee9fe01b058a458a5dd4', '[\"*\"]', NULL, '2022-07-21 03:06:38', '2022-07-21 03:06:38'),
(459, 'App\\Models\\User', 2, 'myapptoken', '0cccc9e2a71323fc6eb48def5f1333c9e411e44cc46cb8a6b94fdc1bf286b710', '[\"*\"]', NULL, '2022-07-21 03:07:03', '2022-07-21 03:07:03'),
(460, 'App\\Models\\User', 1, 'myapptoken', '49c56cc63f72e56f715d8254fb7c8174e764fc3d9790dfef1869eec3f208393b', '[\"*\"]', NULL, '2022-07-21 03:08:11', '2022-07-21 03:08:11'),
(461, 'App\\Models\\User', 2, 'myapptoken', '41217966677f5f8ac275a6acb1f00f8846dcd68bbb66113fe6ab648d645839cd', '[\"*\"]', NULL, '2022-07-21 03:20:35', '2022-07-21 03:20:35'),
(462, 'App\\Models\\User', 1, 'myapptoken', '0db3a461016c424ae1fd55e63c6ecf08455641ec1ba6fb3adab46bff5f870c74', '[\"*\"]', NULL, '2022-07-21 03:24:35', '2022-07-21 03:24:35'),
(463, 'App\\Models\\User', 2, 'myapptoken', 'fa2bad55b95e41dc6c46dcbecb45ca288ffb7e6ad9fd8629665d7c58c4c22a81', '[\"*\"]', NULL, '2022-07-21 03:27:29', '2022-07-21 03:27:29'),
(464, 'App\\Models\\User', 1, 'myapptoken', 'f6b6d51f46fc04d0d950ba589d494efdf6c9c5f6df680fbee2555d234e47b1f4', '[\"*\"]', NULL, '2022-07-21 03:27:41', '2022-07-21 03:27:41'),
(465, 'App\\Models\\User', 1, 'myapptoken', '0639223f2ce7487d1d7fac8e748d2afe76d9fc8a51de953e2ad50647abd4d7f5', '[\"*\"]', NULL, '2022-07-21 03:28:05', '2022-07-21 03:28:05'),
(466, 'App\\Models\\User', 1, 'myapptoken', 'b734a346b67f22b8f25c3a6fccc5e717d0ce610677864927a50348236f0826f0', '[\"*\"]', NULL, '2022-07-21 03:28:38', '2022-07-21 03:28:38'),
(467, 'App\\Models\\User', 1, 'myapptoken', '85b6ec53d62d73ccdb7c81bc624e0746c44ab62da80baaf8e2f5c359f8bf0b76', '[\"*\"]', NULL, '2022-07-21 03:28:55', '2022-07-21 03:28:55'),
(468, 'App\\Models\\User', 2, 'myapptoken', '7172e2186693050f8b34da5b3e9b495a8014bd6050456f526184822316e10818', '[\"*\"]', NULL, '2022-07-21 04:12:52', '2022-07-21 04:12:52'),
(469, 'App\\Models\\User', 1, 'myapptoken', '7a1159011c2c22e707fbaa8068cde3d9e46aba2b9ff33a9f9b342359b99b7dd6', '[\"*\"]', NULL, '2022-07-21 04:13:26', '2022-07-21 04:13:26'),
(470, 'App\\Models\\User', 2, 'myapptoken', '451daff37ee72a8b1e0c411514b724e9f1ae050c96409c7e0fc872ac2e88f897', '[\"*\"]', NULL, '2022-07-21 04:14:09', '2022-07-21 04:14:09'),
(471, 'App\\Models\\User', 2, 'myapptoken', '413de74bee2799f321438debe3e4bce6f678579b14e2c1069efcd587f78ab5f3', '[\"*\"]', NULL, '2022-07-21 04:28:12', '2022-07-21 04:28:12'),
(472, 'App\\Models\\User', 1, 'myapptoken', '5080d504e225807a0775588ae99f597c476a410ad0c98e7f01211ab06189ad89', '[\"*\"]', NULL, '2022-07-21 04:53:07', '2022-07-21 04:53:07'),
(473, 'App\\Models\\User', 2, 'myapptoken', '4d5ed93d6c651a5431cd67b6b5c0f7cb1cf1c36c5b1f502179f06894995df093', '[\"*\"]', NULL, '2022-07-21 04:54:27', '2022-07-21 04:54:27'),
(474, 'App\\Models\\User', 1, 'myapptoken', 'e0d0db6fc8b12b0c20b7b24bc0694602ca3b756d55507dfbb90631d0feb0e4bf', '[\"*\"]', NULL, '2022-07-21 05:08:06', '2022-07-21 05:08:06'),
(475, 'App\\Models\\User', 1, 'myapptoken', '4463ea5db820a45880337b9cd10a1920be2c63ccd34e8d3979b5d6f17751eb84', '[\"*\"]', NULL, '2022-07-21 05:12:56', '2022-07-21 05:12:56'),
(476, 'App\\Models\\User', 2, 'myapptoken', '794eb50f3655a34e47fe04fd63d61b2efc532bed2f08dd89e34bc594b6c89535', '[\"*\"]', NULL, '2022-07-21 05:20:17', '2022-07-21 05:20:17'),
(477, 'App\\Models\\User', 1, 'myapptoken', '25706fbe34e4c7b290deca7900faf16be310da03262740c2f33630bd607780ae', '[\"*\"]', NULL, '2022-07-21 05:22:50', '2022-07-21 05:22:50'),
(478, 'App\\Models\\User', 2, 'myapptoken', '92228d0e3a10f4e0fbca1a4845f45e9c6a7a03473acb11e7ac0e717609930837', '[\"*\"]', NULL, '2022-07-21 05:23:25', '2022-07-21 05:23:25'),
(479, 'App\\Models\\User', 2, 'myapptoken', 'dfe4fcb7d0187d8177467c1b48734f37a39327d8c5e2cd72d6fda10f0fa901f0', '[\"*\"]', NULL, '2022-07-21 06:18:05', '2022-07-21 06:18:05'),
(480, 'App\\Models\\User', 1, 'myapptoken', '287ea64408addf32487bdc992c9feb82d24ad8362da8403baa042afba22ff566', '[\"*\"]', NULL, '2022-07-21 06:27:39', '2022-07-21 06:27:39'),
(481, 'App\\Models\\User', 2, 'myapptoken', '146368ae2427923a453be1193285aa35b1b66e9c86888866d8038861d9eeaf82', '[\"*\"]', NULL, '2022-07-21 06:34:50', '2022-07-21 06:34:50'),
(482, 'App\\Models\\User', 2, 'myapptoken', '60116e70eff9f8b5d9c0eeefa253a90d3447e68193fd73d020536ca8961d4701', '[\"*\"]', NULL, '2022-07-21 06:43:58', '2022-07-21 06:43:58'),
(483, 'App\\Models\\User', 2, 'myapptoken', '4168e3287df172d895c8ab7e1cdd5ae06bfdcbde6fee3bc99ede88e9a5af2432', '[\"*\"]', NULL, '2022-07-21 06:54:10', '2022-07-21 06:54:10'),
(484, 'App\\Models\\User', 1, 'myapptoken', '7d5ad51e4ca57bb1607d445f1ff6d477db6a84ea347bf12fcac0ff3ee8dd3e08', '[\"*\"]', NULL, '2022-07-21 06:54:48', '2022-07-21 06:54:48'),
(485, 'App\\Models\\User', 1, 'myapptoken', 'aa588e6c95e32396201d950e3ac30c04c25915c035e8c3a44e2c0790b48c6896', '[\"*\"]', NULL, '2022-07-21 07:42:34', '2022-07-21 07:42:34'),
(486, 'App\\Models\\User', 1, 'myapptoken', 'c68d79ec116d1c0cfe427ff3ead198b94e755181b63bcc92f19d495e75d3b7e4', '[\"*\"]', NULL, '2022-07-21 07:42:47', '2022-07-21 07:42:47'),
(487, 'App\\Models\\User', 2, 'myapptoken', '55609ee323eacd013053201c9efc2b47a5fdd07e4e584018e05fbcc215626fd2', '[\"*\"]', NULL, '2022-07-21 07:42:56', '2022-07-21 07:42:56'),
(488, 'App\\Models\\User', 2, 'myapptoken', 'a94040c72a6a5e74d847b6b2e64ce96b147a85e60a46455f0841bd52e50d2cad', '[\"*\"]', NULL, '2022-07-21 07:59:49', '2022-07-21 07:59:49'),
(489, 'App\\Models\\User', 1, 'myapptoken', 'ca35a1a0778ca748a02432e52a9be9838b06cf10a5b9b6ffe85b142940950c5a', '[\"*\"]', NULL, '2022-07-21 08:01:53', '2022-07-21 08:01:53'),
(490, 'App\\Models\\User', 2, 'myapptoken', 'ba77b64f2e55c1f22955e52bc4f8648e681d3ce21a354666ca4b146183346721', '[\"*\"]', NULL, '2022-07-21 08:02:11', '2022-07-21 08:02:11'),
(491, 'App\\Models\\User', 1, 'myapptoken', 'f44042948901645a8cf2d8c6ff7deeb769961923825ef6e4e602b6ee6ff7dc85', '[\"*\"]', NULL, '2022-07-21 08:02:57', '2022-07-21 08:02:57'),
(492, 'App\\Models\\User', 2, 'myapptoken', 'fc3c4b138d22cc08fc37c258e95bea17e72b866a97a4ad32cec44cd8b37d26d2', '[\"*\"]', NULL, '2022-07-21 08:03:50', '2022-07-21 08:03:50'),
(493, 'App\\Models\\User', 1, 'myapptoken', '62527d430b62abfd785b3cfbbf0245771ba054604f282cae2aa2442a6e00d19e', '[\"*\"]', NULL, '2022-07-21 08:04:15', '2022-07-21 08:04:15'),
(494, 'App\\Models\\User', 1, 'myapptoken', '9e4265d087855a32224ee38894b2c6bc99ddd59dd62ec96410c5a0e427ec7312', '[\"*\"]', NULL, '2022-07-21 08:04:28', '2022-07-21 08:04:28'),
(495, 'App\\Models\\User', 1, 'myapptoken', 'd75f896c006dfc0dbaf717aba552e29e16bf2e87fcf5537f73c7acc7d729f8e3', '[\"*\"]', NULL, '2022-07-21 08:08:01', '2022-07-21 08:08:01'),
(496, 'App\\Models\\User', 1, 'myapptoken', '48f5ddb0250332ab5c93074775887ea6bdd23b15674fca0652bac1eeba9e039d', '[\"*\"]', NULL, '2022-07-21 08:11:25', '2022-07-21 08:11:25'),
(497, 'App\\Models\\User', 1, 'myapptoken', 'b0223ee5f5f1eaef80da878fe96274f47deb0fa01f2ae9299bae78c4cec291eb', '[\"*\"]', NULL, '2022-07-21 08:11:56', '2022-07-21 08:11:56'),
(498, 'App\\Models\\User', 2, 'myapptoken', 'ea10cd5cbf267c6bdbe6dc2227e04c893385531670cab95df31a2304d4e2861b', '[\"*\"]', NULL, '2022-07-21 08:12:10', '2022-07-21 08:12:10'),
(499, 'App\\Models\\User', 1, 'myapptoken', 'e9e640dc0cfe38f05a2230fddcc90537432ad4126172ddc86ea5d13e3b17a545', '[\"*\"]', NULL, '2022-07-21 08:12:22', '2022-07-21 08:12:22'),
(500, 'App\\Models\\User', 1, 'myapptoken', 'eebdffa58e5a2a448c5dd215e79c6e2c99be9ef3409a6bb2cd10c549fc1a4cd1', '[\"*\"]', NULL, '2022-07-21 21:35:21', '2022-07-21 21:35:21'),
(501, 'App\\Models\\User', 2, 'myapptoken', '495c9ee391211fdf6cd9c27ff0a747a2ace3efe527a84a6e89b2d93b3b57773a', '[\"*\"]', NULL, '2022-07-21 21:35:51', '2022-07-21 21:35:51'),
(502, 'App\\Models\\User', 1, 'myapptoken', 'cc759ce06b511cfa1d0468e4d2691c737085380e79db0dc21ecbf523d80de723', '[\"*\"]', NULL, '2022-07-21 22:14:22', '2022-07-21 22:14:22'),
(503, 'App\\Models\\User', 2, 'myapptoken', 'f4758a4edcc761e2e49fe22454d5b382ffe3cc6acd6fe39c7ac0cb29d63201c9', '[\"*\"]', NULL, '2022-07-21 22:46:14', '2022-07-21 22:46:14'),
(504, 'App\\Models\\User', 1, 'myapptoken', 'b70985d7c2515dafdafa12d056a3283883ace0fd444da8f4533a6624eba083f2', '[\"*\"]', NULL, '2022-07-21 23:33:19', '2022-07-21 23:33:19'),
(505, 'App\\Models\\User', 1, 'myapptoken', '26520c7ab86025688592e74600fb600fb1c718c959ae6faa2dcd7fc78ad19e87', '[\"*\"]', NULL, '2022-07-21 23:33:31', '2022-07-21 23:33:31'),
(506, 'App\\Models\\User', 2, 'myapptoken', '1e3946ecb75dc14935e8730ebd086f40c582a9a4c899e9e88509a7bbc440c82a', '[\"*\"]', NULL, '2022-07-21 23:34:42', '2022-07-21 23:34:42'),
(507, 'App\\Models\\User', 2, 'myapptoken', 'b61f8cd5181cadeb8ebfeac12a37602e6a3505ee877d6b24650817f5ccc65af1', '[\"*\"]', NULL, '2022-07-21 23:38:06', '2022-07-21 23:38:06'),
(508, 'App\\Models\\User', 1, 'myapptoken', '38179341feee6efa69f0cba516dffac291f894dc333ee129e1e556a4d6df854a', '[\"*\"]', NULL, '2022-07-21 23:42:19', '2022-07-21 23:42:19'),
(509, 'App\\Models\\User', 2, 'myapptoken', '6fe762576e3ba3f484d53d60375927e5b08a0d481cf43575c87d3d1038f005fb', '[\"*\"]', NULL, '2022-07-22 00:02:09', '2022-07-22 00:02:09'),
(510, 'App\\Models\\User', 2, 'myapptoken', '597f0c0411cf4d6dba69c331a8c7ff0e822ac76864df650a879f39adc5e86851', '[\"*\"]', NULL, '2022-07-22 01:17:51', '2022-07-22 01:17:51'),
(511, 'App\\Models\\User', 1, 'myapptoken', 'b1875ea3bcdcddfebbe31199f8dd949333ddf2abd5268b08f3f5bda86eea9c46', '[\"*\"]', NULL, '2022-07-22 01:23:22', '2022-07-22 01:23:22'),
(512, 'App\\Models\\User', 2, 'myapptoken', 'a8213c206addea6d91cdadcd7ac048b92047b36d817c4cec80e5dbf29ac7e555', '[\"*\"]', NULL, '2022-07-22 01:24:48', '2022-07-22 01:24:48'),
(513, 'App\\Models\\User', 1, 'myapptoken', '507d2ed6ad0adcae97e1be76cdf363399d69e06302f2e69feb8673ee982dcc73', '[\"*\"]', NULL, '2022-07-22 01:25:21', '2022-07-22 01:25:21'),
(514, 'App\\Models\\User', 1, 'myapptoken', '9dce9be2a5774dc2b1bd2afa65bd2fc046b40e077dea2465bda377bdcde1668f', '[\"*\"]', NULL, '2022-07-22 01:25:28', '2022-07-22 01:25:28'),
(515, 'App\\Models\\User', 2, 'myapptoken', '917b36c0e01b0808d383fb566a8fddef33226fc49daa8b19e189a758bb843168', '[\"*\"]', NULL, '2022-07-22 01:29:58', '2022-07-22 01:29:58'),
(516, 'App\\Models\\User', 1, 'myapptoken', '74d0d78121b3ac19b21e790e4380785b61a273cac722d74e54108e2405c6ce29', '[\"*\"]', NULL, '2022-07-22 01:47:09', '2022-07-22 01:47:09'),
(517, 'App\\Models\\User', 2, 'myapptoken', '7f7c0f8779ab8b236c5695c5d174063a552fd4ec474503b7c5345ada68b75f06', '[\"*\"]', NULL, '2022-07-22 01:58:35', '2022-07-22 01:58:35'),
(518, 'App\\Models\\User', 4, 'myapptoken', '96ae72f1962a6f4cb13520c0fccbc3361b5a413db5d71f63a4eda58d99002dc5', '[\"*\"]', NULL, '2022-07-22 02:25:54', '2022-07-22 02:25:54'),
(519, 'App\\Models\\User', 3, 'myapptoken', 'ad4e314e550eda09c21188dc63fe6f6a555b0331433f477375deb330fb320236', '[\"*\"]', NULL, '2022-07-22 02:26:07', '2022-07-22 02:26:07'),
(520, 'App\\Models\\User', 3, 'myapptoken', '119f3d3fec9c479a3c095eb0e3e6cabe70b6a7d453bc5a6562b80bdf026299c2', '[\"*\"]', NULL, '2022-07-22 02:29:07', '2022-07-22 02:29:07'),
(521, 'App\\Models\\User', 2, 'myapptoken', 'cf814e0ff4e69cba3a1e8ddbe8fb78858b29467c00e5730aebfa1c2332ec2342', '[\"*\"]', NULL, '2022-07-22 02:30:53', '2022-07-22 02:30:53'),
(522, 'App\\Models\\User', 2, 'myapptoken', '2115ae43fe397c5070843bdf607986ef2ca45d9dd8e56f2236f7b7515130e540', '[\"*\"]', NULL, '2022-07-22 02:34:10', '2022-07-22 02:34:10'),
(523, 'App\\Models\\User', 3, 'myapptoken', 'c2c1a9bfeff823a0d6eb30315539cffdd694f6e89d2325d4a98d3409ca25addf', '[\"*\"]', NULL, '2022-07-22 02:37:26', '2022-07-22 02:37:26'),
(524, 'App\\Models\\User', 3, 'myapptoken', 'bf50761c550711bcccc803da1617b2a7c816c5b9b7706e17580062f5ff700ace', '[\"*\"]', NULL, '2022-07-22 02:42:12', '2022-07-22 02:42:12'),
(525, 'App\\Models\\User', 3, 'myapptoken', '192692a33a5583c22194a0ce9642b2634f0b6ab6c0bba884f59284282f0ad8a2', '[\"*\"]', NULL, '2022-07-22 02:43:57', '2022-07-22 02:43:57'),
(526, 'App\\Models\\User', 1, 'myapptoken', 'f4af309dae41e741cdd8f2eb0fbb3b2d0fd250b52a4f3c138546f0e907d83559', '[\"*\"]', NULL, '2022-07-22 02:44:21', '2022-07-22 02:44:21'),
(527, 'App\\Models\\User', 3, 'myapptoken', 'd388089082ab169586091131d662d061452c6376af486ba856cff822fbb11bc6', '[\"*\"]', NULL, '2022-07-22 02:45:21', '2022-07-22 02:45:21'),
(528, 'App\\Models\\User', 1, 'myapptoken', 'c421953b9f81fb1684881b43a1ced8995b3efd3bf4220a25cf33a8e99974034f', '[\"*\"]', NULL, '2022-07-22 02:46:47', '2022-07-22 02:46:47'),
(529, 'App\\Models\\User', 1, 'myapptoken', '0559c021704ef3a1bca0e2e6cb9ec174080843cdeaf13daf823888cbe65aacbf', '[\"*\"]', NULL, '2022-07-22 05:51:17', '2022-07-22 05:51:17'),
(530, 'App\\Models\\User', 3, 'myapptoken', '237d3e40626e160299dd6dc85a87ec1bfcb55939a588f9d345b7a7492ed69059', '[\"*\"]', NULL, '2022-07-22 06:06:23', '2022-07-22 06:06:23'),
(531, 'App\\Models\\User', 1, 'myapptoken', '8c0f8544ed19c6fde5eccf61d025328c29e8c53c96aa4362d10e2b58e37c35b6', '[\"*\"]', NULL, '2022-07-22 06:06:45', '2022-07-22 06:06:45'),
(532, 'App\\Models\\User', 3, 'myapptoken', '2c84aaaeada63bb8f0048e24d94f02aa5250856f0b5306e73d43dd1fcb8e3afa', '[\"*\"]', NULL, '2022-07-22 06:10:57', '2022-07-22 06:10:57'),
(533, 'App\\Models\\User', 1, 'myapptoken', '9e46f6d4a41bd351b97d6a3d7992434f0b6dbe84e552fce52901d3334f96d77b', '[\"*\"]', NULL, '2022-07-22 06:11:10', '2022-07-22 06:11:10'),
(534, 'App\\Models\\User', 1, 'myapptoken', '103d299b5c0161ef7efc9e6db12eced00d4f83dbe1aaa20c42d0b9f8b7de1ccf', '[\"*\"]', NULL, '2022-07-22 06:15:35', '2022-07-22 06:15:35'),
(535, 'App\\Models\\User', 3, 'myapptoken', '09af007ae46d30f8723c68aeeb88396d1a701d825771b21afff8f56c73dc5251', '[\"*\"]', NULL, '2022-07-22 06:16:45', '2022-07-22 06:16:45'),
(536, 'App\\Models\\User', 1, 'myapptoken', 'd023aef55140c91e9734bdaa9743ebf6f25ab4a955d16a3ebf4042db657edbd8', '[\"*\"]', NULL, '2022-07-22 06:16:57', '2022-07-22 06:16:57'),
(537, 'App\\Models\\User', 3, 'myapptoken', '298c0ade0562ab8ac8fd0901c8d37ba8d23cb6b663709ef8391b5cc64f546eed', '[\"*\"]', NULL, '2022-07-22 06:17:35', '2022-07-22 06:17:35'),
(538, 'App\\Models\\User', 1, 'myapptoken', 'de5e0aac8fa10e1e8b7ed43d508df98591708aa728769b4005d50cef2b42cc3b', '[\"*\"]', NULL, '2022-07-22 07:07:32', '2022-07-22 07:07:32'),
(539, 'App\\Models\\User', 1, 'myapptoken', '4ca35277138abfebb384a0916b37f01c65b8d9d145ad446f695733d74945278f', '[\"*\"]', NULL, '2022-07-22 07:15:30', '2022-07-22 07:15:30'),
(540, 'App\\Models\\User', 1, 'myapptoken', '7e93cf0691717cfd660b90ae6aaf214f46899e9add744f9a4250ab37218988fa', '[\"*\"]', NULL, '2022-07-22 07:15:40', '2022-07-22 07:15:40'),
(541, 'App\\Models\\User', 1, 'myapptoken', 'ac46d2039cf5319545e13d605054ab4a438bce21655c53b9602b8301bcdfa03d', '[\"*\"]', NULL, '2022-07-22 07:18:45', '2022-07-22 07:18:45'),
(542, 'App\\Models\\User', 1, 'myapptoken', 'bb8b40aae0dd206e7460df703f0be389b894fec98b4512b064d6faf587854036', '[\"*\"]', NULL, '2022-07-22 07:19:48', '2022-07-22 07:19:48'),
(543, 'App\\Models\\User', 1, 'myapptoken', 'dc7d7fe539a976674f0b04f574cf5c0fcdf08052efb4e61b6b12ac606ac6b5fe', '[\"*\"]', NULL, '2022-07-22 07:20:13', '2022-07-22 07:20:13'),
(544, 'App\\Models\\User', 1, 'myapptoken', '8803be2e55b05c56dc794aa60511fa689628429f2c5d6bde824fe97258116c91', '[\"*\"]', NULL, '2022-07-22 07:20:36', '2022-07-22 07:20:36'),
(545, 'App\\Models\\User', 1, 'myapptoken', '103f48a89ac4ea5786f1e9a915e2cb6d7e151c35bc6c91d1cdaac2b9e611b4ec', '[\"*\"]', NULL, '2022-07-22 07:22:11', '2022-07-22 07:22:11'),
(546, 'App\\Models\\User', 4, 'myapptoken', '8aaa72247ebe64d376f92162047b309612de1420b2cff5ac4d7a4aca20c6b97d', '[\"*\"]', NULL, '2022-07-22 07:26:22', '2022-07-22 07:26:22'),
(547, 'App\\Models\\User', 1, 'myapptoken', 'c35566b8883bfa7caa64ecb1a933d256882dfd25e1be42630a9033fcc2660e12', '[\"*\"]', NULL, '2022-07-22 07:28:53', '2022-07-22 07:28:53'),
(548, 'App\\Models\\User', 4, 'myapptoken', 'a9a5a25066277f105ec60919352f5a97b445067214b0ff1c12bacd5c25d3c8b8', '[\"*\"]', NULL, '2022-07-22 07:29:01', '2022-07-22 07:29:01'),
(549, 'App\\Models\\User', 4, 'myapptoken', 'af84915510521244564e59e0505eb0a7e965e8ddf91e1d299be18512f0944250', '[\"*\"]', NULL, '2022-07-22 07:29:17', '2022-07-22 07:29:17'),
(550, 'App\\Models\\User', 4, 'myapptoken', '522a7a92449a626d91e3071935fa285f1312e11fba69fc88bb337db10d8caa4e', '[\"*\"]', NULL, '2022-07-22 07:35:30', '2022-07-22 07:35:30'),
(551, 'App\\Models\\User', 1, 'myapptoken', '22eb92b95c7db95c7b5a67b96ff7deff59cc310749a567928b84685906380a27', '[\"*\"]', NULL, '2022-07-22 07:36:17', '2022-07-22 07:36:17'),
(552, 'App\\Models\\User', 1, 'myapptoken', '2a82b96907ca508367f358583bcffe41e7cb77fad563ce7c0787a3125de915f5', '[\"*\"]', NULL, '2022-07-22 07:37:09', '2022-07-22 07:37:09'),
(553, 'App\\Models\\User', 7, 'myapptoken', '5302435923152279451a47d72fb8c8ac67b8282cf135e2c26c9b3c6aad1b0ef4', '[\"*\"]', NULL, '2022-07-22 07:37:16', '2022-07-22 07:37:16'),
(554, 'App\\Models\\User', 3, 'myapptoken', '364ba564c8326ecff88bcbd5a52bbde6280a72558db7c6c095495518053355b7', '[\"*\"]', NULL, '2022-07-22 07:39:08', '2022-07-22 07:39:08'),
(555, 'App\\Models\\User', 7, 'myapptoken', 'fc88bc516d9a723b8125ddf0bd14fe7cdf0f85aeb5553d621c442d0b778f371a', '[\"*\"]', NULL, '2022-07-22 07:39:59', '2022-07-22 07:39:59'),
(556, 'App\\Models\\User', 4, 'myapptoken', 'f9173b7e5d5cccf552d3e459d3877c411451aa1f429f2373e725b723b316417e', '[\"*\"]', NULL, '2022-07-22 07:43:39', '2022-07-22 07:43:39'),
(557, 'App\\Models\\User', 3, 'myapptoken', '228874944357d080c30b008a8fc8709fa39accf99e00cce8c9efca0727ba132a', '[\"*\"]', NULL, '2022-07-22 08:36:29', '2022-07-22 08:36:29'),
(558, 'App\\Models\\User', 7, 'myapptoken', 'b426058feb701b2869780a9990915c225e22faa51c08be548e0646f73a401009', '[\"*\"]', NULL, '2022-07-22 08:43:15', '2022-07-22 08:43:15'),
(559, 'App\\Models\\User', 3, 'myapptoken', '1bbd67d1cf57325b84d14c7aaa7291e67eabbe6316ec6895012e32f9a0f2b21a', '[\"*\"]', NULL, '2022-07-22 08:43:47', '2022-07-22 08:43:47'),
(560, 'App\\Models\\User', 3, 'myapptoken', '1de4f9862fb099961d918d42f96d21e2af9bd92e29de0f368a5462d84713a801', '[\"*\"]', NULL, '2022-07-22 08:44:03', '2022-07-22 08:44:03'),
(561, 'App\\Models\\User', 2, 'myapptoken', '14e9151f5f882219420d05997ca7a6f148d4d5c321b2addff9ade460293c8432', '[\"*\"]', NULL, '2022-07-22 08:44:31', '2022-07-22 08:44:31'),
(562, 'App\\Models\\User', 3, 'myapptoken', '3b5557a32f78fbf249767e456beb965cb2bacd1c2037b66a32f045a9ebf205ec', '[\"*\"]', NULL, '2022-07-22 08:44:39', '2022-07-22 08:44:39'),
(563, 'App\\Models\\User', 7, 'myapptoken', 'ff5a80631fa7b90986dc196a92b8f7c7efd8e2f74a2598aeaddced6ea79c3547', '[\"*\"]', NULL, '2022-07-22 08:54:25', '2022-07-22 08:54:25'),
(564, 'App\\Models\\User', 7, 'myapptoken', 'f821fe544f55387de77ce7824d99d5a73835ea357a8a9f2a695f1ce9662a6b5c', '[\"*\"]', NULL, '2022-07-22 08:54:45', '2022-07-22 08:54:45'),
(565, 'App\\Models\\User', 4, 'myapptoken', 'e99a01e01dcacd46896a139ffda9b6b78ab6a270ae917b53e858dc5c6392369c', '[\"*\"]', NULL, '2022-07-22 08:54:54', '2022-07-22 08:54:54'),
(566, 'App\\Models\\User', 3, 'myapptoken', '38392ec4e180b4144a31a51524b382c9dc7bac9b075606d2133a54d1e962ab67', '[\"*\"]', NULL, '2022-07-22 08:55:02', '2022-07-22 08:55:02'),
(567, 'App\\Models\\User', 4, 'myapptoken', '1eea641c1bb2a196405a2d14205695039a669c5e3b68c9ff2c8833025cbc4df6', '[\"*\"]', NULL, '2022-07-22 08:56:41', '2022-07-22 08:56:41'),
(568, 'App\\Models\\User', 4, 'myapptoken', '8a5cba64e5cc5771a4d58146837395c23ab173df7bb18f418802dd54ec3107e4', '[\"*\"]', NULL, '2022-07-22 08:58:47', '2022-07-22 08:58:47'),
(569, 'App\\Models\\User', 4, 'myapptoken', '10c1d573e91a143d27470c10ed24e5ee18f765ece6f6c1f1945d553507c0bc16', '[\"*\"]', NULL, '2022-07-22 08:59:01', '2022-07-22 08:59:01'),
(570, 'App\\Models\\User', 4, 'myapptoken', 'd26ee6754d46d9a5e2d73542c15d6a0a9d1edc9d6d6c1cb640f14b50f0b28e14', '[\"*\"]', NULL, '2022-07-22 08:59:10', '2022-07-22 08:59:10'),
(571, 'App\\Models\\User', 4, 'myapptoken', '2dfe62ceb68efd040e81211302257a212a772b6fed4f23d8b8436f56ab47c86b', '[\"*\"]', NULL, '2022-07-22 09:00:47', '2022-07-22 09:00:47'),
(572, 'App\\Models\\User', 3, 'myapptoken', '30e74014046d08cf35451dfd88a1cf72a2cc0e0b15af2cd34bce9c2052ce8e62', '[\"*\"]', NULL, '2022-07-22 09:10:00', '2022-07-22 09:10:00'),
(573, 'App\\Models\\User', 4, 'myapptoken', '211920a9e0b18675205bbbce92d432c21b7adec6f08502098c5f41ada8520e4d', '[\"*\"]', NULL, '2022-07-22 09:21:49', '2022-07-22 09:21:49'),
(574, 'App\\Models\\User', 3, 'myapptoken', 'ec3944a0d5b48637d745b3e7977469e417d2fd731db4d005636963c677f81e82', '[\"*\"]', NULL, '2022-07-22 09:23:13', '2022-07-22 09:23:13'),
(575, 'App\\Models\\User', 4, 'myapptoken', 'c9ea4b4d1888d0b7595051417803236015ac91b5d35aa3dce96b2f253c7aa1e2', '[\"*\"]', NULL, '2022-07-22 09:23:21', '2022-07-22 09:23:21'),
(576, 'App\\Models\\User', 3, 'myapptoken', 'bba0f9142ae26337f07e66289b47f04286e3dcc94d751092d264ba66ff21e3c4', '[\"*\"]', NULL, '2022-07-22 09:24:16', '2022-07-22 09:24:16'),
(577, 'App\\Models\\User', 3, 'myapptoken', '4642ccec0741530bc5fd2d8481b82617bfc14d7b83514162e524154c0dab22c1', '[\"*\"]', NULL, '2022-07-22 09:24:28', '2022-07-22 09:24:28'),
(578, 'App\\Models\\User', 7, 'myapptoken', '7e66691a8eecc714a3865b5068db8504b2fcbba6a2e27d3d76fc612ef5ba357b', '[\"*\"]', NULL, '2022-07-22 09:25:08', '2022-07-22 09:25:08'),
(579, 'App\\Models\\User', 3, 'myapptoken', '2e10aceed5fcebbe0ac97f8997d8a3a62b7db8c6d4a29cfd6501ef30b4544f98', '[\"*\"]', NULL, '2022-07-22 09:26:33', '2022-07-22 09:26:33'),
(580, 'App\\Models\\User', 5, 'myapptoken', '63c36e81b9c6ea8ac2def074a6213338abb824cb3686b11096ab29fa971770ec', '[\"*\"]', NULL, '2022-07-22 09:26:52', '2022-07-22 09:26:52'),
(581, 'App\\Models\\User', 3, 'myapptoken', '470ba26b326c942e71f24abf5ffa4630c7d615dad23e5306351bf541ace7438b', '[\"*\"]', NULL, '2022-07-22 09:28:57', '2022-07-22 09:28:57'),
(582, 'App\\Models\\User', 7, 'myapptoken', 'bacd12bc7b2cd978ebfca098e6dc1b3bebab639a4a8a35f0e411c81c2b235e99', '[\"*\"]', NULL, '2022-07-22 09:29:44', '2022-07-22 09:29:44'),
(583, 'App\\Models\\User', 7, 'myapptoken', '5ea085812f30b3c7c2d1159862a89ce43a65509c16d95fa256af27c679f65c8f', '[\"*\"]', NULL, '2022-07-22 09:33:29', '2022-07-22 09:33:29'),
(584, 'App\\Models\\User', 7, 'myapptoken', 'ed25dce3ddbd572e5692ff4875a6ebb1edacace4da1f6c7d8c348c3b85459af3', '[\"*\"]', NULL, '2022-07-22 09:40:35', '2022-07-22 09:40:35'),
(585, 'App\\Models\\User', 4, 'myapptoken', '20377e7a3071c9454a11a591e947463222f468ee033d88f898981c14bb23c5ab', '[\"*\"]', NULL, '2022-07-22 09:41:59', '2022-07-22 09:41:59'),
(586, 'App\\Models\\User', 3, 'myapptoken', '83a651f482f8d5cc237dab5622282aa60c0e4ac1ed498df36efa8ad66650d13d', '[\"*\"]', NULL, '2022-07-22 09:42:07', '2022-07-22 09:42:07'),
(587, 'App\\Models\\User', 4, 'myapptoken', 'fced1a3f4c3bb990dab35efb9514faa9e6f1f16842682aaf1def10024855fa62', '[\"*\"]', NULL, '2022-07-22 10:02:14', '2022-07-22 10:02:14'),
(588, 'App\\Models\\User', 4, 'myapptoken', '63e4b594921abeecc2eac55c56b8927de9336263c765b29edf579f5f279ee4a0', '[\"*\"]', NULL, '2022-07-22 10:14:38', '2022-07-22 10:14:38'),
(589, 'App\\Models\\User', 3, 'myapptoken', 'af454db6be4437ea71fe99d619fe4c97cc28f2e6c35a8c69f1de73abf6d721f0', '[\"*\"]', NULL, '2022-07-22 10:14:49', '2022-07-22 10:14:49'),
(590, 'App\\Models\\User', 7, 'myapptoken', 'd34dd1aacde1480deae84b3d9774b3b0469ff903f50d54d25959c6ce9b354a84', '[\"*\"]', NULL, '2022-07-22 10:16:55', '2022-07-22 10:16:55'),
(591, 'App\\Models\\User', 3, 'myapptoken', 'b7a6b476337b030d905f5a5f841687a75890275fcf2208786f83f90065e03fe4', '[\"*\"]', NULL, '2022-07-22 10:18:03', '2022-07-22 10:18:03'),
(592, 'App\\Models\\User', 4, 'myapptoken', '8cd8c393790f0ec061aff6e5d593503ff9a5a237c9c09988b21d1bc913868b50', '[\"*\"]', NULL, '2022-07-22 10:41:38', '2022-07-22 10:41:38'),
(593, 'App\\Models\\User', 4, 'myapptoken', '00d4c1214912318f4d56a60f513f7d62362b3f5c79de392d07fcab50b2957a58', '[\"*\"]', NULL, '2022-07-22 10:58:29', '2022-07-22 10:58:29'),
(594, 'App\\Models\\User', 3, 'myapptoken', '9f3a9fc8accf733ca58281caf06af84b944782626e968ad139657b9e2f443996', '[\"*\"]', NULL, '2022-07-22 11:01:50', '2022-07-22 11:01:50'),
(595, 'App\\Models\\User', 7, 'myapptoken', '336cddfc1f9a89d68c24003cca1282035efbda4e083db666c9ed3f752d141b7f', '[\"*\"]', NULL, '2022-07-22 11:02:28', '2022-07-22 11:02:28'),
(596, 'App\\Models\\User', 4, 'myapptoken', '621e4da948969b866ecee6367eee11ee86f75f55c171b61ef028a36d0a1aa7ee', '[\"*\"]', NULL, '2022-07-22 18:42:46', '2022-07-22 18:42:46'),
(597, 'App\\Models\\User', 3, 'myapptoken', 'decf849d6939c3f6927c219fcdd80e2b090017675b53b29cc54ec34c66f7e731', '[\"*\"]', NULL, '2022-07-22 18:43:00', '2022-07-22 18:43:00'),
(598, 'App\\Models\\User', 5, 'myapptoken', '22e65b365fb77a9fc1e26f8ac7b9f35900bdb16a8df64584704c6e28e6d607d2', '[\"*\"]', NULL, '2022-07-22 20:02:58', '2022-07-22 20:02:58'),
(599, 'App\\Models\\User', 4, 'myapptoken', 'c9672077082415c8c3e4d1344c9f78155510b1241d36ab39f8d500f7addb5597', '[\"*\"]', NULL, '2022-07-22 20:05:25', '2022-07-22 20:05:25'),
(600, 'App\\Models\\User', 3, 'myapptoken', '843ef43b2dcf952d7dd74d1e95956f1230d9ec67717e4aff2e69c65343d89d3a', '[\"*\"]', NULL, '2022-07-22 20:06:14', '2022-07-22 20:06:14'),
(601, 'App\\Models\\User', 7, 'myapptoken', '01d1c33459c321ea9b96830b8856701396b18290f9ae0e39c3a4caad724998ac', '[\"*\"]', NULL, '2022-07-22 20:37:30', '2022-07-22 20:37:30');
INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(602, 'App\\Models\\User', 4, 'myapptoken', '5563153f473adbdbfd6afd634ed88c42c2ebd7ebf64412c8f2d840e64dc936bc', '[\"*\"]', NULL, '2022-07-22 20:43:41', '2022-07-22 20:43:41'),
(603, 'App\\Models\\User', 3, 'myapptoken', '0b5adf5302c82a3bfec74a41644132a05ee50a912b3c1e99debb41057b55e321', '[\"*\"]', NULL, '2022-07-22 20:43:48', '2022-07-22 20:43:48'),
(604, 'App\\Models\\User', 5, 'myapptoken', '9781250d2e627c19056af4c797882d5f7a8cc7f974ef794f47068ce77d3c0b0f', '[\"*\"]', NULL, '2022-07-22 20:53:20', '2022-07-22 20:53:20'),
(605, 'App\\Models\\User', 7, 'myapptoken', '70ac1b44b7d0b1773aa5eec579c8252264163bbfa3638f51b34ce9bfcf1a96cb', '[\"*\"]', NULL, '2022-07-22 20:53:38', '2022-07-22 20:53:38'),
(606, 'App\\Models\\User', 5, 'myapptoken', '92c03b7d845d494033e214f7d18691f515b112826289c242e49861f2127df36b', '[\"*\"]', NULL, '2022-07-22 20:54:31', '2022-07-22 20:54:31'),
(607, 'App\\Models\\User', 7, 'myapptoken', '34a5e60f1e13a9b1cad87038c7c9d6454475fc4a9543f3c1f0ed1f8b828f5f26', '[\"*\"]', NULL, '2022-07-22 20:54:46', '2022-07-22 20:54:46'),
(608, 'App\\Models\\User', 5, 'myapptoken', 'a7a97509688f1c8e2c299122f9ccc28047d076de1922ba248de98cbc25666635', '[\"*\"]', NULL, '2022-07-22 20:55:05', '2022-07-22 20:55:05'),
(609, 'App\\Models\\User', 7, 'myapptoken', '76d77e35996b7b7c377abb704c5e73e2ba13bf832972f16975d8ac86f5e780d5', '[\"*\"]', NULL, '2022-07-22 20:55:31', '2022-07-22 20:55:31'),
(610, 'App\\Models\\User', 4, 'myapptoken', '881d6df1953bdabea899f223fe74f20c9439876584207ebb77eb11298d5c8afa', '[\"*\"]', NULL, '2022-07-22 20:56:32', '2022-07-22 20:56:32'),
(611, 'App\\Models\\User', 3, 'myapptoken', '327ebb972f4ceb8f1a4afbf884d7b52da17e0a5785ff456bf6368d224facccd5', '[\"*\"]', NULL, '2022-07-22 20:56:43', '2022-07-22 20:56:43'),
(612, 'App\\Models\\User', 4, 'myapptoken', 'f98193f14cf01ca6f97db4c5d5c97fae0e84d21ebe3497094bd9566bc5b4d3dc', '[\"*\"]', NULL, '2022-07-22 20:56:59', '2022-07-22 20:56:59'),
(613, 'App\\Models\\User', 3, 'myapptoken', '17fa579b9350b7bea80902b2b010e2df761720a788798bbaf6bde7e2e30a0cdd', '[\"*\"]', NULL, '2022-07-22 21:03:30', '2022-07-22 21:03:30'),
(614, 'App\\Models\\User', 3, 'myapptoken', '1449e80a416b9ee1e2196e14bddbe1996d3ba482d5a395cd4f4de883d18e631f', '[\"*\"]', NULL, '2022-07-22 21:04:04', '2022-07-22 21:04:04'),
(615, 'App\\Models\\User', 3, 'myapptoken', '41aaea6c761ac5d4d2629a39b163a1beb34500f90af94877e2191fb86d5eb527', '[\"*\"]', NULL, '2022-07-22 21:10:41', '2022-07-22 21:10:41'),
(616, 'App\\Models\\User', 4, 'myapptoken', 'fa1fb75051c03aaa1e918ec7d038554e005720948b184e083ee3d82cc577899e', '[\"*\"]', NULL, '2022-07-22 21:10:48', '2022-07-22 21:10:48'),
(617, 'App\\Models\\User', 3, 'myapptoken', 'e4f80893d002c8af9391931278b05a2a8d6450864ae09abf228d6c0a467fd12f', '[\"*\"]', NULL, '2022-07-22 21:12:13', '2022-07-22 21:12:13'),
(618, 'App\\Models\\User', 3, 'myapptoken', '012822aea3247dc69595a54f4910c16018dc15eb84e845c4b1004e503bb05e6c', '[\"*\"]', NULL, '2022-07-22 21:12:48', '2022-07-22 21:12:48'),
(619, 'App\\Models\\User', 4, 'myapptoken', '9b423c454fa4a005362e581d12c0d60cb91b83cb2a45651641ede02395826e06', '[\"*\"]', NULL, '2022-07-22 21:13:12', '2022-07-22 21:13:12'),
(620, 'App\\Models\\User', 7, 'myapptoken', 'f96f797b57f94f541377d2c44a61deaa7d6362c0448e0c2391a4ca18d26018eb', '[\"*\"]', NULL, '2022-07-22 21:13:28', '2022-07-22 21:13:28'),
(621, 'App\\Models\\User', 5, 'myapptoken', '229723d2729659c9ebd121646fef5acd63ebdf64fbd4492560b4900bef6c85a9', '[\"*\"]', NULL, '2022-07-22 21:14:59', '2022-07-22 21:14:59'),
(622, 'App\\Models\\User', 6, 'myapptoken', '5f90501da54e6e4496c4521ab5b83822f7adc4e1437b64f8faa98ebec8da96cf', '[\"*\"]', NULL, '2022-07-22 21:15:07', '2022-07-22 21:15:07'),
(623, 'App\\Models\\User', 6, 'myapptoken', 'dc7b705c07983f083e42a03ce7bea53145973a5e19ad626bd3334d49a5652044', '[\"*\"]', NULL, '2022-07-22 23:29:37', '2022-07-22 23:29:37'),
(624, 'App\\Models\\User', 3, 'myapptoken', '247a4654b772b46150d8aa926d4f096bc439bebc0231309363414d3c6778f4e8', '[\"*\"]', NULL, '2022-07-22 23:29:53', '2022-07-22 23:29:53'),
(625, 'App\\Models\\User', 4, 'myapptoken', 'f46330c422cfad57532e863e35972b0e06e614a31847abfe5620b96472b20e73', '[\"*\"]', NULL, '2022-07-22 23:30:08', '2022-07-22 23:30:08'),
(626, 'App\\Models\\User', 3, 'myapptoken', 'defe2a811d08e6419d00c09f8d30cb00d92176ebef75868cd36754c13d2c867f', '[\"*\"]', NULL, '2022-07-22 23:32:57', '2022-07-22 23:32:57'),
(627, 'App\\Models\\User', 7, 'myapptoken', 'ea3092602cc9ebf2809e8ac7bde95fd20903cf63e6faaa3f99941d4817f94a7d', '[\"*\"]', NULL, '2022-07-22 23:35:21', '2022-07-22 23:35:21'),
(628, 'App\\Models\\User', 7, 'myapptoken', '937a191d7b126e45f8ed7b563f7fa50cca4bc4baa50db2a877b25f22fd44a451', '[\"*\"]', NULL, '2022-07-22 23:35:46', '2022-07-22 23:35:46'),
(629, 'App\\Models\\User', 5, 'myapptoken', '4cc9f97666261655d01b7701ef723d7d48608a5009c117a7d1986e5cc9fb770f', '[\"*\"]', NULL, '2022-07-22 23:36:20', '2022-07-22 23:36:20'),
(630, 'App\\Models\\User', 4, 'myapptoken', 'ed00afab862cbf84a25322c86775cfd1dcdad84f9b0899277862648efd4dc04b', '[\"*\"]', NULL, '2022-07-22 23:36:59', '2022-07-22 23:36:59'),
(631, 'App\\Models\\User', 7, 'myapptoken', '80de076bc4bcd076a437ee1fc49252e419e1e025bd16bff095936c7f505e0c60', '[\"*\"]', NULL, '2022-07-22 23:38:18', '2022-07-22 23:38:18'),
(632, 'App\\Models\\User', 3, 'myapptoken', 'df2de0c5f41e962f74ea046d3539911477fe2039c9dea5f2d42738abecf7317b', '[\"*\"]', NULL, '2022-07-22 23:39:22', '2022-07-22 23:39:22'),
(633, 'App\\Models\\User', 4, 'myapptoken', '5c9e8b17060fab7962a559040cedb9daf400eca825b3f5bd029099a8f335e6b6', '[\"*\"]', NULL, '2022-07-22 23:39:31', '2022-07-22 23:39:31'),
(634, 'App\\Models\\User', 4, 'myapptoken', '30323b003ed59ff3e1ea34fcea576bcc864c57bfaf69e6c9e1ea3d18e41fcc31', '[\"*\"]', NULL, '2022-07-22 23:43:35', '2022-07-22 23:43:35'),
(635, 'App\\Models\\User', 3, 'myapptoken', '03b0c537bea780c3ff4e8ddd2f4f590b2c16b1f26ce7feed1556a903afc30bd5', '[\"*\"]', NULL, '2022-07-22 23:43:42', '2022-07-22 23:43:42'),
(636, 'App\\Models\\User', 6, 'myapptoken', 'aa96fb4c42e0f5c66734673576ff10378ab79cbda9539d6cc2a07c3575db6a3d', '[\"*\"]', NULL, '2022-07-22 23:45:06', '2022-07-22 23:45:06'),
(637, 'App\\Models\\User', 5, 'myapptoken', '1b1ae9f5ad2d2fd54bc972e7fc67b119e1d9b6a6267db49aaa1e38e6edd3bece', '[\"*\"]', NULL, '2022-07-22 23:45:15', '2022-07-22 23:45:15'),
(638, 'App\\Models\\User', 6, 'myapptoken', '0f4f7fb8ca787a170e38d8174e9a77409a3a4e1375ff54250bc13a724880ddb0', '[\"*\"]', NULL, '2022-07-22 23:52:48', '2022-07-22 23:52:48'),
(639, 'App\\Models\\User', 3, 'myapptoken', 'ff345ec0ae1e6f82ca579d4ab0d7f8a38548316e54bf46364f98294357926364', '[\"*\"]', NULL, '2022-07-22 23:53:03', '2022-07-22 23:53:03'),
(640, 'App\\Models\\User', 7, 'myapptoken', '999d3ebc41c0e16dc0d4e477ec571aa0c26ee218a2a2dc5f49a4c4acbee03529', '[\"*\"]', NULL, '2022-07-22 23:53:16', '2022-07-22 23:53:16'),
(641, 'App\\Models\\User', 7, 'myapptoken', 'ec06bd9a62ee65c0c5d27c4d46a9dc612ff5a9b5fb11db99f45364887f753686', '[\"*\"]', NULL, '2022-07-22 23:53:24', '2022-07-22 23:53:24'),
(642, 'App\\Models\\User', 5, 'myapptoken', '1b7d4018bc40906437beb19fc249c8fabfa42bc40737a193f953fb473bc923b4', '[\"*\"]', NULL, '2022-07-23 00:22:49', '2022-07-23 00:22:49'),
(643, 'App\\Models\\User', 7, 'myapptoken', 'a89dfb989da6738b652dd909e2e2a2d98fe1d9f04a281104b18b0fbffe9c7b96', '[\"*\"]', NULL, '2022-07-23 00:23:30', '2022-07-23 00:23:30'),
(644, 'App\\Models\\User', 3, 'myapptoken', '96c3a0e8bcb2c47b1eb9d6d29354104d1e33933b3d1e4eb8f8ba9e13cd5fa854', '[\"*\"]', NULL, '2022-07-23 00:23:40', '2022-07-23 00:23:40'),
(645, 'App\\Models\\User', 4, 'myapptoken', '1f1d750030fd1342d4030e414a5ccd0119fb3b05164d51dd33d038d2aaf67645', '[\"*\"]', NULL, '2022-07-23 00:23:49', '2022-07-23 00:23:49'),
(646, 'App\\Models\\User', 7, 'myapptoken', '27eb79001b937377d1c0444e23fde357650787d0f6948b0ede844400315312bb', '[\"*\"]', NULL, '2022-07-23 00:23:55', '2022-07-23 00:23:55'),
(647, 'App\\Models\\User', 4, 'myapptoken', 'c2941b28fbc7c794f2c6ef57c2db6fe7383bb4c9c9792db25e80a4fd0f316234', '[\"*\"]', NULL, '2022-07-23 00:25:47', '2022-07-23 00:25:47'),
(648, 'App\\Models\\User', 3, 'myapptoken', '5f353b69297d9f35b72b0f2e8d77951ec029d96cb0690d68061a325c8deb27ba', '[\"*\"]', NULL, '2022-07-23 00:26:03', '2022-07-23 00:26:03'),
(649, 'App\\Models\\User', 7, 'myapptoken', 'dea2748df4468a3fe25b4d7511d21fe0e71b5d5941b94cd3b1869caaa3ff63cc', '[\"*\"]', NULL, '2022-07-23 00:26:25', '2022-07-23 00:26:25'),
(650, 'App\\Models\\User', 4, 'myapptoken', 'e810a9ab13a45e97a686b9fa174c50125468af8f20d539c62f282afa8d113094', '[\"*\"]', NULL, '2022-07-23 00:49:21', '2022-07-23 00:49:21'),
(651, 'App\\Models\\User', 4, 'myapptoken', 'ec576839354e8b1ed74254b2b3ec1b0e61b0372e6a5ba20abcfdc2786c69a104', '[\"*\"]', NULL, '2022-07-23 00:49:27', '2022-07-23 00:49:27'),
(652, 'App\\Models\\User', 4, 'myapptoken', '7c549c34890c1dd785c9b9e73e5e57549d6b76a2a80109da9dda8557b766cd7c', '[\"*\"]', NULL, '2022-07-23 01:08:52', '2022-07-23 01:08:52'),
(653, 'App\\Models\\User', 7, 'myapptoken', '119ebc2aa2e4500f121a92049a3f6385ceb9248efc04028eff14db90d359c577', '[\"*\"]', NULL, '2022-07-23 01:09:01', '2022-07-23 01:09:01'),
(654, 'App\\Models\\User', 7, 'myapptoken', '648cc01b46b068ad8d995e4199095d1f89bd3e1094f72c4fb16bc0a09652e157', '[\"*\"]', NULL, '2022-07-23 01:23:13', '2022-07-23 01:23:13'),
(655, 'App\\Models\\User', 7, 'myapptoken', 'af108af40ab1f80af5dc621f5de02872341b5b7b5fe997c9daed5fa34efd5c61', '[\"*\"]', NULL, '2022-07-23 01:23:33', '2022-07-23 01:23:33'),
(656, 'App\\Models\\User', 4, 'myapptoken', '7bd055d0fdb31806a8f639631a7cbde2f74a5fc94c8a7e3dcc2ef935323f335e', '[\"*\"]', NULL, '2022-07-23 01:24:00', '2022-07-23 01:24:00'),
(657, 'App\\Models\\User', 3, 'myapptoken', 'f273f417743b1322d2bf2d1e85305c4c55176e15fde1e05f072d8831a07e6620', '[\"*\"]', NULL, '2022-07-23 01:24:38', '2022-07-23 01:24:38'),
(658, 'App\\Models\\User', 7, 'myapptoken', 'ea8a4196c12e21d892aed8f69bf9c17be1ea4a90656fb8e84fe357289f382cf6', '[\"*\"]', NULL, '2022-07-23 01:25:02', '2022-07-23 01:25:02'),
(659, 'App\\Models\\User', 7, 'myapptoken', 'fa201bc446c8c566ae84cbbd24132f355e2ce2c7830dc99330bca9ac81f9ac74', '[\"*\"]', NULL, '2022-07-23 01:30:52', '2022-07-23 01:30:52'),
(660, 'App\\Models\\User', 7, 'myapptoken', '12db5a3421f03cc4aba147ac9433381487f52be03db8b102d71549ec0ca3b546', '[\"*\"]', NULL, '2022-07-23 01:31:14', '2022-07-23 01:31:14'),
(661, 'App\\Models\\User', 4, 'myapptoken', 'ccfcea3125648445331b4e0bdb9d6c7d9b892cef76d363be41ac09c71ff6b2fb', '[\"*\"]', NULL, '2022-07-23 01:31:28', '2022-07-23 01:31:28'),
(662, 'App\\Models\\User', 4, 'myapptoken', '157ad758d3271e40c34fa2c8cd97062db9067203aa5641107eb851e5f76bc1ef', '[\"*\"]', NULL, '2022-07-23 01:31:36', '2022-07-23 01:31:36'),
(663, 'App\\Models\\User', 3, 'myapptoken', '7f2f612b03cdebf0b316433e3d7f1980a9cba5aa603c36122ca483343715508f', '[\"*\"]', NULL, '2022-07-23 01:33:06', '2022-07-23 01:33:06'),
(664, 'App\\Models\\User', 7, 'myapptoken', '9190b3c34b418a29f7ecc1cdfcd1693ab76f9f36f5f858fe54bbdfeed5609792', '[\"*\"]', NULL, '2022-07-23 01:33:32', '2022-07-23 01:33:32'),
(665, 'App\\Models\\User', 7, 'myapptoken', 'cce7470d84329630e47e08db360a92b27fde5c6e0efa3fabb41a61c73e4f638c', '[\"*\"]', NULL, '2022-07-23 01:33:59', '2022-07-23 01:33:59'),
(666, 'App\\Models\\User', 6, 'myapptoken', '6f130b2c3249a6b17de79351a062dcaa74ef9e7490f8945631a142a295eb52e1', '[\"*\"]', NULL, '2022-07-23 01:35:01', '2022-07-23 01:35:01'),
(667, 'App\\Models\\User', 5, 'myapptoken', 'aba8bb0dbd012ff2ba3f40e49e4f451a4795236f77d251adb7c6ea5d702cd36d', '[\"*\"]', NULL, '2022-07-23 01:35:28', '2022-07-23 01:35:28'),
(668, 'App\\Models\\User', 3, 'myapptoken', '5522fa83729f630a43db9621a805be09410f4658cb09712835d499678697a660', '[\"*\"]', NULL, '2022-07-23 01:37:19', '2022-07-23 01:37:19'),
(669, 'App\\Models\\User', 4, 'myapptoken', 'afe02bf7d8a1893d3b2cb3ed5ef80e1b75528d30ef46fd6c461cabb24c5d2085', '[\"*\"]', NULL, '2022-07-23 01:37:40', '2022-07-23 01:37:40'),
(670, 'App\\Models\\User', 3, 'myapptoken', '0a6367966e7815ccdbe016f68d133e00116cda28c9b86fbfc975689c2a56e5a2', '[\"*\"]', NULL, '2022-07-23 01:40:16', '2022-07-23 01:40:16'),
(671, 'App\\Models\\User', 6, 'myapptoken', '76d9009b29c91070f448810feeb893d47b5e4f76916f717320b8fb0e9f74274f', '[\"*\"]', NULL, '2022-07-23 01:42:08', '2022-07-23 01:42:08'),
(672, 'App\\Models\\User', 3, 'myapptoken', '4649762f86e9351a603a9cee9052bdf67826a9fe2615c141d543230fdcf20cc3', '[\"*\"]', NULL, '2022-07-23 01:43:49', '2022-07-23 01:43:49'),
(673, 'App\\Models\\User', 5, 'myapptoken', 'e0e9713a453d2d66a1ac7daaf453b4b14162d34b412bd00b45d1c3aea3d85502', '[\"*\"]', NULL, '2022-07-23 01:43:56', '2022-07-23 01:43:56'),
(674, 'App\\Models\\User', 7, 'myapptoken', 'f40ac9c2a382f46e16183cb60c2a98df7e82c634a6f73ffda12f48584f690de9', '[\"*\"]', NULL, '2022-07-23 01:44:59', '2022-07-23 01:44:59'),
(675, 'App\\Models\\User', 4, 'myapptoken', '4cd5af12cef24b7f080b306d77a360c7768e0ff42d05753fabeb0e3cecc39485', '[\"*\"]', NULL, '2022-07-23 04:29:30', '2022-07-23 04:29:30'),
(676, 'App\\Models\\User', 3, 'myapptoken', '8f55b6d00dbcf62860853078acbf4f82c12b86894de4ac98d429fdaf32e107ed', '[\"*\"]', NULL, '2022-07-23 04:31:01', '2022-07-23 04:31:01'),
(677, 'App\\Models\\User', 4, 'myapptoken', 'de865bf75c3a46fe293c53028867a881d1996eb9b7f2d320ffe213c2276a6039', '[\"*\"]', NULL, '2022-07-23 04:31:41', '2022-07-23 04:31:41'),
(678, 'App\\Models\\User', 7, 'myapptoken', '0a1b919fea0dc2c193088fb377120a0d764bca082e84214aea2b4f1ecf28cf42', '[\"*\"]', NULL, '2022-07-23 04:31:48', '2022-07-23 04:31:48'),
(679, 'App\\Models\\User', 7, 'myapptoken', 'cce689be45062c4ae67b1a1fd5f45b570c039be577322447cd6320d2a9daddb3', '[\"*\"]', NULL, '2022-07-23 04:34:14', '2022-07-23 04:34:14'),
(680, 'App\\Models\\User', 5, 'myapptoken', '30a906642660261d8b656bfd16c618399dd39e23593371797bc31932bfd04935', '[\"*\"]', NULL, '2022-07-23 04:34:57', '2022-07-23 04:34:57'),
(681, 'App\\Models\\User', 5, 'myapptoken', 'b4f3b35de08ef0283523cd9bc197d0631312a2b0becfaa4d33b92d06de3b1de2', '[\"*\"]', NULL, '2022-07-23 04:35:36', '2022-07-23 04:35:36'),
(682, 'App\\Models\\User', 3, 'myapptoken', 'd76cc0b2efd01bce64b7380d790ad49df87778ac57345e9a9eb316019bf0867f', '[\"*\"]', NULL, '2022-07-23 04:35:46', '2022-07-23 04:35:46'),
(683, 'App\\Models\\User', 4, 'myapptoken', 'c90bddd09eff65e9fa05340c1ee6e0d2ab9b1857d19ddc57ae004a918286b89c', '[\"*\"]', NULL, '2022-07-23 04:37:50', '2022-07-23 04:37:50'),
(684, 'App\\Models\\User', 4, 'myapptoken', 'fea4b943a1722c4c9962bbbf13478e604ae056ffa19b1dfcd0f249525603d604', '[\"*\"]', NULL, '2022-07-23 05:04:58', '2022-07-23 05:04:58'),
(685, 'App\\Models\\User', 7, 'myapptoken', 'd4aacfaae0b892776e12f0009cb8fa523e8c9512bd1d6eedbccadcf34588ad6e', '[\"*\"]', NULL, '2022-07-23 05:05:14', '2022-07-23 05:05:14'),
(686, 'App\\Models\\User', 7, 'myapptoken', '8f5a71db5e3bd5a7a42b7e55c94fac4b6b3af99a12a9758c47aad9e575713238', '[\"*\"]', NULL, '2022-07-23 05:05:35', '2022-07-23 05:05:35'),
(687, 'App\\Models\\User', 7, 'myapptoken', '0eba17807adbc89b97f647e668ac899e592079dc6703534af6f68c3502129f3e', '[\"*\"]', NULL, '2022-07-23 05:05:46', '2022-07-23 05:05:46'),
(688, 'App\\Models\\User', 4, 'myapptoken', '3cce5e56ebb5dfeacd601c21b6512f60b3a026ed45299c660536321ed4246148', '[\"*\"]', NULL, '2022-07-23 05:06:54', '2022-07-23 05:06:54'),
(689, 'App\\Models\\User', 3, 'myapptoken', '022ec291699a56d4973eb6ff708bfdd18a81ab7a86c59ac770230e96227fe8f2', '[\"*\"]', NULL, '2022-07-23 05:07:05', '2022-07-23 05:07:05'),
(690, 'App\\Models\\User', 4, 'myapptoken', 'be3127c55f49e04d0a41b323a9219f55f2e0a7172ef1e74025c0d3f915205b65', '[\"*\"]', NULL, '2022-07-23 05:09:13', '2022-07-23 05:09:13'),
(691, 'App\\Models\\User', 6, 'myapptoken', '9e9cb0142a0c1d7d34352e1948ffd36c8f3316e15e75c9151414ed8368632be3', '[\"*\"]', NULL, '2022-07-23 05:09:23', '2022-07-23 05:09:23'),
(692, 'App\\Models\\User', 7, 'myapptoken', '1e262b1da73da18d189798abf4b818f45eecccb2bea42bfb67054ab2aeb0f6bd', '[\"*\"]', NULL, '2022-07-23 05:09:52', '2022-07-23 05:09:52'),
(693, 'App\\Models\\User', 6, 'myapptoken', 'f3ee0d3621629e212fedba9691c141e6e7f15fe950e0d27c5279045b4366259a', '[\"*\"]', NULL, '2022-07-23 05:10:41', '2022-07-23 05:10:41'),
(694, 'App\\Models\\User', 3, 'myapptoken', '65f6c7dcc1870a233dfab238380dc2f332b5836dfa7cbb6ee0affb22c9cc210a', '[\"*\"]', NULL, '2022-07-23 22:55:07', '2022-07-23 22:55:07'),
(695, 'App\\Models\\User', 3, 'myapptoken', 'c3b43786b1b2f27a3d4050af0040d92c3f3fe50807ab3f0453200e13c1c75ea3', '[\"*\"]', NULL, '2022-07-23 22:59:42', '2022-07-23 22:59:42'),
(696, 'App\\Models\\User', 7, 'myapptoken', 'c304c36861282fab0e5be04252528d7316333470b1c6fa7810dc0d3e0d16896a', '[\"*\"]', NULL, '2022-07-23 23:02:55', '2022-07-23 23:02:55'),
(697, 'App\\Models\\User', 3, 'myapptoken', '7229fc76fda73a4baa91fa5b73ea1653bb71226557bc48ffdb4e60b7e2e93464', '[\"*\"]', NULL, '2022-07-23 23:03:20', '2022-07-23 23:03:20'),
(698, 'App\\Models\\User', 7, 'myapptoken', '6816268daad44b5a42b420e32bfdcaf823208227f33664da6881c662a6cd88db', '[\"*\"]', NULL, '2022-07-23 23:39:29', '2022-07-23 23:39:29'),
(699, 'App\\Models\\User', 7, 'myapptoken', '41c14a6162d40ddd81923fdab9f04081817db46ffa9aba7a8b74b8714e9c08c3', '[\"*\"]', NULL, '2022-07-23 23:41:30', '2022-07-23 23:41:30'),
(700, 'App\\Models\\User', 3, 'myapptoken', 'd7e66113bbea410539740874e622528b0c1a0ec2a6fbb7abed32ec5ab3efe7b5', '[\"*\"]', NULL, '2022-07-23 23:42:53', '2022-07-23 23:42:53'),
(701, 'App\\Models\\User', 4, 'myapptoken', '5caea286c04629489b76e85c48b2b6290c0637bfa495d16246ae02997f1463bc', '[\"*\"]', NULL, '2022-07-24 00:07:41', '2022-07-24 00:07:41'),
(702, 'App\\Models\\User', 4, 'myapptoken', '241e351579838e3263e1be37c395eb0f017d7cf1bec0c0b1e35057af910a3c6b', '[\"*\"]', NULL, '2022-07-24 00:07:54', '2022-07-24 00:07:54'),
(703, 'App\\Models\\User', 5, 'myapptoken', 'b745e8bf12175c660b7bc567db6a788562f6b656757ec5b6969e1df744ce8cf5', '[\"*\"]', NULL, '2022-07-24 00:08:03', '2022-07-24 00:08:03'),
(704, 'App\\Models\\User', 3, 'myapptoken', 'a38130eb512e3c0e0da0c11baee8a3d0d499c504d4c2ed9b085db002dd719d15', '[\"*\"]', NULL, '2022-07-24 00:08:10', '2022-07-24 00:08:10'),
(705, 'App\\Models\\User', 4, 'myapptoken', '5c471822944ba47b02397695d51bf2f4ef28e09db5e2105b235aa52de7dc2d25', '[\"*\"]', NULL, '2022-07-24 00:08:25', '2022-07-24 00:08:25'),
(706, 'App\\Models\\User', 3, 'myapptoken', '4243a3690617223f41c63000310d3bd4c8c97357daab7396ceefe1583c7af603', '[\"*\"]', NULL, '2022-07-24 00:09:03', '2022-07-24 00:09:03'),
(707, 'App\\Models\\User', 7, 'myapptoken', '76eb6b797042b1651c5ff4bc0859d401cbc712284d09f11aa4f12c199db723f8', '[\"*\"]', NULL, '2022-07-24 00:09:31', '2022-07-24 00:09:31'),
(708, 'App\\Models\\User', 3, 'myapptoken', 'a826316938a63cc1567e30e86af2b468a095b987ad12c7c8bed26873936bee90', '[\"*\"]', NULL, '2022-07-24 00:09:54', '2022-07-24 00:09:54'),
(709, 'App\\Models\\User', 3, 'myapptoken', '27ae9dd0c0f7780a7f7440adf8a65e09d696294d4a021e6393a1d7e7015a1795', '[\"*\"]', NULL, '2022-07-24 06:51:13', '2022-07-24 06:51:13'),
(710, 'App\\Models\\User', 4, 'myapptoken', '6c98020f25215305f95ea9b65dd7c0f5b4a4621bedf272832541d840ba4fa6e4', '[\"*\"]', NULL, '2022-07-24 07:04:54', '2022-07-24 07:04:54'),
(711, 'App\\Models\\User', 4, 'myapptoken', 'cab2b2b39b91ca154485f50f88a2e12bef31585a38166261c5f000f71fdbb39d', '[\"*\"]', NULL, '2022-07-24 07:05:24', '2022-07-24 07:05:24'),
(712, 'App\\Models\\User', 4, 'myapptoken', 'a8f5d65efeb28d5acce34702a92c91bd0b0c5fa81ec0d9e8d44629c48b0dab4c', '[\"*\"]', NULL, '2022-07-24 07:05:39', '2022-07-24 07:05:39'),
(713, 'App\\Models\\User', 3, 'myapptoken', 'cc2d5e97fbb4c1abfe3561e74d30a16e61b7d9cf058e6b6346aad87b31afc875', '[\"*\"]', NULL, '2022-07-24 07:07:02', '2022-07-24 07:07:02'),
(714, 'App\\Models\\User', 7, 'myapptoken', 'f3f44589a662d04d6c004e2816054e1030c921bea5d25206ec5c7f032131cdd6', '[\"*\"]', NULL, '2022-07-24 07:08:02', '2022-07-24 07:08:02'),
(715, 'App\\Models\\User', 3, 'myapptoken', '9af3c4ff6ea7049876627480d66c52fcb1c4e636c9cf960deb7c223fd6b0ca5d', '[\"*\"]', NULL, '2022-07-24 07:08:55', '2022-07-24 07:08:55'),
(716, 'App\\Models\\User', 5, 'myapptoken', 'e312763d1a3abb9b615e13aa12b6d8944dfc22c534617c5f0f99583b68d41c6a', '[\"*\"]', NULL, '2022-07-24 07:13:14', '2022-07-24 07:13:14'),
(717, 'App\\Models\\User', 3, 'myapptoken', '2927fb5b372231ded27817e2492200815fd067c8290234bfdd0374f7e4194cae', '[\"*\"]', NULL, '2022-07-24 07:13:41', '2022-07-24 07:13:41'),
(718, 'App\\Models\\User', 7, 'myapptoken', '2f24a43e8b54f0f084f60638d66eb85e1c29b1aa176682a00a1518637bf24a7a', '[\"*\"]', NULL, '2022-07-24 07:15:13', '2022-07-24 07:15:13'),
(719, 'App\\Models\\User', 3, 'myapptoken', '3f663ccf893c7622913f0c3b8ebccff16b99185fa85d2c19e84c7c310bf11219', '[\"*\"]', NULL, '2022-07-24 07:15:37', '2022-07-24 07:15:37'),
(720, 'App\\Models\\User', 7, 'myapptoken', '6f2ca09ed1a2c399b37e1dda147a311b36ce4877ee66001ff0f4227f57434fdb', '[\"*\"]', NULL, '2022-07-24 07:47:21', '2022-07-24 07:47:21'),
(721, 'App\\Models\\User', 4, 'myapptoken', 'f054cdb624016a04e7546ef11388e0adcafae87f5939522fc36c8480e207900e', '[\"*\"]', NULL, '2022-07-24 07:50:10', '2022-07-24 07:50:10'),
(722, 'App\\Models\\User', 3, 'myapptoken', '59e35f0a7a27f86db8dea6329c698dbf269e9cfdf7030f68cb765a96aeeb084d', '[\"*\"]', NULL, '2022-07-24 07:50:53', '2022-07-24 07:50:53'),
(723, 'App\\Models\\User', 7, 'myapptoken', '28cbf649f64e55207f9545226f7a5952954f10fd3cff60a901c520430baa6013', '[\"*\"]', NULL, '2022-07-24 07:51:23', '2022-07-24 07:51:23'),
(724, 'App\\Models\\User', 3, 'myapptoken', 'a869bb33fb9d900c639979906a463400da8622e6e33c8fdf9e3f1f28220bbe36', '[\"*\"]', NULL, '2022-07-24 07:51:45', '2022-07-24 07:51:45'),
(725, 'App\\Models\\User', 7, 'myapptoken', '028319071267c78024d0af7ac25ec2f075caf0ee2e55a3ce4b94e1d420e9024d', '[\"*\"]', NULL, '2022-07-24 07:56:13', '2022-07-24 07:56:13'),
(726, 'App\\Models\\User', 3, 'myapptoken', '0e050794b2c7f9fbd1e2ee737b591ce63d8b9e28d97e9af39a843f2404585573', '[\"*\"]', NULL, '2022-07-24 07:58:47', '2022-07-24 07:58:47'),
(727, 'App\\Models\\User', 4, 'myapptoken', '5c3c75934f656df67179697b231dcc9ca74c5137ce60d43f565790b56aa250f5', '[\"*\"]', NULL, '2022-07-24 08:27:59', '2022-07-24 08:27:59'),
(728, 'App\\Models\\User', 6, 'myapptoken', 'ddca16cc218c9a44f7da8f97a568e2ea59cdd8d70d69421fe82938d6651141c1', '[\"*\"]', NULL, '2022-07-24 08:28:05', '2022-07-24 08:28:05'),
(729, 'App\\Models\\User', 3, 'myapptoken', 'b0bb3e660dffd93fc8709df7b7b4835a60bd68996a82b7f080887e904cfe9472', '[\"*\"]', NULL, '2022-07-24 08:28:45', '2022-07-24 08:28:45'),
(730, 'App\\Models\\User', 4, 'myapptoken', 'a94e1deeb6d7b9f1012b5cf3f38010f46705a4b6e276f4115fbb1a90e3228430', '[\"*\"]', NULL, '2022-07-25 01:44:47', '2022-07-25 01:44:47'),
(731, 'App\\Models\\User', 3, 'myapptoken', '2b8d4550fec1c5d309fb98496f0dc794728095e2cdbbd1b779466ee229fbc51e', '[\"*\"]', NULL, '2022-07-25 01:48:55', '2022-07-25 01:48:55'),
(732, 'App\\Models\\User', 3, 'myapptoken', 'f15e0284e76ebec333e381628311ca88c18ef0e662e0484edb69fca2f985ef18', '[\"*\"]', NULL, '2022-07-25 04:09:42', '2022-07-25 04:09:42'),
(733, 'App\\Models\\User', 4, 'myapptoken', 'b7fd694c306820d926994153bcafbe0fa4f310a9cc53949ae84c3d96e2570805', '[\"*\"]', NULL, '2022-07-25 04:20:19', '2022-07-25 04:20:19'),
(734, 'App\\Models\\User', 7, 'myapptoken', 'ff4ac419d3b45b76e9f7720331536cca0e86ccd964fe80306033d819078ccf7e', '[\"*\"]', NULL, '2022-07-25 04:32:02', '2022-07-25 04:32:02'),
(735, 'App\\Models\\User', 4, 'myapptoken', '466600810611e1dd4f7552bd9e155691df850e16193ab848b4567847cc1496e7', '[\"*\"]', NULL, '2022-07-25 04:32:52', '2022-07-25 04:32:52'),
(736, 'App\\Models\\User', 3, 'myapptoken', '6e7a90ad725d0a130b5181a82a0a71ae2ef9c2aec073c3828a6e9cdbb9310d81', '[\"*\"]', NULL, '2022-07-25 04:42:31', '2022-07-25 04:42:31'),
(737, 'App\\Models\\User', 6, 'myapptoken', 'b250571db0998a0f1d207b676fab241f79fb5441cb65c14f25016e5feeb0640e', '[\"*\"]', NULL, '2022-07-25 04:45:30', '2022-07-25 04:45:30'),
(738, 'App\\Models\\User', 5, 'myapptoken', '25799e25c9ac6d2eb6a7a3f3c45d6ab8c0e63fa4fbe962b94ad3cdea820d270b', '[\"*\"]', NULL, '2022-07-25 04:45:43', '2022-07-25 04:45:43'),
(739, 'App\\Models\\User', 3, 'myapptoken', 'e325e19078f3a2011bdb710626596426daf89a74ed4411d6c53910c8f12dcd5e', '[\"*\"]', NULL, '2022-07-25 04:46:02', '2022-07-25 04:46:02'),
(740, 'App\\Models\\User', 6, 'myapptoken', '7d950419839d9446abf3adfc956aa1377468337b030862591b3ef09cd7070e19', '[\"*\"]', NULL, '2022-07-25 04:47:38', '2022-07-25 04:47:38'),
(741, 'App\\Models\\User', 7, 'myapptoken', 'd136d86122058835024512e70ea6726596b2faee5cc516f50a8dd6c0b2a06797', '[\"*\"]', NULL, '2022-07-25 04:47:58', '2022-07-25 04:47:58'),
(742, 'App\\Models\\User', 3, 'myapptoken', 'ec85dd8a403c723f7ded2c08c34979461c130926f21ae28e18ff818c35cdb494', '[\"*\"]', NULL, '2022-07-25 04:48:16', '2022-07-25 04:48:16'),
(743, 'App\\Models\\User', 3, 'myapptoken', '625846e22ec7bb9637ca2ffd7133437853f5b64f43df409b726349953b7fdc64', '[\"*\"]', NULL, '2022-07-25 05:18:09', '2022-07-25 05:18:09'),
(744, 'App\\Models\\User', 3, 'myapptoken', '7032518d0b6144bdad7925c29060bfa8106e8d6fc76d71738aff2dce80575a2b', '[\"*\"]', NULL, '2022-07-25 20:06:27', '2022-07-25 20:06:27'),
(745, 'App\\Models\\User', 4, 'myapptoken', '7c90a81f0d391f57b9d6df98945eb92f5581817ed57a2f48707ec0f9f9d7dab7', '[\"*\"]', NULL, '2022-07-25 20:09:25', '2022-07-25 20:09:25'),
(746, 'App\\Models\\User', 3, 'myapptoken', '9f092d8007302464145b002ff76fc5b7c994021bbd038354e29672f3139cfee7', '[\"*\"]', NULL, '2022-07-25 20:14:36', '2022-07-25 20:14:36'),
(747, 'App\\Models\\User', 4, 'myapptoken', 'c8f8bfd9c38c55d339f372bdde018c0b07428902a6a0f7c4226cbf7d87e93f17', '[\"*\"]', NULL, '2022-07-25 20:15:19', '2022-07-25 20:15:19'),
(748, 'App\\Models\\User', 4, 'myapptoken', '81409642011fd3241aa684a23614e735fd303762793d700e38bab483b3163c7b', '[\"*\"]', NULL, '2022-07-25 21:05:23', '2022-07-25 21:05:23'),
(749, 'App\\Models\\User', 3, 'myapptoken', '8d660d193b1c923f78109d5a53be14001d6f28902a0e496abbd2552e17463c80', '[\"*\"]', NULL, '2022-07-25 21:36:39', '2022-07-25 21:36:39'),
(750, 'App\\Models\\User', 3, 'myapptoken', 'a8267971298ce68cd46d6e7337d0878bedcc031a572b5105c98769392e690d4e', '[\"*\"]', NULL, '2022-07-25 21:43:40', '2022-07-25 21:43:40'),
(751, 'App\\Models\\User', 3, 'myapptoken', 'acc14e450ad34018e53fed2ec475d0216b4ead86f45a9c6d386ca17d6a6c3bf1', '[\"*\"]', NULL, '2022-07-25 21:45:31', '2022-07-25 21:45:31'),
(752, 'App\\Models\\User', 3, 'myapptoken', '9e7b3574c79dc205fce35278bf6f38abea5d4e7ffd5e0575d93652b77eda4541', '[\"*\"]', NULL, '2022-07-25 21:47:48', '2022-07-25 21:47:48'),
(753, 'App\\Models\\User', 7, 'myapptoken', '9be525018199d77ad8d95289996ad9a37645ca4c8ae89da25180f8b594c27e13', '[\"*\"]', NULL, '2022-07-25 21:48:20', '2022-07-25 21:48:20'),
(754, 'App\\Models\\User', 3, 'myapptoken', '59c7633480a8a100f88e0144d4cf688fbd6c57031b1c6806534317da10d0ec58', '[\"*\"]', NULL, '2022-07-25 21:49:02', '2022-07-25 21:49:02'),
(755, 'App\\Models\\User', 3, 'myapptoken', '20db7f8682dffa6bfc855c013263d29faf6f60c2e14810199b24211455d09ef1', '[\"*\"]', NULL, '2022-07-25 22:01:51', '2022-07-25 22:01:51'),
(756, 'App\\Models\\User', 4, 'myapptoken', '3968561ec8314a37a231a44c631c704693c7c821bbc1806988a090ce11b343c5', '[\"*\"]', NULL, '2022-07-25 22:03:13', '2022-07-25 22:03:13'),
(757, 'App\\Models\\User', 3, 'myapptoken', '6e4209ce50a524d4695bea9222d5779d921932004fbeeabf34d5e8c20764dad0', '[\"*\"]', NULL, '2022-07-25 22:05:14', '2022-07-25 22:05:14'),
(758, 'App\\Models\\User', 4, 'myapptoken', '746547391aa7922dca3c9f5e08d5d37bf7b7fb5e28d5ecd7a5187158d9864d27', '[\"*\"]', NULL, '2022-07-25 22:10:04', '2022-07-25 22:10:04'),
(759, 'App\\Models\\User', 3, 'myapptoken', '494e0399ef527aa850ff7e9ab1c88ff971900133a39447bd9cadf3fca17a1eea', '[\"*\"]', NULL, '2022-07-25 22:11:37', '2022-07-25 22:11:37'),
(760, 'App\\Models\\User', 7, 'myapptoken', '4fda8a62896cb32ad4953ff8e98fa50ce36195a9f190d9d4ddb41cfe70a28953', '[\"*\"]', NULL, '2022-07-25 22:12:21', '2022-07-25 22:12:21'),
(761, 'App\\Models\\User', 3, 'myapptoken', 'd673959a70122feb304e9202e1ff9be669350adb926fd4bf739534917f01d470', '[\"*\"]', NULL, '2022-07-25 22:12:45', '2022-07-25 22:12:45'),
(762, 'App\\Models\\User', 3, 'myapptoken', '82c7daa0cd84243e4be83540827bd50e3196bdc73f6109f9e6f8ec3569e72feb', '[\"*\"]', NULL, '2022-07-25 22:15:59', '2022-07-25 22:15:59'),
(763, 'App\\Models\\User', 3, 'myapptoken', '35107cdb2152f55df4b3ae0cd7ddaf2110b0d233e4e542319c8f77abe7fcc5d4', '[\"*\"]', NULL, '2022-07-25 22:33:15', '2022-07-25 22:33:15'),
(764, 'App\\Models\\User', 7, 'myapptoken', '9337c934c8be2e9e08b624bd44a7f6e29fd866d68c4f28208f7ea503796c9116', '[\"*\"]', NULL, '2022-07-25 22:41:30', '2022-07-25 22:41:30'),
(765, 'App\\Models\\User', 3, 'myapptoken', 'de37c95238ed3a1706971308387e4e40fc971bff4d0d46b40aa42421da1cf128', '[\"*\"]', NULL, '2022-07-25 22:43:50', '2022-07-25 22:43:50'),
(766, 'App\\Models\\User', 7, 'myapptoken', '4ad7eda5d27c898a54952921c3afb1551d25432849d7a36ad2f40c24d90d3794', '[\"*\"]', NULL, '2022-07-25 22:44:01', '2022-07-25 22:44:01'),
(767, 'App\\Models\\User', 4, 'myapptoken', '1aae33c8c61a7d0e457fccbf90b7ab07312b312cc06b0b6d50142bfd86f27707', '[\"*\"]', NULL, '2022-07-25 23:13:45', '2022-07-25 23:13:45'),
(768, 'App\\Models\\User', 3, 'myapptoken', 'b351a18949e3c49fa018ec89cc284e6d2d4e6c7adfb77fa3c3e7563052df988d', '[\"*\"]', NULL, '2022-07-25 23:35:28', '2022-07-25 23:35:28'),
(769, 'App\\Models\\User', 4, 'myapptoken', '2503521a7650251e7e386677d6cb2c29313b9d53f7ca65d6f1c42f966aee2db3', '[\"*\"]', NULL, '2022-07-25 23:36:23', '2022-07-25 23:36:23'),
(770, 'App\\Models\\User', 7, 'myapptoken', '280c1d73ea00bff6a05a5e22686b6ce78944e3b28646c603e639f2a7e41e63d8', '[\"*\"]', NULL, '2022-07-25 23:36:31', '2022-07-25 23:36:31'),
(771, 'App\\Models\\User', 3, 'myapptoken', '9d16f02d60a6940acdb5d7f030eea4c94c98791aa17d3d37eb4c846c76ba0bb1', '[\"*\"]', NULL, '2022-07-25 23:38:59', '2022-07-25 23:38:59'),
(772, 'App\\Models\\User', 7, 'myapptoken', '15afd02a411cf6124096dd14843fcbcbaaff69940e1ecb5ea2ea4e285324ec70', '[\"*\"]', NULL, '2022-07-25 23:42:19', '2022-07-25 23:42:19'),
(773, 'App\\Models\\User', 4, 'myapptoken', '6dbbf547a2e49593040966e83e4979b7b434023f33c6fcc0c0402a301320faf1', '[\"*\"]', NULL, '2022-07-25 23:42:52', '2022-07-25 23:42:52'),
(774, 'App\\Models\\User', 3, 'myapptoken', '428f64c30c8f5c0eb515afd049e960c7e33eb38fb728a82f7885592ba93c09c6', '[\"*\"]', NULL, '2022-07-25 23:43:08', '2022-07-25 23:43:08'),
(775, 'App\\Models\\User', 3, 'myapptoken', '0398d868ecc7eb20f1e438a8179199e57bd73c57f8e094fa272b0ff97ed844a0', '[\"*\"]', NULL, '2022-07-25 23:56:28', '2022-07-25 23:56:28'),
(776, 'App\\Models\\User', 7, 'myapptoken', '90054e7bb11289aedf7006d953a379e24958a8d11f8a882668c4c3b19304229f', '[\"*\"]', NULL, '2022-07-25 23:57:02', '2022-07-25 23:57:02'),
(777, 'App\\Models\\User', 4, 'myapptoken', '73354e53943bfd987f66d8381708c16b27d57340d0275ebd15f7ccd46181a4fc', '[\"*\"]', NULL, '2022-07-25 23:58:01', '2022-07-25 23:58:01'),
(778, 'App\\Models\\User', 7, 'myapptoken', 'f5e49f90daadfb0d1f0ecf5c1d64e4864c15ed08d3db329190f6968e6e72486b', '[\"*\"]', NULL, '2022-07-25 23:58:30', '2022-07-25 23:58:30'),
(779, 'App\\Models\\User', 4, 'myapptoken', '265e7c41969f2785876318dc7d542f967a4b6aec5245b5738cad7ce2078c9bc3', '[\"*\"]', NULL, '2022-07-25 23:58:43', '2022-07-25 23:58:43'),
(780, 'App\\Models\\User', 3, 'myapptoken', '329952ca0f9e05461d5defb43f9283ab0b6c283f24f566ff9d844b847334ed80', '[\"*\"]', NULL, '2022-07-26 00:00:56', '2022-07-26 00:00:56'),
(781, 'App\\Models\\User', 3, 'myapptoken', '82b3e487b51a6d5c77f1555a4cff9cb61f4ade313fee693188da772a9d9ea75d', '[\"*\"]', NULL, '2022-07-26 00:01:18', '2022-07-26 00:01:18'),
(782, 'App\\Models\\User', 7, 'myapptoken', 'ab34a10e9deb132325fed53d10a24053928aa17bfca31218435d2181578a8b3f', '[\"*\"]', NULL, '2022-07-26 00:01:42', '2022-07-26 00:01:42'),
(783, 'App\\Models\\User', 3, 'myapptoken', '14fecaea73353c3e4e8519e19550c133fb94f525ee2eb2dc48839159292844d0', '[\"*\"]', NULL, '2022-07-26 00:02:17', '2022-07-26 00:02:17'),
(784, 'App\\Models\\User', 7, 'myapptoken', '6b495dad5df3cfdb9288d56702d22878dac34b2eb61d216f1afa36f20767c3fa', '[\"*\"]', NULL, '2022-07-26 00:02:40', '2022-07-26 00:02:40'),
(785, 'App\\Models\\User', 3, 'myapptoken', '99a67136784806f3ebe762aa9b21c059d500edeacf9054c0bb1172c765b1662b', '[\"*\"]', NULL, '2022-07-26 00:03:03', '2022-07-26 00:03:03'),
(786, 'App\\Models\\User', 7, 'myapptoken', '08ee30506123aa461b58e2d497e1cd8308248632130fa0b32ca77b5e54ed58fa', '[\"*\"]', NULL, '2022-07-26 00:03:28', '2022-07-26 00:03:28'),
(787, 'App\\Models\\User', 3, 'myapptoken', '792c8a67ceb5573f5dbeb1bac62d1c15d7cf54bd49d32b55f77ad9948356ad7e', '[\"*\"]', NULL, '2022-07-26 00:04:12', '2022-07-26 00:04:12'),
(788, 'App\\Models\\User', 3, 'myapptoken', '357f4d2e1dc3731ef9f241a6522e6aff85d543b770ed905444c8306f348e04ad', '[\"*\"]', NULL, '2022-07-26 00:06:22', '2022-07-26 00:06:22'),
(789, 'App\\Models\\User', 3, 'myapptoken', '055bcf8286621ea2554d2a2cf0b52f22cd1a9cce4aaeab7c414ce37b29d3e194', '[\"*\"]', NULL, '2022-07-26 00:35:54', '2022-07-26 00:35:54'),
(790, 'App\\Models\\User', 3, 'myapptoken', 'dbbc2cd9fee28bc9c032776f9f3a7a5252b354c370eb39490a9074e1823ba731', '[\"*\"]', NULL, '2022-07-26 00:36:03', '2022-07-26 00:36:03'),
(791, 'App\\Models\\User', 4, 'myapptoken', 'a3448f803191da221fcef9382a5e8bbd575cafc04a66e4d85a9dabbbaeed8a9f', '[\"*\"]', NULL, '2022-07-26 00:36:33', '2022-07-26 00:36:33'),
(792, 'App\\Models\\User', 4, 'myapptoken', '54d8b5352a85c962b695d77494904dda0f9e8a9bba9ff1e19030dfaa7e52bc83', '[\"*\"]', NULL, '2022-07-26 00:36:41', '2022-07-26 00:36:41'),
(793, 'App\\Models\\User', 4, 'myapptoken', 'dcbd2f018f995c38061beaf71bc6f13cf95220a1e11f665c1100b5518af99a3d', '[\"*\"]', NULL, '2022-07-26 01:03:17', '2022-07-26 01:03:17'),
(794, 'App\\Models\\User', 4, 'myapptoken', 'b892f5dbe52c11cf1e099ff0aaff45b0ea188b6d36c69a7bcc5dd61154679c22', '[\"*\"]', NULL, '2022-07-26 01:07:46', '2022-07-26 01:07:46'),
(795, 'App\\Models\\User', 4, 'myapptoken', '96337631791311c9003f06804cf2d496a5123394422a3ca0620aa1adc23600a9', '[\"*\"]', NULL, '2022-07-26 01:08:29', '2022-07-26 01:08:29'),
(796, 'App\\Models\\User', 3, 'myapptoken', '2b31492dcd93f3fea4dd85f2293d8588ee5fe0e99bff46375bd20658e32647cd', '[\"*\"]', NULL, '2022-07-26 01:09:35', '2022-07-26 01:09:35'),
(797, 'App\\Models\\User', 7, 'myapptoken', '36092e2a42a9b7b3f90fa2d40d4a1899ed619a39e78f6bd7b2b4e41a37fe3443', '[\"*\"]', NULL, '2022-07-26 01:10:06', '2022-07-26 01:10:06'),
(798, 'App\\Models\\User', 3, 'myapptoken', '287af1d87b781ba2ea6d7007f57c13a55c726acffc096e6770c8af3b8ba087a7', '[\"*\"]', NULL, '2022-07-26 01:10:27', '2022-07-26 01:10:27'),
(799, 'App\\Models\\User', 3, 'myapptoken', 'd96404dfb98ad32c49c8a8c828937e48d1a81dab5e1f2953df7310d61802b8b9', '[\"*\"]', NULL, '2022-07-26 01:10:36', '2022-07-26 01:10:36'),
(800, 'App\\Models\\User', 7, 'myapptoken', '14def5b6010eb7a79b7fbe467abc7bc046b589560950dd1a2d8b0ed04ae0ecec', '[\"*\"]', NULL, '2022-07-26 01:33:33', '2022-07-26 01:33:33'),
(801, 'App\\Models\\User', 4, 'myapptoken', 'd6195bb45a600272c2a625a50c680d7f21a94d05eb9a777de81a49988a255879', '[\"*\"]', NULL, '2022-07-26 01:34:46', '2022-07-26 01:34:46'),
(802, 'App\\Models\\User', 3, 'myapptoken', '3a919ca6a673a6d102d0e6ed2e77268572bf4de6ddfa050472be43e7878fa5b4', '[\"*\"]', NULL, '2022-07-26 02:11:53', '2022-07-26 02:11:53'),
(803, 'App\\Models\\User', 3, 'myapptoken', '49836307d9795ba90a1aaeeeaa8321b5ce2922bf5cea71283af42e5ef0b64bc8', '[\"*\"]', NULL, '2022-07-26 02:14:15', '2022-07-26 02:14:15'),
(804, 'App\\Models\\User', 4, 'myapptoken', '634aff9895bb6baf6bbf22a4a45d9a1fbc4324fc6b4ff2a98adb240f922c95f5', '[\"*\"]', NULL, '2022-07-26 02:15:01', '2022-07-26 02:15:01'),
(805, 'App\\Models\\User', 3, 'myapptoken', 'b520bdbd8575b2e7676ab995517db41273f069eebf8d170a1eec8d68d9320093', '[\"*\"]', NULL, '2022-07-26 02:15:18', '2022-07-26 02:15:18'),
(806, 'App\\Models\\User', 3, 'myapptoken', '7c687ab7889316c594041d4765228374ee0becdefe3e360bd5bce7e23ca173fe', '[\"*\"]', NULL, '2022-07-26 17:10:24', '2022-07-26 17:10:24'),
(807, 'App\\Models\\User', 7, 'myapptoken', '822b134bcc9491c3c84eb90ae33772dd9a6eb31368ec82c2e31912ab9bc8abb3', '[\"*\"]', NULL, '2022-07-26 17:13:33', '2022-07-26 17:13:33'),
(808, 'App\\Models\\User', 7, 'myapptoken', 'a9468fcd507b93c3fafc02ac04cd4e153c5657c41183a04d195ecd3a33836474', '[\"*\"]', NULL, '2022-07-26 19:17:10', '2022-07-26 19:17:10'),
(809, 'App\\Models\\User', 3, 'myapptoken', '1f6acdaf5dd60903c1f4a5816e666a42c45e0ddcfc0329ddf90189fd51c6764d', '[\"*\"]', NULL, '2022-07-26 19:17:28', '2022-07-26 19:17:28'),
(810, 'App\\Models\\User', 3, 'myapptoken', '0247e35807c4d03d083e0e43587a6c722ed3f86ed9c6bcd0bed929afaf6ce9cf', '[\"*\"]', NULL, '2022-07-26 23:34:06', '2022-07-26 23:34:06'),
(811, 'App\\Models\\User', 7, 'myapptoken', 'fe7ec405e1b46291a8866dfe03add17b082182f42e4c1ca77d924aaebd117d7a', '[\"*\"]', NULL, '2022-07-26 23:38:05', '2022-07-26 23:38:05'),
(812, 'App\\Models\\User', 5, 'myapptoken', 'caa2af849fbef74a75184c853a0ca0c3c1c14731f24d6c806d7dd536cde1d30b', '[\"*\"]', NULL, '2022-07-26 23:38:28', '2022-07-26 23:38:28'),
(813, 'App\\Models\\User', 3, 'myapptoken', '743ae6bd9488e5b544315c498f0d31a6ded6314f4b86e15708027af33ead1b09', '[\"*\"]', NULL, '2022-07-26 23:39:37', '2022-07-26 23:39:37'),
(814, 'App\\Models\\User', 3, 'myapptoken', 'aa9bebfd98a2166ff1157237bb4dc1380e36639092adcc58cdf7280d03e692d3', '[\"*\"]', NULL, '2022-07-26 23:43:22', '2022-07-26 23:43:22'),
(815, 'App\\Models\\User', 7, 'myapptoken', '912f854b8231d2bdc0456f19b38a658796fc1f4aa2a3d5bd9f59924875a3c2e2', '[\"*\"]', NULL, '2022-07-27 03:06:45', '2022-07-27 03:06:45'),
(816, 'App\\Models\\User', 3, 'myapptoken', '6b1793281a8d7b7cb935519346f55e51f84f6182aa82817509b42886e4200b0c', '[\"*\"]', NULL, '2022-07-27 03:06:54', '2022-07-27 03:06:54'),
(817, 'App\\Models\\User', 7, 'myapptoken', '06a3dc02bb419fa297336c8f411a7b22661fde0c0bb2d941de207d5b79d53fae', '[\"*\"]', NULL, '2022-07-27 03:08:45', '2022-07-27 03:08:45'),
(818, 'App\\Models\\User', 5, 'myapptoken', '8e1bfae6b590e7a650b1c84193688b234036ceb1e7294e74592dffd20d4e3cfd', '[\"*\"]', NULL, '2022-07-27 03:08:57', '2022-07-27 03:08:57'),
(819, 'App\\Models\\User', 4, 'myapptoken', 'd7e2534a3d93cf7d7dcdedc9acc06083935597e1ecba5c39ebcf410ff8fabaec', '[\"*\"]', NULL, '2022-07-27 03:11:51', '2022-07-27 03:11:51'),
(820, 'App\\Models\\User', 3, 'myapptoken', '1019a7661fa954b04f2706c40c9c2fdb790489cb1d2963b4646fc914456f159b', '[\"*\"]', NULL, '2022-07-27 03:12:24', '2022-07-27 03:12:24'),
(821, 'App\\Models\\User', 7, 'myapptoken', '35d8f6e4e828df32c5a4036cd23ec157952f2a096a9ff182a8336402862a5cdf', '[\"*\"]', NULL, '2022-07-27 03:12:49', '2022-07-27 03:12:49'),
(822, 'App\\Models\\User', 7, 'myapptoken', '7ed6f565ff1aab194abd6c0b2d468bb5b2420a28a5a5efbc3fee03c468998d31', '[\"*\"]', NULL, '2022-07-27 03:13:07', '2022-07-27 03:13:07'),
(823, 'App\\Models\\User', 3, 'myapptoken', '59bb4fa9837800340142988ef0e72af9c1ebec80370a59a1a9f39dd4ddb327b1', '[\"*\"]', NULL, '2022-07-27 03:13:22', '2022-07-27 03:13:22'),
(824, 'App\\Models\\User', 7, 'myapptoken', 'f436a2243d70573fe30e761acf06b0bf11371f8e49b22531010d36705ddbf0ad', '[\"*\"]', NULL, '2022-07-28 02:42:03', '2022-07-28 02:42:03'),
(825, 'App\\Models\\User', 3, 'myapptoken', 'd587bce9b0d87f580313bbc48ed3d0b4f8fc9afec9429faa85a29f0419af9921', '[\"*\"]', NULL, '2022-07-28 02:43:54', '2022-07-28 02:43:54'),
(826, 'App\\Models\\User', 3, 'myapptoken', '3fa304eed0e47e63e3cbe3a099a1767f040079af852a0837653a9b8e4d4dc7da', '[\"*\"]', NULL, '2022-07-28 03:10:26', '2022-07-28 03:10:26'),
(827, 'App\\Models\\User', 4, 'myapptoken', '3ad1dba67ef6282638dd93f8929202e45341b5f2f789c79cdaeed4ad23be6fca', '[\"*\"]', NULL, '2022-07-28 03:10:38', '2022-07-28 03:10:38'),
(828, 'App\\Models\\User', 3, 'myapptoken', 'c98a401a3311dcd43a6b802453f8b4846645f89dfd0d3b5dfb067872cfb3a1ce', '[\"*\"]', NULL, '2022-07-28 03:34:25', '2022-07-28 03:34:25'),
(829, 'App\\Models\\User', 4, 'myapptoken', '6cad54eaccfb520c09e36b2e290442dd50c5bd8ac370f37fd3ad503c9ee3bdb6', '[\"*\"]', NULL, '2022-07-28 03:35:42', '2022-07-28 03:35:42'),
(830, 'App\\Models\\User', 3, 'myapptoken', '58bab8a6d6e287d2425e5d1399265d6a06000a4590e9eda70c4a294ec435e28f', '[\"*\"]', NULL, '2022-07-28 03:55:00', '2022-07-28 03:55:00'),
(831, 'App\\Models\\User', 7, 'myapptoken', '60f0fe2a084c56b070c1e45f10df8b7b6d993edb325a6a4b7445057e7d50bb16', '[\"*\"]', NULL, '2022-07-28 03:55:10', '2022-07-28 03:55:10'),
(832, 'App\\Models\\User', 4, 'myapptoken', 'a62dc7dedbeac616a62f7753f333d5cfd9e9d64ec4899cae0d51d5e5139e7d50', '[\"*\"]', NULL, '2022-07-28 03:55:27', '2022-07-28 03:55:27'),
(833, 'App\\Models\\User', 3, 'myapptoken', 'e7748d4b6ed3efcc749df0641d67eb50c904ef37786c54c817c991b9bcf3de12', '[\"*\"]', NULL, '2022-07-28 04:31:33', '2022-07-28 04:31:33'),
(834, 'App\\Models\\User', 3, 'myapptoken', '29d8b4517d7e360193da402fcad49ac538238140c1f988164064036a31b8d725', '[\"*\"]', NULL, '2022-07-28 04:43:46', '2022-07-28 04:43:46'),
(835, 'App\\Models\\User', 4, 'myapptoken', '4131f65274f6b0130e1fae665cf6b07eeab9b2f69db050f5ab60e98bfec0fd2a', '[\"*\"]', NULL, '2022-07-28 05:18:34', '2022-07-28 05:18:34'),
(836, 'App\\Models\\User', 4, 'myapptoken', 'c38289fa1385a0cb7a2244e77c1bed6a63dbe3e0912881b1b0ec502f7a9abe87', '[\"*\"]', NULL, '2022-07-28 05:21:40', '2022-07-28 05:21:40'),
(837, 'App\\Models\\User', 4, 'myapptoken', '701443df701fb83b66f14c2bc176bf88385a391389b467e1b96361dea01d0d25', '[\"*\"]', NULL, '2022-07-28 05:22:10', '2022-07-28 05:22:10'),
(838, 'App\\Models\\User', 3, 'myapptoken', 'bae644eb6ac61b3f1da35792fdc9cacd4141c13152211ed54c9e062bbcf431c7', '[\"*\"]', NULL, '2022-07-28 14:14:24', '2022-07-28 14:14:24'),
(839, 'App\\Models\\User', 4, 'myapptoken', '4866cb8f9e9c4dce68cbc28836d27566884f1987ea8e47f12d96b6ccea0ef87c', '[\"*\"]', NULL, '2022-07-28 14:32:52', '2022-07-28 14:32:52'),
(840, 'App\\Models\\User', 3, 'myapptoken', '6177effa4e3e0167deab840d72bd5df952bf3c853b15d493641e921098f7decf', '[\"*\"]', NULL, '2022-07-28 14:33:09', '2022-07-28 14:33:09'),
(841, 'App\\Models\\User', 4, 'myapptoken', '6b0cfd32caeb75b959d6030fa95e33dc224f21f785b2d38b24d4d1a6cdb91ff2', '[\"*\"]', NULL, '2022-07-28 14:36:29', '2022-07-28 14:36:29'),
(842, 'App\\Models\\User', 7, 'myapptoken', 'bf037375949803a5dbc7cc4b9ee4c8f9bb96d9144259709db6d81fb941f6aa3e', '[\"*\"]', NULL, '2022-07-28 14:36:42', '2022-07-28 14:36:42'),
(843, 'App\\Models\\User', 3, 'myapptoken', '9efe8ed267fe0547f44e5485979e32046311ed6a958f4d55dfd9e2153785cd2e', '[\"*\"]', NULL, '2022-07-28 14:37:06', '2022-07-28 14:37:06'),
(844, 'App\\Models\\User', 4, 'myapptoken', '5c5bf1b45b19111b9849847b745c1cb7a3dac75e3e43fff7bbfef698188d8df5', '[\"*\"]', NULL, '2022-07-28 16:31:47', '2022-07-28 16:31:47'),
(845, 'App\\Models\\User', 3, 'myapptoken', '645a198d3588fc58e77946c72541fff1d9d19bd4aec341bb4a28f37b72dea17a', '[\"*\"]', NULL, '2022-07-28 16:32:55', '2022-07-28 16:32:55'),
(846, 'App\\Models\\User', 7, 'myapptoken', 'b7c95a3af0a320ebedc5d110b59a89dcaf029effde14809b4cfb085560240ec8', '[\"*\"]', NULL, '2022-07-28 16:33:57', '2022-07-28 16:33:57'),
(847, 'App\\Models\\User', 6, 'myapptoken', '51ce663f7394d98b7425bb6269c96e2d46a8438c7962c9b5cf33e627f99e2519', '[\"*\"]', NULL, '2022-07-28 16:34:19', '2022-07-28 16:34:19'),
(848, 'App\\Models\\User', 3, 'myapptoken', '08dcd33d6b56fcde6f8cbfef7ae7d0803fb548062740bd9d32e71ed10255f400', '[\"*\"]', NULL, '2022-07-28 16:34:39', '2022-07-28 16:34:39'),
(849, 'App\\Models\\User', 3, 'myapptoken', '8f7e77d6abea8b5279f774c626a1c777ba9baad25ae7978e21a48e1c77b445b6', '[\"*\"]', NULL, '2022-07-28 23:57:49', '2022-07-28 23:57:49'),
(850, 'App\\Models\\User', 7, 'myapptoken', 'b6da09d6ba53544d47983374255e6cb406d0863489b448276ea6e324d38bef75', '[\"*\"]', NULL, '2022-07-29 00:07:31', '2022-07-29 00:07:31'),
(851, 'App\\Models\\User', 4, 'myapptoken', '3f4325eeb52a319f6fca15e5d796360994480c828e39e97bc8898d6749f92e77', '[\"*\"]', NULL, '2022-07-29 00:07:57', '2022-07-29 00:07:57'),
(852, 'App\\Models\\User', 3, 'myapptoken', 'dec60555173d25adf688223b5a436f54f26403fabb83bce3d800154c12a32510', '[\"*\"]', NULL, '2022-07-29 00:09:06', '2022-07-29 00:09:06'),
(853, 'App\\Models\\User', 4, 'myapptoken', '33341a9b26a79902e0d11101960ae7adc84cb8ac821076d0c1c5c720c384f143', '[\"*\"]', NULL, '2022-07-29 00:11:55', '2022-07-29 00:11:55'),
(854, 'App\\Models\\User', 4, 'myapptoken', '27d4d3021df0f459db0d1d813e307445d8ea5b64bde9fff9ac2b6ce726a010b5', '[\"*\"]', NULL, '2022-07-29 00:23:58', '2022-07-29 00:23:58'),
(855, 'App\\Models\\User', 3, 'myapptoken', '065eff90331c3cf4363f755d72b236384cd57672a0a321b924b922011b537b4d', '[\"*\"]', NULL, '2022-07-29 23:41:59', '2022-07-29 23:41:59'),
(856, 'App\\Models\\User', 4, 'myapptoken', '83b4e06c74e59925fa4f847309f4e635a600e74d50a08a3ab2876ff09ca0eb93', '[\"*\"]', NULL, '2022-07-29 23:42:59', '2022-07-29 23:42:59'),
(857, 'App\\Models\\User', 3, 'myapptoken', '510f9212a7024bcaaee4c9c136eb7121633e944aaa582426e7c51e4a50d72e11', '[\"*\"]', NULL, '2022-07-29 23:44:03', '2022-07-29 23:44:03'),
(858, 'App\\Models\\User', 4, 'myapptoken', '8ae3515cae46926672c32eb66bca95b949b1e4a6fd06f0b81e830bf2faf4068d', '[\"*\"]', NULL, '2022-07-30 00:17:06', '2022-07-30 00:17:06'),
(859, 'App\\Models\\User', 3, 'myapptoken', 'b43715b588c7f468a02cc39622e6221ed962bef8aa66b61ebab59078e60c2cc6', '[\"*\"]', NULL, '2022-07-30 00:17:20', '2022-07-30 00:17:20'),
(860, 'App\\Models\\User', 7, 'myapptoken', '7bdac079d1190239ef26e9940ed6a344fb1d87cdf27b4d00ecb4378da22b7b5f', '[\"*\"]', NULL, '2022-07-30 00:20:15', '2022-07-30 00:20:15'),
(861, 'App\\Models\\User', 3, 'myapptoken', '11e5c47c8c2275db144093fdabbb6bc7185fbff7fe0c248f9bef3ad06ddd2738', '[\"*\"]', NULL, '2022-07-30 00:20:29', '2022-07-30 00:20:29'),
(862, 'App\\Models\\User', 3, 'myapptoken', '8fec160ce706f6c5355fade3d8c312d6068e7a84222a0356aee7ab5e72520943', '[\"*\"]', NULL, '2022-07-30 00:44:08', '2022-07-30 00:44:08'),
(863, 'App\\Models\\User', 4, 'myapptoken', '471635aec921e466e5d16a7522ee14391319377710cbd2baefc1ab2c91d24458', '[\"*\"]', NULL, '2022-07-30 02:18:26', '2022-07-30 02:18:26'),
(864, 'App\\Models\\User', 3, 'myapptoken', '9465c4239d9cd22e5ac474f01590d3778dee5e671379c1b7f969e5688d8377e4', '[\"*\"]', NULL, '2022-07-30 02:19:26', '2022-07-30 02:19:26'),
(865, 'App\\Models\\User', 7, 'myapptoken', 'c8aff5bc7d3f68f3e0ed49064500b5405acde08ea4e72d1aabac1cea65235fed', '[\"*\"]', NULL, '2022-07-30 02:24:49', '2022-07-30 02:24:49'),
(866, 'App\\Models\\User', 3, 'myapptoken', 'fc19c370032ac49e2e60bd63af8822a7a7e8630d392f512ae9bd5de5d107906b', '[\"*\"]', NULL, '2022-07-30 02:25:27', '2022-07-30 02:25:27'),
(867, 'App\\Models\\User', 3, 'myapptoken', 'cb23c2a416864595d2f231655efb55f7e0dc55d0e7bcacff9d00fd877f277215', '[\"*\"]', NULL, '2022-07-30 02:26:53', '2022-07-30 02:26:53'),
(868, 'App\\Models\\User', 7, 'myapptoken', 'a15787aa78670bb886dbe69605a9b443af5c6699be3a1f8d750a2faa9c79330c', '[\"*\"]', NULL, '2022-07-30 02:27:18', '2022-07-30 02:27:18'),
(869, 'App\\Models\\User', 3, 'myapptoken', 'd3de8d0fd0cff243927e25dcf4391d29c78c7529c6c4c4c700b6381317d5794f', '[\"*\"]', NULL, '2022-07-30 02:27:49', '2022-07-30 02:27:49'),
(870, 'App\\Models\\User', 7, 'myapptoken', 'ed7b0cebdb173a1b1bf7fdfb0dfb7435f099ef665b2b1e597e5c685d4cc5d087', '[\"*\"]', NULL, '2022-07-30 02:48:07', '2022-07-30 02:48:07'),
(871, 'App\\Models\\User', 3, 'myapptoken', '92e1dd88b48b211200f032b054ac66cd4c087a4c22ee8456734feb5372a2539b', '[\"*\"]', NULL, '2022-07-30 02:48:24', '2022-07-30 02:48:24'),
(872, 'App\\Models\\User', 7, 'myapptoken', '97bd4c17e576f6dca4ad42351c878c8d1b82c83463874c68d7e861ccea0691d9', '[\"*\"]', NULL, '2022-07-30 04:44:48', '2022-07-30 04:44:48'),
(873, 'App\\Models\\User', 4, 'myapptoken', '11f838b27887d1c90d794507ef31b368d4931caa9e65d6ef6d25fc9a40e97815', '[\"*\"]', NULL, '2022-07-30 04:45:01', '2022-07-30 04:45:01'),
(874, 'App\\Models\\User', 3, 'myapptoken', 'b70d628979a11bd13510213cc2c246c70718c12df5af259f778c0ab86a5bcf70', '[\"*\"]', NULL, '2022-07-30 05:18:45', '2022-07-30 05:18:45'),
(875, 'App\\Models\\User', 3, 'myapptoken', 'e6e73fb9089d58541967ee08763d1d3c5d4cb2e2d9700b9e4aebfa801730f994', '[\"*\"]', NULL, '2022-07-31 01:40:00', '2022-07-31 01:40:00'),
(876, 'App\\Models\\User', 7, 'myapptoken', '2772cd5b205b98b5011e96b8b7b840af02943db96abc27ba520fd97b7b5946fd', '[\"*\"]', NULL, '2022-07-31 01:40:33', '2022-07-31 01:40:33'),
(877, 'App\\Models\\User', 3, 'myapptoken', 'ff9fb6f2f9e5a7b42b0d98eacf87c493e20f5b3b276bc3eddaf27b387e61b5a9', '[\"*\"]', NULL, '2022-07-31 01:40:43', '2022-07-31 01:40:43'),
(878, 'App\\Models\\User', 4, 'myapptoken', '8ba056e152c3f3264d704be52d2c6924761890aacee495ecd2c1a01c415c605a', '[\"*\"]', NULL, '2022-07-31 01:40:53', '2022-07-31 01:40:53'),
(879, 'App\\Models\\User', 3, 'myapptoken', 'c94eb6339376ec9325c216630093ca5245927bf0818ed653cf24428c85e90209', '[\"*\"]', NULL, '2022-07-31 01:41:44', '2022-07-31 01:41:44'),
(880, 'App\\Models\\User', 3, 'myapptoken', '34d6bed8dae2598a42adb4e09e8eb20bbd7b7c138e0f1232190711fe6741215e', '[\"*\"]', NULL, '2022-07-31 07:00:01', '2022-07-31 07:00:01'),
(881, 'App\\Models\\User', 5, 'myapptoken', 'ca5a6a451c2538af163dd84414e98cc9e7dd232e823e2ef7ad721cea1c7f3600', '[\"*\"]', NULL, '2022-07-31 07:01:28', '2022-07-31 07:01:28'),
(882, 'App\\Models\\User', 3, 'myapptoken', '67c9e0221e82af68e21d304b34cef152a3d8a367e9ee5b8b93cde0e1fcbf0148', '[\"*\"]', NULL, '2022-07-31 07:01:49', '2022-07-31 07:01:49'),
(883, 'App\\Models\\User', 3, 'myapptoken', 'e8554415edc1a7cf36e493e5aa12700a3af082e4e325c0aa509b49f7df8178ce', '[\"*\"]', NULL, '2022-07-31 07:15:52', '2022-07-31 07:15:52'),
(884, 'App\\Models\\User', 4, 'myapptoken', 'b4b8f43106ec77f92f4d2e05461233f6d333596cb159ded261a8fe8fc0fe2409', '[\"*\"]', NULL, '2022-07-31 07:22:44', '2022-07-31 07:22:44'),
(885, 'App\\Models\\User', 7, 'myapptoken', '50fcc1191fadc634fb8f0084ebed7a9ccc8ea1f6df89436354c9ffa1bfa37041', '[\"*\"]', NULL, '2022-07-31 10:13:15', '2022-07-31 10:13:15'),
(886, 'App\\Models\\User', 3, 'myapptoken', 'c4933108e1dd967444b53e5cec730e16e3f36cc5161e759d854c3bbda85be3c8', '[\"*\"]', NULL, '2022-07-31 10:13:28', '2022-07-31 10:13:28'),
(887, 'App\\Models\\User', 3, 'myapptoken', 'c9c4ecb8a910100b6dee1820c9558557e57fbcc38ce3edb3693370464b82ac65', '[\"*\"]', NULL, '2022-07-31 10:17:17', '2022-07-31 10:17:17');
INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `created_at`, `updated_at`) VALUES
(888, 'App\\Models\\User', 7, 'myapptoken', '023109929356f63ccd310f438964535cee96cadc41e80c9a0d2246d6f75f7df1', '[\"*\"]', NULL, '2022-07-31 10:40:20', '2022-07-31 10:40:20'),
(889, 'App\\Models\\User', 3, 'myapptoken', 'cb10cdec3f7b27d024188c67afafa248929d6e2aaf8e8ff14934a1e3201c18a1', '[\"*\"]', NULL, '2022-07-31 10:40:33', '2022-07-31 10:40:33'),
(890, 'App\\Models\\User', 3, 'myapptoken', '9d635adc0272dbf9ea074e2f334cc354bd4e4d57c8e3cdc8b565b101ef118fcf', '[\"*\"]', NULL, '2022-07-31 22:00:42', '2022-07-31 22:00:42'),
(891, 'App\\Models\\User', 4, 'myapptoken', '5fbdd6f966da2b86f4a360532587162b4f79813d9a7253474d618e6b8dbfc5a5', '[\"*\"]', NULL, '2022-07-31 22:09:26', '2022-07-31 22:09:26'),
(892, 'App\\Models\\User', 3, 'myapptoken', '57f29ac954501841efe2c76f803c32af1fe780c49f39d4d44a7f8ab56d7a9ece', '[\"*\"]', NULL, '2022-07-31 22:09:36', '2022-07-31 22:09:36'),
(893, 'App\\Models\\User', 7, 'myapptoken', '1a0952f688f7e34a9ba02b0fa910adc5ca8835da0d1b943074984745fca3e6d4', '[\"*\"]', NULL, '2022-07-31 22:10:55', '2022-07-31 22:10:55'),
(894, 'App\\Models\\User', 3, 'myapptoken', '643c4133dcaba8a469b302d9059e3b2c51c3d19599fc9b9a2eeeafb45dd3dc76', '[\"*\"]', NULL, '2022-07-31 22:11:14', '2022-07-31 22:11:14'),
(895, 'App\\Models\\User', 3, 'myapptoken', '8257ca5920830abb198e9351f5f79645c74ed094e025857821d60aa065687a6c', '[\"*\"]', NULL, '2022-07-31 22:12:28', '2022-07-31 22:12:28'),
(896, 'App\\Models\\User', 4, 'myapptoken', '1659aab222d5f8354525126ff7ae53eaa2723abd2c0332ab52798680e37a2c4f', '[\"*\"]', NULL, '2022-07-31 23:21:57', '2022-07-31 23:21:57'),
(897, 'App\\Models\\User', 4, 'myapptoken', '19abfd74dbcb8adf4b662763bad481e316b31391d8e7f24ee64dcef8969ba119', '[\"*\"]', NULL, '2022-07-31 23:52:36', '2022-07-31 23:52:36'),
(898, 'App\\Models\\User', 3, 'myapptoken', 'b79228a66b4958f5122c2adce2792ce4734a86bc58d20cda751601d6b3a9952a', '[\"*\"]', NULL, '2022-07-31 23:56:29', '2022-07-31 23:56:29'),
(899, 'App\\Models\\User', 3, 'myapptoken', 'd26c9fea63bf12ae19ed6668833720350ce643f979cbbbbb9a118adcf682c111', '[\"*\"]', NULL, '2022-07-31 23:56:42', '2022-07-31 23:56:42'),
(900, 'App\\Models\\User', 4, 'myapptoken', 'e155e83525772294012bd4ee8bd08ec8b6c11b937666abaab98be67ae07ec47a', '[\"*\"]', NULL, '2022-07-31 23:59:03', '2022-07-31 23:59:03'),
(901, 'App\\Models\\User', 3, 'myapptoken', 'f818333d03a7ed83c3b2eeee155d723845016b1046498b012c806c3abb93bb7b', '[\"*\"]', NULL, '2022-08-01 00:11:44', '2022-08-01 00:11:44'),
(902, 'App\\Models\\User', 4, 'myapptoken', '288e4b883ec7ca785a5c1676efe64b572460820b37e7d9109de1d68033bed0a8', '[\"*\"]', NULL, '2022-08-01 00:20:13', '2022-08-01 00:20:13'),
(903, 'App\\Models\\User', 3, 'myapptoken', '0ac9e98cc2d3420f4084a20a281d99fbd5aeff68c8653c51a79d2477ccd7dc68', '[\"*\"]', NULL, '2022-08-01 00:23:11', '2022-08-01 00:23:11'),
(904, 'App\\Models\\User', 3, 'myapptoken', 'e9c08f5d86cf33d8d76196abb22b0ce0685e0e77fb26a2354fa37d04947d2921', '[\"*\"]', NULL, '2022-08-01 00:50:23', '2022-08-01 00:50:23'),
(905, 'App\\Models\\User', 7, 'myapptoken', '5ccb7e517f396adb45114db489b8788fb9620bde1c8bab6c01d091940f86ace1', '[\"*\"]', NULL, '2022-08-01 00:52:40', '2022-08-01 00:52:40'),
(906, 'App\\Models\\User', 3, 'myapptoken', '00bed45c38bb78666c7b2e6f2c1a250a0cb0b041e3aa122a9a2f78662944c7b7', '[\"*\"]', NULL, '2022-08-01 00:52:54', '2022-08-01 00:52:54'),
(907, 'App\\Models\\User', 3, 'myapptoken', '2bc81be7f636d7a82fbaa5eda75b62e4407faeae07c9377ba2356f16fa117005', '[\"*\"]', NULL, '2022-08-01 06:05:52', '2022-08-01 06:05:52'),
(908, 'App\\Models\\User', 7, 'myapptoken', 'eaf58126b06611ab34e2311c2e4997b7d55b20a970f949a36e8e71a205565f5e', '[\"*\"]', NULL, '2022-08-01 06:06:27', '2022-08-01 06:06:27'),
(909, 'App\\Models\\User', 3, 'myapptoken', '70da8b3de82feceb703627c214d4b510bc0eb860f650fd85f5be8cac91d8eb07', '[\"*\"]', NULL, '2022-08-01 06:09:23', '2022-08-01 06:09:23'),
(910, 'App\\Models\\User', 4, 'myapptoken', '16bb22701a4c1cd685d6d11ceafa9245b027e8fb572d258bbd2472e90a110416', '[\"*\"]', NULL, '2022-08-01 06:16:10', '2022-08-01 06:16:10'),
(911, 'App\\Models\\User', 3, 'myapptoken', '829c73bdeb34537b06548025a4aa9d8e2711aa7d9be410d1156dd7cbe30cd6ac', '[\"*\"]', NULL, '2022-08-01 06:28:29', '2022-08-01 06:28:29'),
(912, 'App\\Models\\User', 7, 'myapptoken', 'dc663c4dd28ec8936683c8d2e14f3d3313566269817349fe6d0a373c6547cf7a', '[\"*\"]', NULL, '2022-08-01 06:30:01', '2022-08-01 06:30:01'),
(913, 'App\\Models\\User', 4, 'myapptoken', '52902c14c4eb6b24ba7e739044d04d3d4cfd4d03393cbfb3e1a6d52799da6cee', '[\"*\"]', NULL, '2022-08-01 06:30:14', '2022-08-01 06:30:14'),
(914, 'App\\Models\\User', 4, 'myapptoken', '48d4e6f82998e85266c240265a11b512d8f5deb10b1f7afa3a6cec8b4e9e39fc', '[\"*\"]', NULL, '2022-08-01 06:30:23', '2022-08-01 06:30:23'),
(915, 'App\\Models\\User', 4, 'myapptoken', '65396a33633eddf5cccc3dea28a93e105b88917766cdec4713e2a4dc047e6cab', '[\"*\"]', NULL, '2022-08-01 06:31:18', '2022-08-01 06:31:18'),
(916, 'App\\Models\\User', 7, 'myapptoken', '8035b988f84fbaba31abe91da070c487f9ae0835ea2b7110f87dc7c436615d3d', '[\"*\"]', NULL, '2022-08-01 06:44:25', '2022-08-01 06:44:25'),
(917, 'App\\Models\\User', 3, 'myapptoken', '89ba760c01f914f669136ae232c9fae22dd36622cd70138c71e97d78777dc06f', '[\"*\"]', NULL, '2022-08-01 06:55:57', '2022-08-01 06:55:57'),
(918, 'App\\Models\\User', 7, 'myapptoken', '1ec117e96f7189b072492807ba235b634e5446fbade3284bb91501f00b9245b8', '[\"*\"]', NULL, '2022-08-01 06:57:06', '2022-08-01 06:57:06'),
(919, 'App\\Models\\User', 3, 'myapptoken', '4564981c3071c8fcd0539cac169695b9f6731c3669dbcdd9ba216608a8562c63', '[\"*\"]', NULL, '2022-08-01 06:57:45', '2022-08-01 06:57:45'),
(920, 'App\\Models\\User', 7, 'myapptoken', 'e5f326dda5abd84b3af104c3fc2c02941d2f7b8d78bc22ac45e5de0cfccfc4f1', '[\"*\"]', NULL, '2022-08-01 06:58:01', '2022-08-01 06:58:01'),
(921, 'App\\Models\\User', 3, 'myapptoken', '2aa4ec67a245dc23a9689fea004389ff99aba453bb4ab1369f8f04e979ff457b', '[\"*\"]', NULL, '2022-08-01 06:58:36', '2022-08-01 06:58:36'),
(922, 'App\\Models\\User', 4, 'myapptoken', '1eed6c0f8648bf97ba7cd61bb5b35de8b92c87d3eebce9766f8ccf1dea4e58d2', '[\"*\"]', NULL, '2022-08-01 06:59:04', '2022-08-01 06:59:04'),
(923, 'App\\Models\\User', 4, 'myapptoken', 'f08a82318180451bb54457ea76f4ab7b0fb32ec83f2e772e9930c7c0b5e1a396', '[\"*\"]', NULL, '2022-08-01 06:59:48', '2022-08-01 06:59:48'),
(924, 'App\\Models\\User', 4, 'myapptoken', '2eed072da24629e89408171ebc107368b82b0d39a7c62f4d309d85fa991de2a8', '[\"*\"]', NULL, '2022-08-01 07:01:39', '2022-08-01 07:01:39'),
(925, 'App\\Models\\User', 3, 'myapptoken', '10a83d9af90b2f790e3b09bd277bc3b9ce0491f21bf8862cf65888f0dd8a1ef1', '[\"*\"]', NULL, '2022-08-01 07:02:33', '2022-08-01 07:02:33'),
(926, 'App\\Models\\User', 4, 'myapptoken', 'aa9c2111ae6db803454f9585838b7a5a0840fdb6c49fd0c315febbb5a20c3f4a', '[\"*\"]', NULL, '2022-08-01 07:12:48', '2022-08-01 07:12:48'),
(927, 'App\\Models\\User', 3, 'myapptoken', 'e93cc6cd63c74db2da616f953fbbc0c4893d660766171ff2ae6862bdda7f5a24', '[\"*\"]', NULL, '2022-08-01 07:14:02', '2022-08-01 07:14:02'),
(928, 'App\\Models\\User', 7, 'myapptoken', 'ef6d93fc2d176692f76fe3785494a1bc358ad3c1a07e55d981b1e695a17b0751', '[\"*\"]', NULL, '2022-08-01 07:14:32', '2022-08-01 07:14:32'),
(929, 'App\\Models\\User', 7, 'myapptoken', 'd950a51694eeb50939022491aeedb834d60d867502a84993e7687547577e795d', '[\"*\"]', NULL, '2022-08-01 07:14:49', '2022-08-01 07:14:49'),
(930, 'App\\Models\\User', 3, 'myapptoken', '79b1fb533c9bebaf6cfdc94fcc2a08ee8d308c6eaedb6fb528487accddd90859', '[\"*\"]', NULL, '2022-08-01 07:15:32', '2022-08-01 07:15:32'),
(931, 'App\\Models\\User', 7, 'myapptoken', '470995075098389c4f9d8a6c401f6a9a394d1b7a4d6549477d5a9162a552630e', '[\"*\"]', NULL, '2022-08-01 07:16:08', '2022-08-01 07:16:08'),
(932, 'App\\Models\\User', 3, 'myapptoken', 'ab51a4b256a7fe892b05fdb4ea2ae702d7d8b143a89528f6bc32e0f0c568abd4', '[\"*\"]', NULL, '2022-08-01 07:16:34', '2022-08-01 07:16:34'),
(933, 'App\\Models\\User', 5, 'myapptoken', '01433d9516f8d77398c195240febecfcfb658997b6fe42e93b272300790d60f7', '[\"*\"]', NULL, '2022-08-01 07:18:06', '2022-08-01 07:18:06'),
(934, 'App\\Models\\User', 3, 'myapptoken', 'af652604cb618692bdf797bfacaa146e6e886ca3588697f5d68f4956610b6a1e', '[\"*\"]', NULL, '2022-08-01 07:18:58', '2022-08-01 07:18:58'),
(935, 'App\\Models\\User', 3, 'myapptoken', '15afaf4bd59814ab7c5f0a3b303901b7cfb883a8be33817fd633c93c1d4fa435', '[\"*\"]', NULL, '2022-08-01 07:21:06', '2022-08-01 07:21:06'),
(936, 'App\\Models\\User', 3, 'myapptoken', '9d1cb8b9b1cfd6e81778360b0c1011df07b3fa20459a33a57d146bf17bd66b2a', '[\"*\"]', NULL, '2022-08-01 07:23:37', '2022-08-01 07:23:37'),
(937, 'App\\Models\\User', 4, 'myapptoken', 'adfd40c6bd3354d7d8aae62e27656bd48f0b326887493800b5ab58c104058d4c', '[\"*\"]', NULL, '2022-08-01 07:27:02', '2022-08-01 07:27:02'),
(938, 'App\\Models\\User', 3, 'myapptoken', '70993b87929823eb8928ed1b6f92188b01d6c685f64146c239af77b6096419a8', '[\"*\"]', NULL, '2022-08-01 07:30:59', '2022-08-01 07:30:59'),
(939, 'App\\Models\\User', 7, 'myapptoken', '57049af491c1d373e03245483e63b16c2904282263500ff5b65deda79cb31c1f', '[\"*\"]', NULL, '2022-08-01 07:32:17', '2022-08-01 07:32:17'),
(940, 'App\\Models\\User', 5, 'myapptoken', '7319e4caa6d34e0c6453746b6c809b9f51c1c57d123399a010ffa2747151abdb', '[\"*\"]', NULL, '2022-08-01 07:32:35', '2022-08-01 07:32:35'),
(941, 'App\\Models\\User', 3, 'myapptoken', '937a5150e7bfea8ba1250f820b705d139880fb57075109da5bb5f5d42f34982f', '[\"*\"]', NULL, '2022-08-01 07:33:18', '2022-08-01 07:33:18'),
(942, 'App\\Models\\User', 7, 'myapptoken', 'd570fcc034dfd2c97983cd2efae086b75d5b824edec3a4645da58b735fe77f55', '[\"*\"]', NULL, '2022-08-01 07:34:10', '2022-08-01 07:34:10'),
(943, 'App\\Models\\User', 3, 'myapptoken', 'b7858edf83359f1acc85e98464819e7bf04dd813463cfc4e24b28afba23acaaf', '[\"*\"]', NULL, '2022-08-01 07:35:04', '2022-08-01 07:35:04'),
(944, 'App\\Models\\User', 3, 'myapptoken', '384420cb486ae9a61d6d0524b1459b0f59d2f479f68964f6cf65b8a035eed0de', '[\"*\"]', NULL, '2022-08-01 23:02:38', '2022-08-01 23:02:38'),
(945, 'App\\Models\\User', 7, 'myapptoken', '2a23feb728c0b61c37dbd2390b8b14c8e3166a713ed5ac97e0ad7b4f1764878c', '[\"*\"]', NULL, '2022-08-01 23:02:54', '2022-08-01 23:02:54'),
(946, 'App\\Models\\User', 3, 'myapptoken', '282ad95e43cc1c30a817d004a851ef5f0a26235469858705f408a87d9a884665', '[\"*\"]', NULL, '2022-08-01 23:13:28', '2022-08-01 23:13:28'),
(947, 'App\\Models\\User', 4, 'myapptoken', '3d59710d1e84a4eff3283ea08e348f9e952ef070db2b291db18b6ab9c1c68437', '[\"*\"]', NULL, '2022-08-01 23:13:54', '2022-08-01 23:13:54'),
(948, 'App\\Models\\User', 3, 'myapptoken', 'f46258e3462e54b571f46554d340ac6486116f521d4078643cf0bfb1932bb34c', '[\"*\"]', NULL, '2022-08-01 23:15:05', '2022-08-01 23:15:05'),
(949, 'App\\Models\\User', 4, 'myapptoken', '306da0c69fec7c66167920e57a3344612cbe2396594a64e6c3987c4bc658b57a', '[\"*\"]', NULL, '2022-08-02 18:55:11', '2022-08-02 18:55:11'),
(950, 'App\\Models\\User', 3, 'myapptoken', 'a79c5c43d853004d0f6b9dec0688c9c9cf0f12bf425ae5ef16d5466a181ff1fd', '[\"*\"]', NULL, '2022-08-02 18:56:16', '2022-08-02 18:56:16'),
(951, 'App\\Models\\User', 7, 'myapptoken', '90f919e6ddc263a08d7e9ab211fc5ea2871a9f2aa76663781314a6249e136496', '[\"*\"]', NULL, '2022-08-02 18:59:15', '2022-08-02 18:59:15'),
(952, 'App\\Models\\User', 3, 'myapptoken', 'a109d4e122e5a67784229224b911ae7aeb56b1cff8c5e5a0fe8587428ee85788', '[\"*\"]', NULL, '2022-08-02 18:59:34', '2022-08-02 18:59:34'),
(953, 'App\\Models\\User', 7, 'myapptoken', 'aa3e701c434b4fee4086187d6cc647df7a22eae086a2b82e15d7c20e82d56fe8', '[\"*\"]', NULL, '2022-08-02 19:02:04', '2022-08-02 19:02:04'),
(954, 'App\\Models\\User', 4, 'myapptoken', 'd6819682fcb1b1b21ea3fff7667074b8c3952ed46b23c1647dba49e2acaa5639', '[\"*\"]', NULL, '2022-08-02 19:02:50', '2022-08-02 19:02:50'),
(955, 'App\\Models\\User', 3, 'myapptoken', 'ed3d7b829e36db82f458af3e20657421891d226f2e10d47aefc64bc5b5cef639', '[\"*\"]', NULL, '2022-08-02 19:03:10', '2022-08-02 19:03:10'),
(956, 'App\\Models\\User', 4, 'myapptoken', '4873c8ae0da27157d9861b4e894a57bf98743684f2fe2700cbf109fce6e66f78', '[\"*\"]', NULL, '2022-08-02 19:03:29', '2022-08-02 19:03:29'),
(957, 'App\\Models\\User', 3, 'myapptoken', '7db49e2d8974d72114386a9171da36074410559e97a2c57ac6a78421b7b50151', '[\"*\"]', NULL, '2022-08-02 19:05:41', '2022-08-02 19:05:41'),
(958, 'App\\Models\\User', 7, 'myapptoken', 'ad9aed81acf818e65d0e3e91108f09d7ce578af74a871a7e62d572be9dd410b4', '[\"*\"]', NULL, '2022-08-02 19:13:27', '2022-08-02 19:13:27'),
(959, 'App\\Models\\User', 3, 'myapptoken', '30c37184f143dd860fe007ba8d51c384749966c846113eeadbc9ba1eaceb78ad', '[\"*\"]', NULL, '2022-08-05 02:49:43', '2022-08-05 02:49:43'),
(960, 'App\\Models\\User', 4, 'myapptoken', '0793b007f7f8af127d4674d249ad6002ebe90f32fd2d2c4164ed2be3f7660006', '[\"*\"]', NULL, '2022-08-05 02:52:33', '2022-08-05 02:52:33'),
(961, 'App\\Models\\User', 7, 'myapptoken', '8cdfae09c858a26738997d2557fe5d8fc986dd14c7bd2e266ad72b40ad37d02b', '[\"*\"]', NULL, '2022-08-05 03:07:17', '2022-08-05 03:07:17'),
(962, 'App\\Models\\User', 3, 'myapptoken', 'f34a92f56437761b8d1f7c47ec373b441412bab087e2bfd124e2ac6f52af225e', '[\"*\"]', NULL, '2022-08-05 03:07:43', '2022-08-05 03:07:43'),
(963, 'App\\Models\\User', 7, 'myapptoken', '998012df81a5c4e5293985854d2f3be4f89f8db99c533e5bfd2a453f3e1984d5', '[\"*\"]', NULL, '2022-08-05 03:11:26', '2022-08-05 03:11:26'),
(964, 'App\\Models\\User', 4, 'myapptoken', '2f5722cf4abfb2aa9783f06ffa4661131fa72b45c59cff0185ddc3608a349e87', '[\"*\"]', NULL, '2022-08-05 03:13:46', '2022-08-05 03:13:46'),
(965, 'App\\Models\\User', 3, 'myapptoken', 'd4f33799054cb707afa6d1a7e16a20a29043a3baf52a856f50fdd5a4d880c07e', '[\"*\"]', NULL, '2022-08-05 03:19:54', '2022-08-05 03:19:54'),
(966, 'App\\Models\\User', 4, 'myapptoken', '702d620fce4af2e1811b67ac772977af817e184bcfda0f054739a2ccb3f70f0e', '[\"*\"]', NULL, '2022-08-05 03:20:05', '2022-08-05 03:20:05'),
(967, 'App\\Models\\User', 3, 'myapptoken', '49382fe0e4e5841a75517a259f3734c06d9e02e700e426d901d82e7192e5bfc3', '[\"*\"]', NULL, '2022-08-05 03:20:58', '2022-08-05 03:20:58'),
(968, 'App\\Models\\User', 4, 'myapptoken', 'd40b3f1a45a0beb4c2c76feb4d3dae4f487fa665b1591ab0b6a02edbd1f260d5', '[\"*\"]', NULL, '2022-08-05 03:21:07', '2022-08-05 03:21:07'),
(969, 'App\\Models\\User', 4, 'myapptoken', '4967c44f6a84c59beec715c1615b6b8b946dbd44b548985e4acbc83f141ede0c', '[\"*\"]', NULL, '2022-08-05 03:23:03', '2022-08-05 03:23:03'),
(970, 'App\\Models\\User', 4, 'myapptoken', 'b48955dad2f8f5c942f7ac04ce28733ee57ff84a3f26acaa3d039ddddc1e4c97', '[\"*\"]', NULL, '2022-08-05 03:23:23', '2022-08-05 03:23:23'),
(971, 'App\\Models\\User', 4, 'myapptoken', '93e7a2da8db2023990004d192807541f091fb829331a86c60e4d41c9462e50c8', '[\"*\"]', NULL, '2022-08-05 03:24:18', '2022-08-05 03:24:18'),
(972, 'App\\Models\\User', 4, 'myapptoken', '87abbb64f5a94723d370d17434b91e1a1821bf75a42930dca466ac3e89721867', '[\"*\"]', NULL, '2022-08-05 03:29:11', '2022-08-05 03:29:11'),
(973, 'App\\Models\\User', 3, 'myapptoken', 'b36be2730af9855ef06124224c250e8936a5a5de85805a2cc15e663a6ad7374f', '[\"*\"]', NULL, '2022-08-05 03:29:21', '2022-08-05 03:29:21'),
(974, 'App\\Models\\User', 4, 'myapptoken', 'ea8786004ac181db5fc41dc6b1987402b967b5094189181cefdcec9ffde856af', '[\"*\"]', NULL, '2022-08-05 03:29:31', '2022-08-05 03:29:31'),
(975, 'App\\Models\\User', 4, 'myapptoken', '80d91809ebc2426fc565f3429160b5a50e402505b5b90187b3334b98fbf24873', '[\"*\"]', NULL, '2022-08-05 03:29:37', '2022-08-05 03:29:37'),
(976, 'App\\Models\\User', 7, 'myapptoken', 'fc36b6970c2833527c61221c516b59ba541b06d411f232d74cb230ca6d295d1f', '[\"*\"]', NULL, '2022-08-05 03:30:24', '2022-08-05 03:30:24'),
(977, 'App\\Models\\User', 7, 'myapptoken', '75a6f36b0f403f0684bd8d6465cf161a68358f4c56ef0e8a0cb6f22e0770af34', '[\"*\"]', NULL, '2022-08-05 03:30:44', '2022-08-05 03:30:44'),
(978, 'App\\Models\\User', 4, 'myapptoken', '1c554bc2eec7481be4ea29e2d10b86187f3d5ba8a2ba0fe7e804adef1a820d15', '[\"*\"]', NULL, '2022-08-05 03:31:05', '2022-08-05 03:31:05'),
(979, 'App\\Models\\User', 3, 'myapptoken', '64a76d1997b2f54775859d6ec382146b74193007e88b6e86168ce8cd54ed2faf', '[\"*\"]', NULL, '2022-08-05 03:31:20', '2022-08-05 03:31:20'),
(980, 'App\\Models\\User', 4, 'myapptoken', 'e483ffab3943ca8110d89393476f47df38236ce19e0227e18c0d03f4028459de', '[\"*\"]', NULL, '2022-08-05 03:31:26', '2022-08-05 03:31:26'),
(981, 'App\\Models\\User', 4, 'myapptoken', '013c2b64f2c1b4f2265b4dd4ff0fabc5dd036e6c63092748f99ec81c19a4d477', '[\"*\"]', NULL, '2022-08-05 03:31:33', '2022-08-05 03:31:33'),
(982, 'App\\Models\\User', 3, 'myapptoken', '6071dbf4260a9962f0834833c5d819bb6feadc3358ba0bb7be3c0d58d6ab79d4', '[\"*\"]', NULL, '2022-08-05 03:32:06', '2022-08-05 03:32:06'),
(983, 'App\\Models\\User', 3, 'myapptoken', 'a7adbc83c76da58321f53751a634652d972b3b2856985b4db5324a317e87fae3', '[\"*\"]', NULL, '2022-08-05 03:33:49', '2022-08-05 03:33:49'),
(984, 'App\\Models\\User', 7, 'myapptoken', 'fa45d4a60a408193affb516bba363c9d0b70452dfdef7e44ef53e16d77a1b5a9', '[\"*\"]', NULL, '2022-08-05 03:36:48', '2022-08-05 03:36:48'),
(985, 'App\\Models\\User', 3, 'myapptoken', 'b1b5b24f3bd8e91e0c065922ca0c69e178b0070434f588f4be6f9ee2f736b063', '[\"*\"]', NULL, '2022-08-07 02:27:43', '2022-08-07 02:27:43'),
(986, 'App\\Models\\User', 4, 'myapptoken', 'df70ea4cf122bda8e1059d47f9e0bc7c2af42da177e6404977ff6b8eddae22a8', '[\"*\"]', NULL, '2022-08-07 02:29:16', '2022-08-07 02:29:16'),
(987, 'App\\Models\\User', 3, 'myapptoken', 'da960e147337a099214032572e1f8e47df8f8c6a0da7f7bff0d619b6680f8098', '[\"*\"]', NULL, '2022-08-07 02:32:29', '2022-08-07 02:32:29'),
(988, 'App\\Models\\User', 3, 'myapptoken', '59aa121e31684c7f9d2990d87d7810e0ab2c79482b0a870669105df3e8bd58b3', '[\"*\"]', NULL, '2022-08-07 03:22:40', '2022-08-07 03:22:40'),
(989, 'App\\Models\\User', 4, 'myapptoken', '7a546a7945d9aa8dd355730c83abd57046352f897c79ccbb24ee20bc5fb4f071', '[\"*\"]', NULL, '2022-08-07 03:28:47', '2022-08-07 03:28:47'),
(990, 'App\\Models\\User', 7, 'myapptoken', '492788ddf7affdfdd5c93b4e0c7e3d00b7af2a92519b808ad2f3c55700bec55c', '[\"*\"]', NULL, '2022-08-07 03:30:11', '2022-08-07 03:30:11'),
(991, 'App\\Models\\User', 3, 'myapptoken', 'dbcd11ca984e4254f8acd93b092a2bde6b3e17a23628d2a8fe726b3950d58eea', '[\"*\"]', NULL, '2022-08-08 23:28:28', '2022-08-08 23:28:28'),
(992, 'App\\Models\\User', 4, 'myapptoken', '71ac3728a93b9f6027b078fbb5cdf79c945d623bfcf8ad98c1187e0a8c2d87d7', '[\"*\"]', NULL, '2022-08-08 23:30:01', '2022-08-08 23:30:01'),
(993, 'App\\Models\\User', 4, 'myapptoken', 'ae7ef00782064178c010311d77dbd2cb9b6e5665e6dcc9a4ade4f3c5ad60cc66', '[\"*\"]', NULL, '2022-08-08 23:30:13', '2022-08-08 23:30:13'),
(994, 'App\\Models\\User', 3, 'myapptoken', '137ed1d4871dc23d57aa3712679de7ae69aaad6d333b85e1c8dd4587d71d7462', '[\"*\"]', NULL, '2022-08-08 23:30:21', '2022-08-08 23:30:21'),
(995, 'App\\Models\\User', 3, 'myapptoken', 'cbd0ec07e376b316de4861dc5035b5cb5acc2232a859a51a15ed1af361fcaab8', '[\"*\"]', NULL, '2022-08-08 23:57:10', '2022-08-08 23:57:10'),
(996, 'App\\Models\\User', 4, 'myapptoken', '37105f6ca0a0862f3bd4f4d3081995a478bf3547c60b18c12b30d1179269a0d8', '[\"*\"]', NULL, '2022-08-08 23:59:46', '2022-08-08 23:59:46'),
(997, 'App\\Models\\User', 5, 'myapptoken', 'e28b83e89d99484a676d4cea3df5b4dcf28d29680be6fc0cfc974badbeee8257', '[\"*\"]', NULL, '2022-08-09 00:01:13', '2022-08-09 00:01:13'),
(998, 'App\\Models\\User', 5, 'myapptoken', '1c71af77878a3fd2bc82e249b3952e90d5d394469bc1b1f8853d3b251ed35bcb', '[\"*\"]', NULL, '2022-08-09 00:02:48', '2022-08-09 00:02:48'),
(999, 'App\\Models\\User', 7, 'myapptoken', '7475465e5d77cb5e6e400d45f08ddd87847534ffdd6168c0d88025cad03ccab8', '[\"*\"]', NULL, '2022-08-09 00:02:57', '2022-08-09 00:02:57'),
(1000, 'App\\Models\\User', 3, 'myapptoken', '8b6fd159b3fcd01df39eb5fa005ed58ced85e43c558a43d61222dfaefd4d1a1d', '[\"*\"]', NULL, '2022-08-09 00:03:10', '2022-08-09 00:03:10'),
(1001, 'App\\Models\\User', 3, 'myapptoken', '4e1a3b482cee1522e71c6f893cbb8a5bcded8ee324e94d6c8bc22f069962b33d', '[\"*\"]', NULL, '2022-08-09 18:31:37', '2022-08-09 18:31:37'),
(1002, 'App\\Models\\User', 4, 'myapptoken', '3183d24bcdaed2f26bc6383e4eeb2f0f4c81fb6bc5660ef3f6ea2a39aedcfaed', '[\"*\"]', NULL, '2022-08-09 18:33:46', '2022-08-09 18:33:46'),
(1003, 'App\\Models\\User', 3, 'myapptoken', '6228eb90909a0384c955ae1dab74ad215d6b00874f56aa968892c6729f1ed776', '[\"*\"]', NULL, '2022-08-09 18:34:01', '2022-08-09 18:34:01'),
(1004, 'App\\Models\\User', 4, 'myapptoken', '001a045f62321757f59fc4aa35e2dcbd0df8a5ce28fb41d4e4be1ce4ce29b6e3', '[\"*\"]', NULL, '2022-08-09 18:34:32', '2022-08-09 18:34:32'),
(1005, 'App\\Models\\User', 3, 'myapptoken', 'ba087aeb50e43907d3f5bb95d05b34951986643efc8acc5d3e05e9c92f6f345e', '[\"*\"]', NULL, '2022-08-09 18:35:56', '2022-08-09 18:35:56'),
(1006, 'App\\Models\\User', 7, 'myapptoken', 'f76eaf669ded4c3bb315819738efce9fbab734c614c51e2538b0885ef68eb423', '[\"*\"]', NULL, '2022-08-09 18:37:06', '2022-08-09 18:37:06'),
(1007, 'App\\Models\\User', 3, 'myapptoken', '160ba677ee65e8d8c176e919abfe489298eba5cc381f80f790ba2b932b09846d', '[\"*\"]', NULL, '2022-08-09 18:37:42', '2022-08-09 18:37:42'),
(1008, 'App\\Models\\User', 3, 'myapptoken', 'aaece94981b13ff51148c9020b7765be890d30c12f3e350ae77f841cc4beb7ae', '[\"*\"]', NULL, '2022-08-09 20:37:25', '2022-08-09 20:37:25'),
(1009, 'App\\Models\\User', 4, 'myapptoken', 'a3780a3ba9893e5641bcc0bb97d26cb5c219b726c5b57582ddb4e1eef53ee5cf', '[\"*\"]', NULL, '2022-08-09 20:45:08', '2022-08-09 20:45:08'),
(1010, 'App\\Models\\User', 4, 'myapptoken', '6254692eb8c010098a313acaeabaa443e228df53cf6e8c028ee4cd0b0c73e3e3', '[\"*\"]', NULL, '2022-08-09 20:45:34', '2022-08-09 20:45:34'),
(1011, 'App\\Models\\User', 3, 'myapptoken', 'b76a8f09469cfa12ba2ed001d14c432b7cafd200e9c600b01f613025a8cea771', '[\"*\"]', NULL, '2022-08-09 20:45:41', '2022-08-09 20:45:41'),
(1012, 'App\\Models\\User', 4, 'myapptoken', 'f695cd5038eec7780d54f0946bc1beef89dfc888c8930064a71166cbc8852683', '[\"*\"]', NULL, '2022-08-09 20:57:01', '2022-08-09 20:57:01'),
(1013, 'App\\Models\\User', 3, 'myapptoken', 'df4a8ac3bebda70340fcf7dd8609359564ed5d6be65857d8297b9b7ccc73277b', '[\"*\"]', NULL, '2022-08-09 20:57:42', '2022-08-09 20:57:42'),
(1014, 'App\\Models\\User', 4, 'myapptoken', '5c5ceda14d9f6f200422568821c615b978def29432c16ce8cb0dd52720b7d9f7', '[\"*\"]', NULL, '2022-08-09 22:26:10', '2022-08-09 22:26:10'),
(1015, 'App\\Models\\User', 3, 'myapptoken', '23c576b77e162464949ebae8930086ff144c8fc4d2be5273a53808e08a2cc24a', '[\"*\"]', NULL, '2022-08-09 22:27:12', '2022-08-09 22:27:12'),
(1016, 'App\\Models\\User', 7, 'myapptoken', 'af8d8607b9e6ce79cea72a36bdee63873a28a39f64e6dced651bcb8658aa51e4', '[\"*\"]', NULL, '2022-08-09 22:28:11', '2022-08-09 22:28:11'),
(1017, 'App\\Models\\User', 3, 'myapptoken', 'bc561f5c8da33147708b4338e771c9c9b453b1f8d0b8f212e517ca4f3e80a341', '[\"*\"]', NULL, '2022-08-09 22:28:43', '2022-08-09 22:28:43'),
(1018, 'App\\Models\\User', 3, 'myapptoken', '60933789677d0c21e2cd7c98d3ddfad71c4c517ed87f9d06af2668d3e30018ce', '[\"*\"]', NULL, '2022-08-12 22:37:16', '2022-08-12 22:37:16'),
(1019, 'App\\Models\\User', 4, 'myapptoken', '66ba6c1dc7f3cb0c8aaf8fcf7cc95bd9a59a06334dcc4fc52e98723ca6fe742e', '[\"*\"]', NULL, '2022-08-12 22:37:27', '2022-08-12 22:37:27'),
(1020, 'App\\Models\\User', 4, 'myapptoken', '133a102d976c074033591e8a828b075b2c52e42c932b7b594bbcb4ff7fbceca1', '[\"*\"]', NULL, '2022-08-12 22:39:25', '2022-08-12 22:39:25'),
(1021, 'App\\Models\\User', 3, 'myapptoken', 'f4eacea69c3219e14c08f523abfb810e3f7a9ca7a982884aeaf6a91b030bce48', '[\"*\"]', NULL, '2022-08-12 22:39:48', '2022-08-12 22:39:48'),
(1022, 'App\\Models\\User', 7, 'myapptoken', '70c9d5db303da87699e6405205cd97fbad75b69d14dce75e210e7d30a623b19b', '[\"*\"]', NULL, '2022-08-12 22:40:50', '2022-08-12 22:40:50');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'staff', 'web', '2021-07-04 21:19:53', '2021-07-04 21:19:53'),
(2, 'admin', 'web', '2021-07-04 21:20:03', '2021-07-04 21:20:03'),
(3, 'Regional Director', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(4, 'Document Controller', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(5, 'Division Head', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(6, 'Section Head', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16'),
(7, 'Ordinary User Role', 'web', '2022-07-22 01:49:16', '2022-07-22 01:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(4, 3),
(5, 3),
(6, 3),
(7, 4),
(8, 3),
(8, 4),
(8, 5),
(8, 6),
(8, 7),
(9, 3),
(9, 4),
(10, 4),
(11, 5),
(11, 6),
(11, 7);

-- --------------------------------------------------------

--
-- Table structure for table `route_to_tables`
--

CREATE TABLE `route_to_tables` (
  `route_to_id` int(10) UNSIGNED NOT NULL,
  `route_doc_id` int(10) UNSIGNED NOT NULL,
  `route_to_name` varchar(100) NOT NULL,
  `route_to_message` text DEFAULT NULL,
  `id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `receive` varchar(10) NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `routing_details`
--

CREATE TABLE `routing_details` (
  `route_id` int(10) UNSIGNED NOT NULL,
  `route_slip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_from` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route_thru` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route_to` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT 'None',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isAdmin` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `usergroup` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `phone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `picture` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fname`, `lname`, `email`, `password`, `isAdmin`, `usergroup`, `remember_token`, `created_at`, `updated_at`, `birthday`, `phone`, `picture`) VALUES
(52, 'Andrea', 'Remoreras', 'regional@director', '$2y$10$Iz8ru9PvRQX7G2odB5rQQOiUPxL3H5EGhzkK7nGoXFfFY5P8IYr9W', '0', 'Regional Director', NULL, '2022-07-16 02:00:23', '2022-07-21 04:55:13', '2022-07-09', NULL, 'images.jpg'),
(53, 'Document', 'Controller', 'document@controller', '$2y$10$1QGudchnxSEtvKduenrhWerh.qCxvhcrdcgUiX7d8KHWh7ThOnT5a', '0', 'Document Controller', NULL, '2022-07-16 03:51:47', '2022-07-18 04:48:30', '2022-07-09', '123123', 'IMG_20220606_192913.jpg'),
(55, 'Remi', 'Vallejo', 'ordinary@user', '$2y$10$Ul8ffP3JdrOUQsShyhouo.IfAYJ4TFlzKRiHw9YnvwhBOg9Wfq.ee', '0', 'Ordinary User', NULL, '2022-07-17 00:47:08', '2022-07-19 22:24:53', '2022-07-09', '09128332', 'IMG_20220606_192913.jpg'),
(56, 'document2', 'controller', 'document@controller2', '$2y$10$TyILaK/6.IV2c6q4DvDV..tiqrLGyYiT/hHpanSiIzJEr1zc.L5CG', '0', 'Document Controller', NULL, '2022-07-17 01:02:48', '2022-07-17 01:04:33', '2022-07-08', '123213', NULL),
(57, 'Ma. Hannah', 'Budiongan', 'hannah@budiongan', '$2y$10$4KWfqhH/tfBDN6RKoak1XONrBk2UEIHIygB4hPsQP3IpG1ehG27ZK', '0', 'Section Head', NULL, '2022-07-18 02:36:18', '2022-07-18 02:36:18', NULL, NULL, NULL),
(58, 'Andrea', 'Remoreras', 'andrea@remoreras', '$2y$10$.bzavWLrWIWdv6GW0MB4c.7RYCmwcfomCRlkam5Fu21KBpssnJRYm', '0', 'Ordinary User', NULL, '2022-07-18 05:48:09', '2022-07-22 08:44:24', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attachments_lists`
--
ALTER TABLE `attachments_lists`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_doc_id` (`doc_id`);

--
-- Indexes for table `document_details`
--
ALTER TABLE `document_details`
  ADD PRIMARY KEY (`doc_id`),
  ADD KEY `fk_route_id` (`route_id`),
  ADD KEY `fk_users_id` (`users_id`);

--
-- Indexes for table `document_types`
--
ALTER TABLE `document_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_guard_name_unique` (`name`,`guard_name`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `route_to_tables`
--
ALTER TABLE `route_to_tables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `route_to_tables_ibfk_1` (`route_doc_id`),
  ADD KEY `route_to_tables_ibfk_2` (`route_to_id`);

--
-- Indexes for table `routing_details`
--
ALTER TABLE `routing_details`
  ADD PRIMARY KEY (`route_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `attachments_lists`
--
ALTER TABLE `attachments_lists`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=208;

--
-- AUTO_INCREMENT for table `document_details`
--
ALTER TABLE `document_details`
  MODIFY `doc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;

--
-- AUTO_INCREMENT for table `document_types`
--
ALTER TABLE `document_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1023;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `route_to_tables`
--
ALTER TABLE `route_to_tables`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `routing_details`
--
ALTER TABLE `routing_details`
  MODIFY `route_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attachments_lists`
--
ALTER TABLE `attachments_lists`
  ADD CONSTRAINT `fk_doc_id` FOREIGN KEY (`doc_id`) REFERENCES `document_details` (`doc_id`);

--
-- Constraints for table `document_details`
--
ALTER TABLE `document_details`
  ADD CONSTRAINT `fk_route_id` FOREIGN KEY (`route_id`) REFERENCES `routing_details` (`route_id`),
  ADD CONSTRAINT `fk_users_id` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `route_to_tables`
--
ALTER TABLE `route_to_tables`
  ADD CONSTRAINT `route_to_tables_ibfk_1` FOREIGN KEY (`route_doc_id`) REFERENCES `document_details` (`doc_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `route_to_tables_ibfk_2` FOREIGN KEY (`route_to_id`) REFERENCES `routing_details` (`route_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
