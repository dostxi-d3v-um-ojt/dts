<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Regional director permissions

        Permission::create(['name' => 'create-users']);
        Permission::create(['name' => 'edit-users']);
        Permission::create(['name' => 'delete-users']);


        //document controller permissions
        Permission::create(['name' => 'create-incoming-documents']);
        Permission::create(['name' => 'view-incoming-documents']); //<- head permissions and ordinary users
        Permission::create(['name' => 'edit-incoming-documents']);
        Permission::create(['name' => 'delete-incoming-documents']);


        //head permissions and ordinary users
        Permission::create(['name' => 'receive-incoming-documents']);


        $RegionalDirectorRole = Role::create(['name' => 'Regional Director']);
        $DocumentControllerRole = Role::create(['name' => 'Document Controller']);
        $DivisionHeadRole = Role::create(['name' => 'Division Head']);
        $SectionHeadRole = Role::create(['name' => 'Section Head']);
        $OrdinaryUserRole = Role::create(['name' => 'Ordinary User Role']);


        $RegionalDirectorRole->givePermissionTo([
            'create-users',
            'edit-users',
            'delete-users',

            'edit-incoming-documents',
            'view-incoming-documents'
        ]);

        $DocumentControllerRole->givePermissionTo([
            'create-incoming-documents',
            'edit-incoming-documents',
            'view-incoming-documents',
            'delete-incoming-documents'
        ]);

        $DivisionHeadRole->givePermissionTo([
            'receive-incoming-documents',
            'view-incoming-documents'
        ]);

        $SectionHeadRole->givePermissionTo([
            'receive-incoming-documents',
            'view-incoming-documents'
        ]);

        $OrdinaryUserRole->givePermissionTo([
            'receive-incoming-documents',
            'view-incoming-documents'
        ]);
    }
}
