// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Pie Chart Example
var ctx = document.getElementById("myPieChart");
var myPieChart = new Chart(ctx, {
  type: 'pie',
  data: {
    labels: ["Received", "Unreceived"],
    datasets: [{
      data: _xpie,
      backgroundColor: ['#007bff', '#dc3545'],
    }],
  },
   options: {
    maintainAspectRatio: true,
    responsive: true,
    tooltips: {
      callbacks: {
        label: (tooltipItem, data) => {
          var value = data.datasets[0].data[tooltipItem.index];
          var total = data.datasets[0].data.reduce((a, b) => a + b, 0);
          var pct = 100 / total * value;
          var pctRounded = Math.round(pct * 10) / 10;
          return value + ' (' + pctRounded + '%)';
        }
      }
    }
  }
});




