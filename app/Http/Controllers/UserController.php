<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Access\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{

    // ------------------------------- User Management Controls -------------------------------------------------

    public function showProfile()
    {
        $id = auth()->user()->id;
        $user_info = DB::table('users')
            ->select('users.*')
            ->where('id', $id)
            ->get();

        return view('user.profile', compact('user_info'));
    }
    public function showEdit($id)
    {
        $user_info = DB::table('users')
            ->select('users.*')
            ->where('id', $id)
            ->get();

        return view('user.profile', compact('user_info'));
    }
    public function try()
    {
        $ordinary = DB::table('users')
            ->select('users.fname', 'users.lname')
            ->where('usergroup', '=', 'Ordinary User')
            ->get();
        $heads = DB::table('users')
            ->select('users.fname', 'users.lname', 'usergroup')
            ->where('usergroup', '<>', 'Ordinary User')
            ->where('usergroup', '<>', 'Document Controller')
            ->where('usergroup', '<>', 'Regional Director')
            ->get();
        $actions = DB::table('actions')
            ->get();

        $doc_types = DB::table('document_types')
            ->get();
        $document = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->where('document_details.doc_id', '182')
            ->get();


        return view('try', compact('document', 'ordinary', 'heads', 'actions', 'doc_types'));
    }
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $formfields = $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'required'
        ]);

        $formfields['birthday'] = $request->birthday;
        $formfields['phone'] = $request->phone;

        if ($request->hasFile('picture')) {
            $formfields['picture'] = $request->file('picture')->getClientOriginalName();
            $request->file('picture')->move(public_path("/uploads"), $request->file('picture')->getClientOriginalName());
        }
        $user->update($formfields);

        return redirect('user/myprofile/' . $id);
    }

    public function pendingDocumentsOrdinary()
    {
        $document = DB::table('document_details')
            ->join(
                'routing_details',
                'document_details.route_id',
                '=',
                'routing_details.route_id'
            )
            ->where('routing_details.route_to', '=', auth()->user()->fname . " " . auth()->user()->lname)
            ->get();

        return view('user.document', compact('document'));
    }
    public function ret(Request $request, $action, $doc_id, $route_id)
    {
        $document = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->where('document_details.doc_id', $doc_id)
            ->get();

        if ($action == "edit") {
            return View("edit", compact('document'));
        } else if ($action == "delete") {
            DB::delete('delete from attachments_lists where doc_id = ?', [$doc_id]);
            DB::delete('delete from document_details where doc_id = ?', [$doc_id]);
            DB::delete('delete from routing_details where route_id = ?', [$route_id]);
            return redirect('user/mydocs');
        } else if ($action == "retrieve") {
            return View("recordview", compact('document'));
        } else {
            return View("view", compact('document'));
        }
    }
}
