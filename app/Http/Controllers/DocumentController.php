<?php

namespace App\Http\Controllers;

use to;
use Carbon\Carbon;
use App\Models\User;
use App\Traits\Module;
use Illuminate\Http\Request;
use App\Models\route_to_table;
use App\Models\Routing_Details;
use App\Models\Document_Details;
use App\Models\route_thru_table;
use App\Models\Attachments_Lists;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class DocumentController extends Controller
{
    use Module;

    //  --------------------------- Document Management Control -------------------------------------------------------
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */




    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    public function index()
    {
        $documents = DB::table('document_details')
            ->select('document_details.*')
            ->get();
        return view("display", compact('documents'));
    }
    public function adminDashboard()
    {
        $id = auth()->user()->id;

        $ddata = DB::table('document_details')
            ->select('doc_id', 'created_at')->get()->groupBy(function ($data) {
                return Carbon::parse($data->created_at)->format('d M');
            });
        $dmonths = [];
        $dmonthCount = [];

        foreach ($ddata as $dmonth => $values) {
            $dmonths[] = $dmonth;
            $dmonthCount[] = count($values);
        }

        $data = DB::table('document_details')
            ->select('doc_id', 'created_at')->get()->groupBy(function ($data) {
                return Carbon::parse($data->created_at)->format('M');
            });
        $months = [];
        $monthCount = [];

        foreach ($data as $month => $values) {
            $months[] = $month;
            $monthCount[] = count($values);
        }




        $user = DB::table('users')
            ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.name as role')
            ->where('users.id', '!=', $id)
            ->get();

        $permission = DB::table('users')
            ->join('model_has_permissions', 'users.id', '=', 'model_has_permissions.model_id')
            ->join('permissions', 'model_has_permissions.permission_id', '=', 'permissions.id')
            ->select('users.*', 'permissions.name as permission')
            ->where('users.id', '!=', $id)
            ->get();
        $documents = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->select('document_details.*')
            ->whereNull('route_thru')
            ->get();

        $routedocuments = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->select('document_details.*')
            ->whereNotNull('route_thru')
            ->get();

        $comments = DB::table('route_to_tables')
            ->join('document_details', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where('route_to_tables.route_to_message', '!=', NULL)
            ->get();

        $received = DB::table('document_details')
            ->join('route_to_tables', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where('route_to_tables.receive', '=', 'yes')
            ->get();

        $unreceived = DB::table('document_details')
            ->join('route_to_tables', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where('route_to_tables.receive', '=', 'no')
            ->get();

        $_xpie = [count($received), count($unreceived)];

        return view('admin.dashboard', ['data' => $data, 'months' => $months, 'monthCount' => $monthCount, 'dmonths' => $dmonths, 'dmonthCount' => $dmonthCount, '_xpie' => $_xpie],  compact('user', 'permission', 'documents', 'routedocuments', 'comments'));
    }


    public function userDashboard()
    {

        $id = auth()->user()->id;
        $name = '%' . auth()->user()->fname . " " . auth()->user()->lname . '%';

        $document = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->where('document_details.users_id', $id)
            ->orWhere('routing_details.route_thru', 'like', $name)
            ->orWhere('routing_details.route_to', 'like', $name)
            ->get();

        $user = DB::table('users')
            ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.name as role')
            ->get();

        $pending = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->whereNull('route_to')
            ->where('document_details.users_id', '=', $id)
            ->get();

        $pendings = DB::table('route_to_tables')
            ->select('route_to_tables.*')
            ->where([['route_to_tables.route_to_name', 'like', '%' . $name . '%'], ['route_to_tables.receive', '!=', 'yes']])
            ->get();

        return view('user.dashboard', compact('user', 'document', 'pending', 'pendings'));
    }
    public function pendingDocuments()
    {
        $document = DB::table('route_to_tables')
            ->join('document_details', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where([['route_to_tables.route_to_name', 'like', '%' . auth()->user()->fname . " " . auth()->user()->lname . '%'], ['route_to_tables.receive', '!=', 'yes']])
            ->get();

        return view('user.document', compact('document'));
    }
    public function pendingIncomingDocuments()
    {
        $id = auth()->user()->id;

        $document = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->whereNull('route_to')
            ->where('document_details.users_id', '=', $id)
            ->get();
        return view('user.document', compact('document'));
    }

    public function receivedDocuments()
    {
        $document = DB::table('route_to_tables')
            ->join('document_details', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where([['route_to_tables.route_to_name', 'like', '%' . auth()->user()->fname . " " . auth()->user()->lname . '%'], ['route_to_tables.receive', '=', 'yes']])
            ->get();

        return view('user.document', compact('document'));
    }

    public function displayunrouted()
    {
        $documents = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->select('document_details.*', 'routing_details.*')
            ->whereNull('route_thru')
            ->get();
        return view("display", compact('documents'));
    }
    public function displaycomments()
    {
        $documents = DB::table('document_details')
            ->join('route_to_tables', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where('route_to_tables.route_to_message', '!=', NULL)
            //     ->select('document_details.updated_at', 'route_to_tables.route_to_message')
            ->orderBy('route_to_tables.updated_at', 'desc')
            ->get();

        //dd($documents);

        return view("comments", compact('documents'));
    }
    public function displaydocumentreceive()
    {
        $documents = DB::table('document_details')
            ->join('route_to_tables', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where('route_to_tables.receive', '=', 'yes')
            ->orderBy('route_to_tables.updated_at', 'desc')
            ->get();

        return view("received", compact('documents'));
    }
    public function displaydocumentunreceive()
    {
        $documents = DB::table('document_details')
            ->join('route_to_tables', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
            ->where('route_to_tables.receive', '=', 'no')
            ->orderBy('route_to_tables.updated_at', 'desc')
            ->get();

        return view("received", compact('documents'));
    }
    public function showDocuments()
    {
        $id = auth()->user()->id;
        $user = User::find($id);

        $username = $user->fname . ' ' . $user->lname;
        $document = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->where('document_details.users_id', $id)
            ->get();

        return view('user.document', compact('document'));
    }

    public function displayrouted()
    {
        $documents = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->select('document_details.*', 'routing_details.*')
            ->whereNotNull('route_thru')
            ->get();
        //dd($documents);


        return view("display", compact('documents'));
    }


    public function newDocView()
    {
        $doc_types = DB::table('document_types')
            ->get();

        return view('document', compact('doc_types'));
    }

    public function recordsData($did)
    {
        $results = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->select('document_details.*', 'routing_details.routing_slip', 'routing_details.route_thru')
            ->where('doc_id', $did)
            ->get();
        dd($results);
    }

    public function view(Request $request, $action, $doc_id, $route_id)
    {
        $document = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->where('document_details.doc_id', $doc_id)
            ->get();


        if ($action == "edit") {
            $ordinary = DB::table('users')
                ->select('users.fname', 'users.lname')
                ->where('usergroup', '=', 'Ordinary User')
                // ->orWhere('usergroup', '=', 'Document Controller')
                ->get();
            $heads = DB::table('users')
                ->select('users.fname', 'users.lname', 'usergroup')
                ->where('usergroup', '<>', 'Ordinary User')
                ->where('usergroup', '<>', 'Document Controller')
                ->where('usergroup', '<>', 'Regional Director')
                ->get();
            $actions = DB::table('actions')
                ->get();

            $doc_types = DB::table('document_types')
                ->get();

            return View("edit", compact('document', 'ordinary', 'heads', 'actions', 'doc_types'));
        } else if ($action == "delete") {
            DB::delete('delete from attachments_lists where doc_id = ?', [$doc_id]);
            DB::delete('delete from document_details where doc_id = ?', [$doc_id]);
            DB::delete('delete from routing_details where route_id = ?', [$route_id]);

            $id = Auth::user()->id;
            $userinfo = User::where('users.id', '=', $id)
                ->join('model_has_roles', 'model_id', '=', 'users.id')
                ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                ->first();

            $role = $userinfo->name;

            if ($role == "Regional Director") {
                return redirect('admin/display');
            } else {
                return redirect('user/mydocs');
            }
        } else if ($action == "retrieve") {
            $name = auth()->user()->fname . " " . auth()->user()->lname;
            $comments = DB::table('route_to_tables')
                ->join('document_details', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
                ->where([['route_to_tables.route_to_name', 'like', '%' . $name . '%'], ['document_details.doc_id', '=', $doc_id]])
                ->get();

            return View("recordview", compact('document', 'comments'));
        } else {

            return View("view", compact('document', 'comments'));
        }
    }

    public function saveRecord(Request $request)
    {
        $request->validate([
            'subject' => 'required',
            'doc_type' => 'required',
            'doc_date' => 'required',
            'date_receive' => 'required',
            'sender' => 'required',
            'company' => 'required',
            'rfrom' => 'required'
        ]);
        $valid = true;
        $files = $request->file('files');

        foreach ($files as $file) {
            $rules = array('files' => 'required|mimes:pdf'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('files' => $file), $rules);

            if ($validator->fails()) {
                $valid = false;
                return back()->with('fail', 'File attachments should be in pdf format.');
            }
        }


        // checks if the files are valid (in pdf format) 
        if ($valid) {
            // insert code for routing_details //
            $route = new routing_details;
            $route->route_slip = $request->rslip;
            $route->route_from = $request->rfrom;
            $route->route_thru = $request->routeThru;
            $route->route_to = $request->routeTo;
            $route->action = $request->action;
            $route->save();

            // insert code for document_details //
            $max_id = DB::table('routing_details')->max('route_id');
            $route_id = $max_id;

            $doc = new document_details;
            $doc->users_id = auth()->user()->id;
            $doc->subject = $request->subject;
            $doc->document_type = $request->doc_type;
            $doc->document_date = $request->doc_date;
            $doc->date_received = $request->date_receive;
            $doc->sender = $request->sender;
            $doc->route_id = $route_id;
            $doc->company = $request->company;
            $doc->save();

            // insert code for attachment_list //
            $max = DB::table('document_details')->max('doc_id');
            $doc_id = $max;

            foreach ($files as $file) {
                $fileName = $file->getClientOriginalName();

                $attach = new attachments_lists;
                $attach->file_name = $fileName;
                $attach->doc_id = $doc_id;
                $attach->save();
            }

            // saving the file to local //
            $attach_files = $request->file('files');
            $paths = [];

            foreach ($attach_files as $savefile) {
                // Generate a file name with extension
                $fileName = $savefile->getClientOriginalName();
                // Save the file
                $paths[] = $savefile->storeAs('files', $fileName);
            }
        }

        $id = Auth::user()->id;
        $userinfo = User::where('users.id', '=', $id)
            ->join('model_has_roles', 'model_id', '=', 'users.id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->first();

        $role = $userinfo->name;

        if ($role == "Regional Director") {
            return redirect('admin/display');
        } else {
            return redirect('user/mydocs')->with('success', 'Incoming File Added Succesfully');
        }
    }

    public function received(Request $request, $id)
    {

        $name = auth()->user()->fname . " " . auth()->user()->lname;
        $user = auth()->user()->usergroup;

        if ($user == "Ordinary User" || $user == "Section Head" || $user == "Division Head") {
            DB::table('route_to_tables')
                ->join('document_details', 'route_to_tables.route_doc_id', '=', 'document_details.doc_id')
                ->where([['route_to_tables.route_to_name', 'like', '%' . $name . '%'], ['document_details.doc_id', '=', $id]])
                ->update([
                    'route_to_tables.receive' => 'yes',
                    'route_to_message' => $request->comments,
                    'route_to_tables.updated_at' => Carbon::now()
                ]);
        }

        return redirect('user/pendingdoc');
    }

    public function editRecord(Request $request, $id)
    {
        $route_to = array_merge((array) $request->routeTo, (array) $request->routeThru);
        DB::table('document_details')
            ->where('doc_id', $request->doc_id)
            ->update([
                'subject' => $request->subject,
                'document_type' => $request->doc_type,
                'document_date' => $request->doc_date,
                'date_received' => $request->date_received,
                'sender' => $request->sender,
                'company' => $request->company,
            ]);

        DB::table('routing_details')
            ->where('route_id', $request->route_id)
            ->update([
                'route_from' => $request->rfrom,
            ]);

        if ($request->routeThru == null && $request->action == null && $request->routeTo == null) {
            DB::table('routing_details')
                ->where('route_id', $request->route_id)
                ->update([
                    'route_from' => $request->rfrom,
                ]);
        } else {
            $route_thrus = implode(", ", (array) $request->routeThru);
            $route_tos = implode(", ", (array) $request->routeTo);
            $action = implode(", ", (array) $request->action);


            DB::table('routing_details')
                ->where('route_id', $request->route_id)
                ->update([
                    'route_from' => $request->rfrom,
                    'route_thru' =>  $route_thrus,
                    'route_to' => $route_tos,
                    'action' => $action
                ]);

            for ($i = 0; $i < count($route_to); $i++) {
                $route = new route_to_table();
                $route->route_to_id = $request->route_id;
                $route->route_doc_id = $request->doc_id;
                $route->route_to_name = $route_to[$i];
                $route->save();
            }
        }

        $files = $request->file('file');
        if ($files == null) {
        } else {
            $valid = false;
            foreach ($files as $file) {
                $rules = array('file' => 'required|mimes:pdf'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = Validator::make(array('file' => $file), $rules);

                if ($validator->fails()) {
                    return back()->with('fail', 'A file/s must be of type pdf.');
                } else {

                    $fileName = $file->getClientOriginalName();

                    $attach = new attachments_lists;
                    $attach->file_name = $fileName;
                    $attach->doc_id = $id;
                    $attach->save();

                    // Save the file
                    $paths[] = $file->storeAs('files', $fileName);
                }
            }
            // delete the oldfile in local 
            $old = $request->oldFile;

            foreach ($old as $filename) {
                Storage::delete($filename);
                unlink(storage_path('app/files/' . $filename));

                // delete the old file in database
                DB::table('attachments_lists')->where('file_name', $filename)->delete();
            }
        }

        $id = Auth::user()->id;
        $userinfo = User::where('users.id', '=', $id)
            ->join('model_has_roles', 'model_id', '=', 'users.id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->first();

        $role = $userinfo->name;

        if ($role == "Regional Director") {
            return redirect('admin/display');
        } else {
            return redirect('user/mydocs');
        }
    }

    public function showRoutedDoc()
    {
        $user = Auth::user()->fname . ' ' . Auth::user()->lname;
        $document = DB::table('document_details')
            ->join('routing_details', 'document_details.route_id', '=', 'routing_details.route_id')
            ->where('routing_details.route_to', $user)
            ->get();


        return view('user.routings', compact('document'));
    }
    public function getDownload($file_name)
    {
        //PDF file is stored under dts/storage/app/files/info.pdf
        $file = storage_path('app/files/' . $file_name);
        return response()->download($file);
    }

    public function actionsettings()
    {
        $actions = DB::table('actions')
            ->get();
        return view('admin.actions', compact('actions'));
    }
    public function saveaction(Request $request)
    {

        $request->validate([
            'action' => 'required'
        ]);
        //insert data
        DB::table('actions')
            ->insert(array('action' => $request->action));

        $actions = DB::table('actions')
            ->get();
        return view('admin.actions', compact('actions'));
    }
    public function updateaction(Request $request)
    {
        DB::table('actions')
            ->where('id', $request->id)
            ->update([
                'action' => $request->action
            ]);
        $actions = DB::table('actions')
            ->get();
        return view('admin.actions', compact('actions'));
    }
    public function documenttypes(Request $request)
    {
        // $doc_types = DB::table('document_types')
        //     ->get();

        $doc_types = DB::table('document_types')
            ->leftJoin('document_details', 'document_types.doc_type', '=', 'document_details.document_type')
            ->select('document_types.doc_type', 'document_details.document_type', 'id', DB::raw('count(document_type) as count'))
            ->groupBy('doc_type', 'document_type', 'id', DB::Raw('IFNULL(document_type , 0 )'))
            ->orderBy('count', 'desc')
            ->get();

        return view('admin.types', compact('doc_types'));
    }
    public function savedoctype(Request $request)

    {

        $request->validate([
            'doc_type' => 'required'
        ]);
        //insert data
        DB::table('document_types')
            ->insert(array('doc_type' => $request->doc_type));

        return back()->with('success', 'Document Type  was added.');
    }
    public function updatedocumenttype(Request $request)
    {
        DB::table('document_types')
            ->where('id', $request->id)
            ->update([
                'doc_type' => $request->doc_type
            ]);

        return back()->with('success', 'Document Type was successfully updated.');
    }
    // --------------------------end of used codes -----------------------------------------------

}
