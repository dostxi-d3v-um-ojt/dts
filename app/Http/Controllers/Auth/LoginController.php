<?php

namespace App\Http\Controllers\Auth;


use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Http\Client\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Validator;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    //use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = '/sadasdsad';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('guest')->except('logout');
    // }



    // -------------------------functions for login and registration forms ---------------------------------------

    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function handleLogin()
    {
        request()->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $userinfo = User::where('email', '=', request()->email)
            ->join('model_has_roles', 'model_id', '=', 'users.id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->first();

        if (!$userinfo) {

            return redirect('login')->with('fail', 'We do not recognize your email address');
        } else {
            $email = request()->email;
            $password = request()->password;

            if (Auth::attempt(['email' => $email, 'password' => $password])) {
                request()->session()->put('LoggedUser', $userinfo->id);
                $role = $userinfo->name;

                $token = $userinfo->createToken('myapptoken')->plainTextToken;
                $response = [
                    'user' => $userinfo,
                    'token' => $token
                ];

                //$users->assignRole('user');
                response($response, 201);

                if ($role == "Regional Director") {
                    return redirect('/admin/dashboard');
                } else {
                    return redirect('/user/dashboard');
                }
            }
            //response(['message' => 'bad creds']);
            return back()->with('fail', 'Invalid username/password');
        }
    }


    public function logout()
    {

        auth()->user()->tokens()->delete();
        //auth()->logout();
        Session::flush();
        // Auth::logout()
        return redirect('login');
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }
}
