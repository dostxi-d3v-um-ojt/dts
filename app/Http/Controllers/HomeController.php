<?php

namespace App\Http\Controllers;


use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }



    // --------------------------- functions for user's CRUD in admin dashboard --------------------------------------
    public function showAddUser()
    {
        return view('admin.addUser');
    }

    public function users()
    {
        $id = auth()->user()->id;
        $user = DB::table('users')
            ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
            ->select('users.*', 'roles.name as role')
            ->where('users.id', '!=', $id)
            ->get();

        return view('admin.user', compact('user'));
    }

    public function showEdit($id)
    {
        $user = DB::table('users')
            ->select('users.*')
            ->where('id', $id)
            ->get();

        $userole = DB::table('model_has_roles')->select('role_id')->where('model_id', $id)->get();
        return view('admin.editUser', compact('user', 'userole'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update($request->except(['_token', 'role']));
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        if ($request->usergroup  == "Ordinary User") {
            $user->assignRole('Ordinary User Role');
        } else {
            $user->assignRole($request->usergroup);
        }

        return redirect('admin/user');
    }

    public function add(Request $request)
    {

        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|max:12',
            'usergroup' => 'required'
        ]);

        $user = User::create([
            'fname' => $request->firstname,
            'lname' => $request->lastname,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'usergroup' => $request->usergroup
        ]);

        if ($request->usergroup == "Regional Director") {
            $user->assignRole('Regional Director');
        } elseif ($request->usergroup == "Document Controller") {
            $user->assignRole('Document Controller');
        } elseif ($request->usergroup == "Division Head") {
            $user->assignRole('Division Head');
        } elseif ($request->usergroup == "Section Head") {
            $user->assignRole('Section Head');
        } elseif ($request->usergroup == "Ordinary User") {
            $user->assignRole('Ordinary User Role');
        }
        return redirect('admin/adduser')->with('success', 'Account created');
    }

    public function delete($id)
    {
        User::find($id)->delete();
        DB::table('model_has_roles')->where('model_id', $id)->delete();

        return redirect('admin/user');
    }
    public function underdevelopment()
    {
        return view('errors.underdev');
    }
}
