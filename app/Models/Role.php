<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\PermissionRelationship;

class Role extends Model
{

    protected $connection = 'mysql';

    protected $table = 'roles';

    protected $fillable = ['name'];
}
