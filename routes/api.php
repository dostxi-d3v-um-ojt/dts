<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//public routes should be here
Route::post('/authenticate', [LoginController::class, 'handleLogin'])->name('auth.check');
Route::get('/about-us', [LoginController::class, 'about'])->name('about');
Route::get('/contact-info', [LoginController::class, 'contact'])->name('contact');


//private routes should be here
Route::group(['middleware' => 'auth:sanctum'], function () {
  Route::get('/register', [RegisterController::class, 'showRegisterForm'])->name('auth.register');
  Route::post('/saveregister', [RegisterController::class, 'save'])->name('saveregister');
  Route::get('/logout', [LoginController::class, 'logout'])->name('auth.logout');
});
