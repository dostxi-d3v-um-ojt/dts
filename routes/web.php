<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\HomeController;
use League\CommonMark\Block\Element\Document;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', [LoginController::class, 'showLoginForm'])->name('auth.login');


Route::group(['middleware' => ['role:Regional Director']], function () {
  Route::prefix('admin')->name('admin.')->group(function () {

    //Regional Director  = Admin
    // Routes for user managemnet 
    Route::get('/user', [HomeController::class, 'users'])->name('showUser');
    Route::get('/adduser', [HomeController::class, 'showAddUser'])->name('addUser');
    Route::post('/saveaccount', [HomeController::class, 'add'])->name('saveaccount');
    Route::get('/edit/{id}', [HomeController::class, 'showEdit'])->name('editUser');
    Route::post('/saveEdit/{id}', [HomeController::class, 'update'])->name('update');
    Route::post('/savedelete/{id}', [HomeController::class, 'delete'])->name('accountdelete');

    // routes for Document Management - Reginal Director
    Route::get('/dashboard', [DocumentController::class, 'adminDashboard'])->name('dashboard');
    Route::get('/display', 'DocumentController@displayunrouted')->name('documents');
    Route::get('/routed', 'DocumentController@displayrouted')->name('routed');
    Route::get('/{action}/{id}/{rid}/', 'DocumentController@view')->name('recordview');
    Route::get('/{action}/{id}/{rid}', 'DocumentController@view')->name('delete');

    //myprofile
    Route::get('/myprofile/{id}', [UserController::class, 'showEdit'])->name('profile');

    //commments
    Route::get('/displaycomments', [DocumentController::class, 'displaycomments'])->name('displaycomments');

    //document receive
    Route::get('/displaydocumentreceive', [DocumentController::class, 'displaydocumentreceive'])->name('displaydocumentreceive');
    Route::get('/displaydocumentunreceive', [DocumentController::class, 'displaydocumentunreceive'])->name('displaydocumentunreceive');

    //actions settings
    Route::get('/actions', [DocumentController::class, 'actionsettings'])->name('actions');
    Route::post('/saveaction', [DocumentController::class, 'saveaction']);
    Route::post('/updateaction/{id}', [DocumentController::class, 'updateaction'])->name('updateaction');
    Route::post('/deleteaction/{id}', [DocumentController::class, 'deleteaction'])->name('deleteaction');

    //document types
    Route::get('/types', [DocumentController::class, 'documenttypes'])->name('types');
    Route::post('/savedoctype', [DocumentController::class, 'savedoctype']);
    Route::post('/updatedocumenttype/{id}', [DocumentController::class, 'updatedocumenttype'])->name('updatedocumenttype');
  });
});


// User routes
Route::group(['middleware' => ['role:Document Controller']], function () {
  Route::prefix('user')->name('user.')->group(function () {
    //mydocuments tab
    Route::get('/newdocument', 'DocumentController@newDocView')->name('incoming_doc');
  });
});

Route::group(['middleware' => ['permission:view-incoming-documents']], function () {
  Route::prefix('admin')->name('admin.')->group(function () {
    Route::post('/document', 'DocumentController@saveRecord')->name('savedoc');
    Route::get('/{action}/{id}/{rid}', 'DocumentController@view')->name('edit');
    Route::post('/saveupdate/{id}', 'DocumentController@editRecord')->name('saveupdate');
    Route::get('/download/{file}', 'DocumentController@getDownload')->name('download');
  });
});


Route::group(['middleware' => ['role:Document Controller|Division Head|Section Head|Ordinary User Role']], function () {
  Route::prefix('user')->name('user.')->group(function () {
    //dashboard tab
    Route::get('/dashboard', [DocumentController::class, 'userDashboard'])->name('dashboard');

    //recent document
    Route::get('/mydocs', [DocumentController::class, 'showDocuments'])->name('document');
    Route::get('/{action}/{id}/{rid}/', 'DocumentController@view')->name('recordview');
    Route::get('/{action}/{id}/{rid}', 'DocumentController@view')->name('edit');
    Route::post('/received/{id}', [DocumentController::class, 'received'])->name('received');
    Route::get('/download/{file}', 'DocumentController@getDownload')->name('download');

    //pending document - ordinary user/heads
    Route::get('/pendingdoc', [DocumentController::class, 'pendingDocuments'])->name('pending');
    Route::get('/pendingIncomingDocuments', [DocumentController::class, 'pendingIncomingDocuments'])->name('pendingincoming');

    //received document
    Route::get('/receiveddoc', [DocumentController::class, 'receivedDocuments'])->name('receive');

    //routed to me
    Route::get('/routedocument', 'DocumentController@showRoutedDoc')->name('routings');

    //myprofile
    Route::get('/myprofile/{id}', [UserController::class, 'showEdit'])->name('profile');
    Route::post('/update/{id}', [UserController::class, 'update'])->name('update');
  });
});

//for testings
Route::get('/', [UserController::class, 'try']);

//route for underdevelopment page
Route::get('/underdevelpoment', [HomeController::class, 'underdevelopment']);




// C:/xampp/htdocs/DTS/dts
// Auth::routes();
// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
